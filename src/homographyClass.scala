//version 3.1

import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet._
import processing.core.PGraphics._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library._;
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;


class homographyClass extends PApplet{
  

  var homographyMatrix = Matrix.zeros[Double](3,3)

  def main(args: Array[String]) {
  }
  def pointExample2(){
	  val proj1=new PVector(100,100)
	  val proj2=new PVector(200,100)
      val proj3=new PVector(200,200)
	  val proj4=new PVector(100,200)
	  val cam1=new PVector(98,102)
	  val cam2=new PVector(255,96)
	  val cam3=new PVector(255,204)
	  val cam4=new PVector(98,198)
      val proj=Array[PVector](proj1,proj2,proj3,proj4)
	  val cam=Array[PVector](cam1,cam2,cam3,cam4)

      computeHomography(cam,proj)
	  
	  val testPoint=new PVector(100,100)
	  println(applyToPoint(testPoint))
  }

  def applyToScreen1(source: PImage):PImage={
	var destination: PImage = createImage(source.width, source.height, RGB);
	var sourceLocation: Int = 0
	var destinationLocation: Int = 0
	var c =color(source.pixels(sourceLocation))
	var mappedCoordinate=Array[Double](0,0)
	var X:Double=0
	var Y:Double=0
	
    source.loadPixels();
    destination.loadPixels();
    
    val widthh = source.width
    val heightt = source.height
    
    for (y <- 0 to (heightt-1)) {
      for (x <- 0 to (widthh-1)) {
        destinationLocation = x + y*widthh;

        
        
        mappedCoordinate=applyToCoordinate(Array(x,y))
        X=mappedCoordinate(0)
        Y=mappedCoordinate(1)
        if (X >= 0 & X<widthh & Y>=0 & Y<heightt) {
	        sourceLocation = X.toInt + widthh * Y.toInt
	        c = color(source.pixels(sourceLocation))
	        destination.pixels(destinationLocation)=c
        }
        
        }
      }
	destination.updatePixels();
	return(destination)
  }
    
    
    
  def applyToPImage2(source: PImage):PImage={
	var destination: PImage = createImage(source.width, source.height, RGB);
	var sourceLocation: Int = 0
	var destinationLocation: Int = 0
	var c =color(source.pixels(sourceLocation))
	var mappedCoordinate=Array[Double](0,0)
	var X:Double=0
	var Y:Double=0
	
    source.loadPixels();
    destination.loadPixels();
    
    val widthh = source.width
    val heightt = source.height
    
    for (y <- 0 to (heightt-1)) {
      for (x <- 0 to (widthh-1)) {
        destinationLocation = x + y*widthh;
        
        mappedCoordinate=applyToCoordinate(Array(x,y))
        X=mappedCoordinate(0)
        Y=mappedCoordinate(1)
        if (X >= 0 & X<widthh & Y>=0 & Y<heightt){
	        sourceLocation = X.toInt + widthh * Y.toInt
	        c = color(source.pixels(sourceLocation))
	        destination.pixels(destinationLocation)=c
        }
        
        }
      }
	destination.updatePixels();
	return(destination)
  }
    def applyToPoint(inputPoint: PVector):PVector={
	  var X=inputPoint.x
	  var Y=inputPoint.y
	  var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
	  
	  val x:Float=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val y:Float=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val outputPoint: PVector=new PVector(x,y,inputPoint.z)
	  return outputPoint
  }
  def applyToPImage(source: PImage):PImage={
	var destination: PImage = createImage(source.width, source.height, RGB);
	var sourceLocation: Int = 0
	var destinationLocation: Int = 0
	var c =color(source.pixels(sourceLocation))
	var mappedCoordinate=Array[Double](0,0)
	var X:Double=0
	var Y:Double=0
    source.loadPixels();
    destination.loadPixels();
    val widthh = source.width
    val heightt = source.height
    for (y <- 0 to (heightt-1)) {
      for (x <- 0 to (widthh-1)) {
        sourceLocation = x + y*widthh;
        c = color(source.pixels(sourceLocation))
        
        
        mappedCoordinate=applyToCoordinate(Array(x,y))
        X=mappedCoordinate(0)
        Y=mappedCoordinate(1)
        if (X >= 0 & X<widthh & Y>=0 & Y<heightt){
	        destinationLocation = X.toInt + widthh * Y.toInt
	        destination.pixels(destinationLocation)=c
        }
        
        }
      }
	destination.updatePixels();
	return(destination)
  }
  def writeHomography(fileName: String){
	  val file = new PrintWriter(new File(fileName));
	  var content: java.lang.String=""
		for(i <- 0 to 2){
		for(j <- 0 to 2){
			content = homographyMatrix(i,j).toString
			file.write(content)
			file.write("\n")
		}
	}
	file.close();
  }    
    
  def loadHomography(fileName: String) {
		var item:String=""
		var column=0
		var row=0
		var homographyMatrix = Matrix.zeros[Double](3,3)
		val file = Source.fromFile(fileName)
		file.getLines.foreach((line) =>{
			item=line
			homographyMatrix(row,column)=item.toDouble
			column+=1
			if (column==3){
				column=0
				row+=1
			}
		})
	}
	

  def computeHomography(cameraPoints: Array[PVector], projectorPoints: Array[PVector]){  
	    if (cameraPoints.length!=projectorPoints.length){println("Different points amount bewteen camera and projector image! Please have a double check.")}
		var numberOfPoints: Int = (cameraPoints.length)
        
		var cam = Matrix.zeros[Double](numberOfPoints,2)
		var proj = Matrix.zeros[Double](numberOfPoints,2)
		for (i <- 0 to (numberOfPoints-1)){
			cam(i,0)=cameraPoints(i).x.toDouble
			cam(i,1)=cameraPoints(i).y.toDouble
			proj(i,0)=projectorPoints(i).x.toDouble
			proj(i,1)=projectorPoints(i).y.toDouble
		}
		
		var matrix1 = Matrix.zeros[Double](numberOfPoints*2,9)
		var row1 = DenseVector.zeros[Double](9)
		var row2 = DenseVector.zeros[Double](9)
		for (i <- 0 to (numberOfPoints-1)){
			row1 = DenseVector(cam(i,0), cam(i,1), 1, 0, 0, 0, -cam(i,0)*proj(i,0), -cam(i,1)*proj(i,0), -proj(i,0))
			row2 = DenseVector(0, 0, 0, cam(i,0), cam(i,1), 1, -cam(i,0)*proj(i,1), -cam(i,1)*proj(i,1), -proj(i,1))
			matrix1(i*2,::) := row1
			matrix1(i*2+1,::) := row2
		}

		var matrix2 = matrix1.t * matrix1		
		val (eigenvalues,valuee0,eigenvectors)=eig(matrix2)
		
		
		var minPosition=0
		var minValue=eigenvalues(0)
		for (i <- 0 to 8){
		  if (eigenvalues(i)<minValue){
		    minValue=eigenvalues(i)
		    minPosition=i
		  }
		}
		
		var eigenvector = eigenvectors(::,minPosition)
		
		for(i <- 0 to 2){
			for(j <- 0 to 2){
				homographyMatrix(i,j)=eigenvector(i+3*j)
			}
		}
  }	


  
  def applyToCoordinates(inputCoordinates: Array[Double]):Array[Double]={
      var outputCoordinates=inputCoordinates
      val numberOfPoints=(inputCoordinates.length)/2
      
      var X=0.0
      var Y=0.0
      var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
      
      for (i <- 0 to (numberOfPoints-1)){
		  X=inputCoordinates(i*2)
		  Y=inputCoordinates(i*2+1)

		  outputCoordinates(i*2)=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
		  outputCoordinates(i*2+1)=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))
      }
	  return outputCoordinates
  }
  
  
  def applyToCoordinate(inputCoordinate: Array[Double]):Array[Double]={
	  val X=inputCoordinate(0)
	  val Y=inputCoordinate(1)
	  var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
	  
	  val x:Double=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
	  val y:Double=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))
	  return Array(x,y)
  }
  
  def applyToCoordinate3(XX: Int, YY: Int):Array[Float]={
	  val X=XX.toDouble
	  val Y=YY.toDouble
	  var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
	  
	  val x=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val y=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat

	  return Array(x,y)
  }
  
  def applyToCoordinate2(inputCoordinate: Array[Double]):Array[Double]={
	val cameraFrame=DenseVector[Double](inputCoordinate(0),inputCoordinate(1),1.toDouble)
    val projectorFrame=homographyMatrix * cameraFrame

	return Array(projectorFrame(0)/projectorFrame(2),projectorFrame(1)/projectorFrame(2))
  }  
}

  



