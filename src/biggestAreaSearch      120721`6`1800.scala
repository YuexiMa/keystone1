import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _, abs=>_, _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;

object biggestAreaSearch1 extends PApplet{
	val qqx=640/4
	val qqy=480/4
//	  1.0, 1.0, 998.45 ][ 571.567, 57.1062, 984.85974 ][ 520.5797, 476.74725, 1000.238 ][ 51.028015, 455.55762, 1001.67426 ]

	val aa=Array[Double]( 221.0, 131.0, 2629.0 )
	val bb=Array[Double](451.0, 131.0, 2063.0 )
	val cc=Array[Double](  451.0, 281.0, 1553.0  )
	val dd=Array[Double]( 221.0, 281.0, 0.0)
//	[ 451.0, 131.0, 2063.0 ][ 451.0, 281.0, 1553.0 ][ 221.0, 281.0, 0.0 ]
	
	var q1:PVector = new PVector(aa(0).toFloat, aa(1).toFloat,aa(2).toFloat);
	var q2:PVector = new PVector(bb(0).toFloat, bb(1).toFloat,bb(2).toFloat);
	var q3:PVector = new PVector(cc(0).toFloat, cc(1).toFloat,cc(2).toFloat);
	var q4:PVector = new PVector(dd(0).toFloat, dd(1).toFloat,dd(2).toFloat);
	
	/*[0] [ 528.1312, 1.0, 473.15677 ]
[1] [ 989.87823, 23.639526, 252.19371 ]
[2] [ 1360.8964, 378.2489, 835.48285 ]
[3] [ 549.89343, 708.0903, 813.11346 ]
[0] [ 691.78925, 105.25421, 0.0 ]
[1] [ 1312.0, 105.25421, 0.0 ]
[2] [ 1312.0, 456.31833, 0.0 ]
[3] [ 691.78925, 456.31833, 0.0 ]*/
	
	
	var biggestArea=0.toFloat
	var qqq1=q1
	var qqq2=q2
	var qqq3=q3
	var qqq4=q4
	var area=new maximizeProjectionClass()
	val ratio=480.toFloat/848
		val searchs=1000
	
	def main(args: Array[String]) {
		val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(biggestAreaSearch1)
		biggestAreaSearch1.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
		
	}
	override def setup() {
		size(1200,700,P2D)
		stroke(0,255,0)
		line(q1.x,q1.y,q2.x,q2.y)
		line(q1.x,q1.y,q4.x,q4.y)
		line(q3.x,q3.y,q2.x,q2.y)
		line(q3.x,q3.y,q4.x,q4.y)
		
//		searchLeft2(q1,q2,q3,q4)
//		searchRight(q1,q2,q3,q4)
		var qqq=area.maximize(q1,q2,q3,q4,ratio)
		qqq1=qqq(0)
		qqq2=qqq(1)
		qqq3=qqq(2)
		qqq4=qqq(3)
//		println("qqq is" +qqq1+qqq2+qqq3+qqq4)
		background(0)
		stroke(255,0,0)
//		line(0,height/2,width,height/2)
		
		drawLine(createLine(qqq1,qqq2))
//		drawLine(createLine(qqq2,qqq3))
		line(qqq2.x,qqq2.y,qqq3.x,qqq3.y)
		drawLine(createLine(qqq3,qqq4))
		line(qqq4.x,qqq4.y,qqq1.x,qqq1.y)
//		drawLine(createLine(qqq1,qqq4))
//		println(biggestArea)
		println(qqq1)
		println(qqq2)
		println(qqq3)
		println(qqq4)
		
		val line1=createLine(q1,q2)
		stroke(0,0,255)
		drawLine(line1)
		val line2=createLine(q2,q3)
		val intt=getIntersect(line1,line2)
		ellipse(intt.x,intt.y,10,10)
		
		stroke(0,255,0)
		line(q1.x,q1.y,q2.x,q2.y)
		line(q1.x,q1.y,q4.x,q4.y)
		line(q3.x,q3.y,q2.x,q2.y)
		line(q3.x,q3.y,q4.x,q4.y)
		
	}
//	override def draw() {
//	}
	
	
	
	
	
	def searchRight(a:PVector,b:PVector,c:PVector,d:PVector) {
		val line1=createLine(a,b)
		val line2=createLine(b,c)
		val line3=createLine(c,d)
		val line4=createLine(d,a)
		
		
		if (q2.x>q3.x) {
//			println{"start from q3"}
			var start=q3.x.toInt
//			for (xx <- start to (start+(abs(q3.x-q2.x)).toInt)) {
			for (i <- 0 to searchs) {
				val xx=start+(abs(q3.x-q2.x))*i/searchs
				val yy=getY(xx,line2)
				val qq3=new PVector(xx,yy)
				val oblique=createLine(qq3,new PVector(qq3.x-1,qq3.y-ratio))
//				println("oblique")
//				println(oblique)
				val qq11=getIntersect(oblique,line1)
				val qq12=getIntersect(oblique,line4)
				val qq13=getIntersect(oblique,line3)
				var qq1=qq3
				if (withinQuadrilateral(qq11,a,b,c,d)) {qq1=qq11}
				if (withinQuadrilateral(qq12,a,b,c,d)) {qq1=qq12}
				if (withinQuadrilateral(qq13,a,b,c,d)) {qq1=qq13}
//				println(qq3)
//				println(qq1)
				var right=createLine(qq3,new PVector(qq3.x,qq3.y-1))
				var down=createLine(qq3,new PVector(qq3.x-1,qq3.y))
				
				var qq4=new PVector(qq1.x,qq3.y)
				if (withinQuadrilateral(qq4,a,b,c,d)) {}else {
						val qq41=getIntersect(down,line1)
						val qq42=getIntersect(down,line4)
						val qq43=getIntersect(down,line3)
						if (withinQuadrilateral(qq41,a,b,c,d)) {qq4=qq41}
						if (withinQuadrilateral(qq42,a,b,c,d)) {qq4=qq42}
						if (withinQuadrilateral(qq43,a,b,c,d)) {qq4=qq43}
					
					qq1.x=qq4.x
					qq1.y=qq3.y+(qq1.x-qq3.x)*ratio
				}
//				println(qq1)
				
				var qq2=new PVector(qq3.x,qq1.y)
				if (withinQuadrilateral(qq2,a,b,c,d)) {}else {
					val qq21=getIntersect(right,line1)
					val qq22=getIntersect(right,line4)
					val qq23=getIntersect(right,line3)
					if (withinQuadrilateral(qq21,a,b,c,d)) {qq2=qq21}//};println(1)}
					if (withinQuadrilateral(qq22,a,b,c,d)) {qq2=qq22}//;println(1)}
					if (withinQuadrilateral(qq23,a,b,c,d)) {qq2=qq23}//;println(1)}
					
					qq1.y=qq2.y
					qq1.x=qq3.x+(qq1.y-qq3.y)/ratio
					qq4.x=qq1.x
				}
				
				var area=abs((qq3.x-qq1.x)*(qq3.y-qq1.y))
//				println(qq1)
//				println(qq2)
//				println(qq3)
//				println(qq4)
//				println(area)
				
				if (area>biggestArea) {
					biggestArea=area
//					println(biggestArea)
					qqq1=qq1
					qqq2=qq2
					qqq3=qq3
					qqq4=qq4
				}
			}
		}else {
			var start=q2.x.toInt
			for (i <- 0 to searchs) {
				val xx=start+(abs(q3.x-q2.x))*i/searchs
//			for (xx <- start to (start+(abs(q3.x-q2.x)).toInt)) {
				val yy=getY(xx,line2)
				val qq2=new PVector(xx,yy)
				val oblique=createLine(qq2,new PVector(qq2.x-1,qq2.y+ratio))
//				println("oblique")
//				println(oblique)
				val qq41=getIntersect(oblique,line1)
				val qq42=getIntersect(oblique,line4)
				val qq43=getIntersect(oblique,line3)
				var qq4=qq2
				if (withinQuadrilateral(qq41,a,b,c,d)) {qq4=qq41}
				if (withinQuadrilateral(qq42,a,b,c,d)) {qq4=qq42}
				if (withinQuadrilateral(qq43,a,b,c,d)) {qq4=qq43}
//				println(qq2)
//				println(qq4)
				var right=createLine(qq2,new PVector(qq2.x,qq2.y+1))
				var up=createLine(qq2,new PVector(qq2.x-1,qq2.y))
				
				var qq3=new PVector(qq2.x,qq4.y)
				if (withinQuadrilateral(qq3,a,b,c,d)) {}else {
						val qq31=getIntersect(right,line1)
						val qq32=getIntersect(right,line4)
						val qq33=getIntersect(right,line3)
						if (withinQuadrilateral(qq31,a,b,c,d)) {qq3=qq31}
						if (withinQuadrilateral(qq32,a,b,c,d)) {qq3=qq32}
						if (withinQuadrilateral(qq33,a,b,c,d)) {qq3=qq33}
					
					qq4.y=qq3.y
					qq4.x=qq2.x-(qq4.y-qq2.y)/ratio
				}
//				println(qq4)
//				println(qq3)
//				println("")
				
				var qq1=new PVector(qq4.x,qq2.y)
				if (withinQuadrilateral(qq1,a,b,c,d)) {}else {
					val qq11=getIntersect(up,line1)
					val qq12=getIntersect(up,line4)
					val qq13=getIntersect(up,line3)
					if (withinQuadrilateral(qq11,a,b,c,d)) {qq1=qq11}//};println(1)}
					if (withinQuadrilateral(qq12,a,b,c,d)) {qq1=qq12}//;println(1)}
					if (withinQuadrilateral(qq13,a,b,c,d)) {qq1=qq13}//;println(1)}
//					println(qq1)
					qq4.x=qq1.x
					
					qq4.y=qq2.y-(qq4.x-qq2.x)*ratio
					qq3.y=qq4.y
				}
				
				var area=abs((qq3.x-qq1.x)*(qq3.y-qq1.y))
//				println(qq1)
//				println(qq2)
//				println(qq3)
//				println(qq4)
//				println(area)
				
				if (area>biggestArea) {
					biggestArea=area
//					println(biggestArea)
					qqq1=qq1
					qqq2=qq2
					qqq3=qq3
					qqq4=qq4
				}		
				
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	def searchLeft2(a:PVector,b:PVector,c:PVector,d:PVector) {
		val line1=createLine(a,b)
		val line2=createLine(b,c)
		val line3=createLine(c,d)
		val line4=createLine(d,a)
		
		if (q1.x>q4.x) {
//			println{"start from q4"}
			var start=q4.x.toInt
//			for (xx <- start to (start+(abs(q1.x-q4.x)).toInt)) {
			
			for (i <- 0 to searchs) {
				val xx=start+(abs(q3.x-q2.x))*i/searchs
				val yy=getY(xx,line4)
				val qq1=new PVector(xx,yy)
				val oblique=createLine(qq1,new PVector(qq1.x+1,qq1.y+ratio))
//				println("oblique")
//				println(oblique)
				val qq31=getIntersect(oblique,line1)
				val qq32=getIntersect(oblique,line2)
				val qq33=getIntersect(oblique,line3)
				var qq3=qq1
				if (withinQuadrilateral(qq31,a,b,c,d)) {qq3=qq31}//;println("line1")}
				if (withinQuadrilateral(qq32,a,b,c,d)) {qq3=qq32}//;println("line2")}
				if (withinQuadrilateral(qq33,a,b,c,d)) {qq3=qq33}//;println("line3")}
//				println("qq3 is")
//				println(qq3)
//				println(qq1)
				
				var left=createLine(qq1,new PVector(qq1.x,qq1.y+1))
				var up=createLine(qq1,new PVector(qq1.x+1,qq1.y))
				
				var qq4=new PVector(qq1.x,qq3.y)
				if (withinQuadrilateral(qq4,a,b,c,d)) {}else {
						val qq41=getIntersect(left,line1)
						val qq42=getIntersect(left,line2)
						val qq43=getIntersect(left,line3)
						if (withinQuadrilateral(qq41,a,b,c,d)) {qq4=qq41}
						if (withinQuadrilateral(qq42,a,b,c,d)) {qq4=qq42}
						if (withinQuadrilateral(qq43,a,b,c,d)) {qq4=qq43}
					
					qq3.y=qq4.y
					qq3.x=qq1.x+(qq3.y-qq1.y)/ratio
				}
				
				var qq2=new PVector(qq3.x,qq1.y)
				if (withinQuadrilateral(qq2,a,b,c,d)) {}else {
					val qq21=getIntersect(up,line1)
					val qq22=getIntersect(up,line2)
					val qq23=getIntersect(up,line3)
					if (withinQuadrilateral(qq21,a,b,c,d)) {qq2=qq21}
					if (withinQuadrilateral(qq22,a,b,c,d)) {qq2=qq22}
					if (withinQuadrilateral(qq23,a,b,c,d)) {qq2=qq23}
					
					qq3.x=qq2.x
					qq3.y=qq1.y+(qq3.x-qq1.x)*ratio
					qq4.y=qq3.y
				}
				
				var area=abs((qq3.x-qq1.x)*(qq3.y-qq1.y))
//				println(qq1)
//				println(qq2)
//				println(qq3)
//				println(qq4)
//				println(area)
				
				if (area>biggestArea) {
					biggestArea=area
//					println(biggestArea)
					qqq1=qq1
					qqq2=qq2
					qqq3=qq3
					qqq4=qq4
				}
			}
		}else {
			var start=q1.x.toInt
//			for (xx <- start to (start+(abs(q1.x-q4.x)).toInt)) {
			for (i <- 0 to searchs) {
				val xx=start+(abs(q3.x-q2.x))*i/searchs
				val yy=getY(xx,line4)
				val qq4=new PVector(xx,yy)
				val oblique=createLine(qq4,new PVector(qq4.x+1,qq4.y-ratio))
				
				val qq21=getIntersect(oblique,line1)
				val qq22=getIntersect(oblique,line2)
				val qq23=getIntersect(oblique,line3)
				var qq2=qq4
				if (withinQuadrilateral(qq21,a,b,c,d)) {qq2=qq21}
				if (withinQuadrilateral(qq22,a,b,c,d)) {qq2=qq22}
				if (withinQuadrilateral(qq23,a,b,c,d)) {qq2=qq23}
				
				
				var left=createLine(qq4,new PVector(qq4.x,qq4.y-1))
				var down=createLine(qq4,new PVector(qq4.x+1,qq4.y))
				
				var qq1=new PVector(qq4.x,qq2.y)
				
				if (withinQuadrilateral(qq4,a,b,c,d)) {}else {
						val qq11=getIntersect(left,line1)
						val qq12=getIntersect(left,line2)
						val qq13=getIntersect(left,line3)
						if (withinQuadrilateral(qq11,a,b,c,d)) {qq1=qq11}
						if (withinQuadrilateral(qq12,a,b,c,d)) {qq1=qq12}
						if (withinQuadrilateral(qq13,a,b,c,d)) {qq1=qq13}
					qq2.y=qq1.y
					qq2.x=qq4.x+(qq2.y-qq1.y)/ratio
				}
				var qq3=new PVector(qq2.x,qq4.y)
				if (withinQuadrilateral(qq3,a,b,c,d)) {}else {
					
					val qq31=getIntersect(down,line1)
					val qq32=getIntersect(down,line2)
					val qq33=getIntersect(down,line3)
					if (withinQuadrilateral(qq31,a,b,c,d)) {qq3=qq31}
					if (withinQuadrilateral(qq32,a,b,c,d)) {qq3=qq32}
					if (withinQuadrilateral(qq33,a,b,c,d)) {qq3=qq33}
//					
//					val qq31=getIntersect(down,line2)
//					val qq32=getIntersect(down,line3)
//					if (withinQuadrilateral(qq31,a,b,c,d)) {
//						qq3=qq31
////	if (xx==112) {println("no31......");println(qq31)
////	println(left)}
//						
//					}else {
//						qq3=qq32
////	if (xx==112) {println("no32......");println(qq32)}
//					}
////	if (xx==112) {println("no3......");println(qq3)}
					qq2.x=qq3.x
					qq2.y=qq1.y+(qq2.x-qq1.x)*ratio
					qq1.y=qq2.y
				}
				
//				println(qq1)
//				println(qq2)
//				println(qq3)
//				println(qq4)
				var area=(qq3.x-qq1.x)*(qq3.y-qq1.y)
//				println(area)
				if (area>biggestArea) {
					biggestArea=area
					qqq1=qq1
					qqq2=qq2
					qqq3=qq3
					qqq4=qq4
				}
			}
		}
	}
	
	
	
	
	
	
//	def searchLeft(a:PVector,b:PVector,c:PVector,d:PVector) {
//		val line1=createLine(a,b)
//		val line2=createLine(b,c)
//		val line3=createLine(c,d)
//		val line4=createLine(d,a)
//		
//		var start=0
//		val linee=createLine(q1,q4)
//		if (q1.x>q4.x) {start=q4.x.toInt}else {start=q1.x.toInt}
//		for (xx <- start to (start+(abs(q1.x-q4.x)).toInt)) {
//			val yy=getY(xx,linee)
//			val pointt=new PVector(xx,yy)
//			val horizontal=createLine(pointt,new PVector(xx+1,yy))
//			val vertical=createLine(pointt,new PVector(xx,yy+1))
//			
//			var its1=getIntersect(horizontal,line1)
//			var its2=getIntersect(horizontal,line2)
//			var its3=getIntersect(horizontal,line3)
//			var itsX=pointt
//			if (withinQuadrilateral(its1,a,b,c,d)) {itsX=its1}
//			if (withinQuadrilateral(its2,a,b,c,d)) {itsX=its2}
//			if (withinQuadrilateral(its3,a,b,c,d)) {itsX=its3}
//			
//			its1=getIntersect(vertical,line1)
//			its2=getIntersect(vertical,line2)
//			its3=getIntersect(vertical,line3)
//			var itsY=pointt
//			if (withinQuadrilateral(its1,a,b,c,d)) {itsY=its1}
//			if (withinQuadrilateral(its2,a,b,c,d)) {itsY=its2}
//			if (withinQuadrilateral(its3,a,b,c,d)) {itsY=its3}
//			
//			var qq1=pointt
//			var qq2=itsX
//			var qq4=itsY
//			
//			var left=createLine(qq1,qq4)
//			var up=createLine(qq1,qq2)
//			
//			if (abs(qq2.x-qq1.x)*ratio>abs(qq2.y-qq4.y)) {qq2.x=qq1.x+abs(qq4.y-qq1.y)/ratio}else {qq4.y=qq1.y+(qq4.y-qq1.y)*(abs(qq2.x-qq1.x)*ratio/abs(qq4.y-qq1.y))}
//			
//			var down=createLine(qq4,new PVector(qq4.x+1,qq4.y))
//			var right=createLine(qq2,new PVector(qq2.x,qq2.y+1))
//			var qq3=getIntersect(down,right)
//			
//			var area=abs(qq1.x-qq2.x)*abs(qq1.x-qq2.x)*(ratio)
//			if (withinQuadrilateral(qq4,a,b,c,d)) {}else {
//				val oblique=createLine(q1,q3)
//				val qq31=getIntersect(oblique,line3)
//				val qq32=getIntersect(oblique,line2)
//				//within.....................................................
//				if (getDistance(qq1,qq31)>getDistance(qq1,qq32)){qq3=qq32}else {qq3=qq31}
//				
//			}
//		}
//	}
	def getDistance(a:PVector,b:PVector):Float={
		return (abs(sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)))).toFloat
	}
	def getY(xx:Float,linee:PVector):Float= {
		return linee.x*xx+linee.y
	}
	def getX(yy:Float,linee:PVector):Float= {
		return (yy-linee.y)/linee.x
	}
	def withinQuadrilateral(p:PVector,a:PVector,b:PVector,c:PVector,d:PVector):Boolean= {
		val line1=createLine(a,b)
		val line2=createLine(b,c)
		val line3=createLine(c,d)
		val line4=createLine(d,a)
		if ((p.y).toInt>=(getY(p.x,line1).toInt) &&  (p.y).toInt<= (getY(p.x,line3 )).toInt && (p.x).toInt<=(getX(p.y,line2)).toInt && (p.x).toInt>=(getX(p.y,line4)).toInt){return true}else {return false}
	}
	def withinQuadrilateral2(p:PVector,a:PVector,b:PVector,c:PVector,d:PVector):Boolean= {
		val line1=createLine(a,b)
		val line2=createLine(b,c)
		val line3=createLine(c,d)
		val line4=createLine(d,a)
		if ((p.y)>=(getY(p.x,line1)) &&  (p.y)<= (getY(p.x,line3 )) && (p.x)<=(getX(p.y,line2)) && (p.x)>=(getX(p.y,line4))){return true}else {return false}
	}
	def drawLine(linee:PVector) {//not yet for vertical lines!!!!!!!!!!!!!!
//		loadPixels()
//		for (i <-0 to width-1) {
//			for (j <- 0 to height-1) {
//				if (onLine(new PVector(i,j),linee)) {pixels(i+j*640)=color(0,0,255)}
//			}
//		}
		line(0,linee.y,width,linee.x*width+linee.y)
	}
	def onLine(pointt: PVector, linee:PVector):Boolean= {
		if (pointt.y==linee.x*pointt.x+linee.y) {return true}else {return false}
	}
	
	def createLine(m:PVector,n:PVector):PVector= {
		val a=(m.y-n.y)/(m.x-n.x)
		//y-n.y=a*(x-n.x)=ax-a*n.x
		val b=(-n.x*a+n.y)
		var c=0.toFloat
		if (m.x-n.x==0) {c=m.x}
		return new PVector(a,b,c)
	}
	
	def getIntersect(line1:PVector, line2:PVector):PVector= {
		/**y=line1.x*x+line1.y
		y=line2.x*x+line2.y
		line1.x*x+line1.y=lone2.x*x+line2.y
		x(line1.x-line2.x)=line2.y-line1.y
		**/
		if (line1.z!=0) {return new PVector(line1.z,getY(line1.z,line2))}
		if (line2.z!=0) {return new PVector(line2.z,getY(line2.z,line1))}
		else {
			val xx=(line2.y-line1.y)/(line1.x-line2.x)
			val yy=line1.x*xx+line1.y
			return new PVector(xx,yy)
		}
	}
	
	
//	def getCenterPoint(a:PVector, b:PVector, c:PVector, d:PVector):PVector= {
//		val m1=(q3.y-q1.y)/(q3.x-q1.x)//slope
//		val m2=(q4.y-q2.y)/(q4.x-q2.x)//slope
//		val xx=(m1*q1.x-m2*q2.x+q2.y-q1.y)/(m1-m2)
//		val yy=m1*(xx-q1.x)+q1.y
//		return new PVector(xx,yy,0)
//	}
}