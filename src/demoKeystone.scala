
//version 1.0
//120724`2`0002


/**notes
 * 1. for the right side rotation problem, it may be happening from the ratio control part? no, it happens even when i am not using biggest projection.
 * 2. infinity bug solution not confirmed. ??
 * 3. constrain function solves the enlarging ratio problem.
 * 4. constrain can toggle from homography application part.
 */

import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._

class demoKeystoneClass extends PApplet{
	
	var antiKeystone=new antiKeystoneClass()
	
	def main(args: Array[String]) {
	}
	def run(depthValues:Array[Int]):PImage={
		var b1:PVector = new PVector(220,130,0)
		var b2:PVector = new PVector(450,130,0)
		var b3:PVector = new PVector(450,280,0)
		var b4:PVector = new PVector(220,280,0)
		println("demo")
		return antiKeystone.antiKeystone(b1,b2,b3,b4,depthValues)
	}
}