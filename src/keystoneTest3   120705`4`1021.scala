import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _, abs=>_, _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
/* For OpenGl
 * 1. put jogl.jar and gluegen-rt.jar from processing libiray: H:\Program Files\processing-1.5.1\modes\java\libraries\opengl\library\windows32
 * 2. set jogl.jar's native library to where it comes from.
 * 3. OR, find jogl.dll and jogl_awt.dll and put them in the lib and set native library to it.
 */
import processing.opengl._;

object keystoneTest3 extends PApplet{
	val tuningDepth=false
	val testingDepthMagnification=false
	val tuningCoordinate=true
	val tuningTranslate=true
//    var context: SimpleOpenNI  = null;
	var kinect: SimpleOpenNI  = null;
//	var tuneDepth = -1.toFloat//(0.6375).toFloat;
	var tuneDepth = (0.6375).toFloat
	var tuneShift: Float = 1.5.toFloat
//	var tuneCoordinateX: Float = 843.toFloat
//	var tuneCoordinateY: Float = 2831.toFloat
	var tuneCoordinateX: Float = 0.3328.toFloat
	var tuneCoordinateY: Float = 0.1375.toFloat
	var tuneTranslateX: Float = 1.toFloat
	var tuneTranslateY: Float = 1.toFloat
	var depthMagnification = 1.toFloat
	
	var q1:PVector = new PVector(0,0,0);
	var q2:PVector = new PVector(640,0,0);
	var q3:PVector = new PVector(640,480,0);
	var q4:PVector = new PVector(0,480,0);
	var centerPoint = new PVector(640/2, 480/2, 0)
	var pointt = 0;
	var lasttime=second()+minute()*60;
	var lasttime2=second()+minute()*60;
  
	var  dx:Float = 0.toFloat;
	var  dy:Float = 0.toFloat;
	var  dz:Double = 0.0;
	
	val perPixelPerDistance: Float=1045.toFloat/966/640

	val whitePage = createImage(640,480,RGB)
	whitePage.loadPixels()
	for (i <- 0 to 640*480-1){whitePage.pixels(i)=color(255)}
	
	def main(args: Array[String]) {

		val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(keystoneTest3)
		keystoneTest3.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
	}
	override def setup() {
		/*
//		context = new SimpleOpenNI(this);
//
//		// mirror is by default enabled
//		context.setMirror(true);
//		// enable depthMap generation 
//		if(context.enableDepth() == false)
//		{
//			println("Can't open the depthMap, maybe the camera is not connected!"); 
//			exit();
//			return;
//		}
//		if(context.enableRGB() == false)
//		{
//			println("Can't open the rgbMap, maybe the camera is not connected or there is no rgbSensor!"); 
//			exit();
//			return;
//		}
//
//		size(context.depthWidth() + context.rgbWidth() + 10, context.rgbHeight());
		*/
	  
		size(640, 480,OPENGL);
		kinect = new SimpleOpenNI(this);
		kinect.enableRGB();
		kinect.enableDepth();
	}

	override def draw() {
		/* initial test
//		// update the cam
//		context.update();
//
//		background(255,255,255);
//
//		// draw depthImageMap
//		image(context.depthImage(),0,0);
//
//		// draw rgbImageMap
//		image(context.rgbImage(),context.depthWidth() + 10,0);
		*/
		
		kinect.update();
		background(0);
		val imagee = kinect.rgbImage();
		
		//draw axis
		imagee.loadPixels()
		for (i <- 0 to width-1){imagee.pixels(i+imagee.height/2*imagee.width)=color(0,255,0)}
		for (i <- 0 to height-1){imagee.pixels(imagee.width/2+i*imagee.width)=color(0,255,0)}
		imagee.updatePixels()
		//
		
//		image(imagee, 0, 0);
		
		//calculate corner depth
		if (second()+minute()*60-lasttime>0.5){
			val depthValues = kinect.depthMap();
			var a = new PVector(200,250,0);
			var b = new PVector(300,260,0);
			var c= new PVector(250, 300,0);
			a.z=(-1)*depthValues((a.x+a.y*640).toInt)*tuneDepth;
			b.z=(-1)*depthValues((b.x+b.y*640).toInt)*tuneDepth;
			c.z=(-1)*depthValues((c.x+c.y*640).toInt)*tuneDepth;
			

//			q1.z=(-1)*depthValues((q1.x+100+(q1.y+100)*640).toInt)*tuneDepth;
//			q2.z=(-1)*depthValues((q2.x-100+(q2.y+100)*640).toInt)*tuneDepth;
//			q3.z=(-1)*depthValues((q3.x-100+(q3.y-100)*640).toInt)*tuneDepth;
//			q4.z=(-1)*depthValues((q4.x+100+(q4.y-100)*640).toInt)*tuneDepth;
//			subtractCornerDepths()
					
			/* [replaced by subtractConorDepth().]
				val adjust=min3(a.z,b.z,c.z)+150;
				a.z=a.z-adjust;
				b.z=b.z-adjust;
				c.z=c.z-adjust;
			*/
			math2(a,b,c);
			lasttime=second()+minute()*60;
		}
		
		//drawing
		stroke(0,255,0);
//		background(0);
		//    fill(0,255,0);
		//    rect(0+100,100,width-100,height-100);
//		translate(-tuneDepth*centerPoint.z*dx,-tuneDepth*centerPoint.z*dy)
//		translate(centerPoint.z*dx,centerPoint.z*dy)
//		translate(0,3*(mouseY-height/2))
//		translate(translateProjection().x,translateProjection().y)
		beginShape();
		textureMode(NORMALIZED);
//		texture(imagee);
		texture(whitePage)
		vertex(q1.x, q1.y, q1.z, 0, 0);
		vertex(q2.x, q2.y, q2.z, 1, 0);
		vertex(q3.x, q3.y, q3.z, 1, 1);
		vertex(q4.x, q4.y, q4.z, 0, 1);
		
//		vertex(q1.x, q1.y, 0, 0);
//		vertex(q2.x, q2.y, 1, 0);
//		vertex(q3.x, q3.y, 1, 1);
//		vertex(q4.x, q4.y, 0, 1);
		endShape();
		
		//draw axis
		stroke(0,0,255)
		line(0,height/2,width,height/2)
		line(width/2,0,width/2,height)
		
//		if (second()+minute()*60-lasttime2>2){
//			println("");
//			println(q1.z);
//			println(q2.z);
//			println(q3.z);
//			println(q4.z);
//			lasttime2=second()+minute()*60;
//		}

	}
	override def mousePressed(){
		/* initial test
//		val depthValues = kinect.depthMap();
//		val clickPosition = mouseX + (mouseY * 640);
//		val millimeters = depthValues(clickPosition);
//		val inches = millimeters / 25.4;
//		println("mm: " + millimeters + " in: " + inches);
        */
		if (tuningCoordinate && mouseButton==LEFT){
			tuneCoordinateX = mouseX.toFloat/width.toFloat
//					println(mouseX)
			println("set tuneCoordinateX valut to " + tuneCoordinateX.toString)
			tuneCoordinateY = mouseY.toFloat/height.toFloat
//					println(mouseY)
			println("set tuneCoordinateX valut to " + tuneCoordinateY.toString)
		}
		if (tuningTranslate && mouseButton==RIGHT ){
//			tuneTranslateX = (width/2-mouseX.toFloat)/(width/2).toFloat
//			tuneTranslateY = (height/2-mouseY.toFloat)/(height/2).toFloat
			tuneTranslateX = mouseX.toFloat/width.toFloat
			tuneTranslateY = mouseY.toFloat/height.toFloat
			
			println("set tuneTranslateX valut to " + tuneCoordinateX.toString)
			println("set tuneTranslateY valut to " + tuneCoordinateY.toString)
		}
		if (tuningDepth){
		    //for tuneDepth. we use this now because the homography between homography and camera are not set yet.
			tuneDepth = mouseX.toFloat/width
			println(mouseX)
			println("set tuneDepth valut to " + tuneDepth.toString)
		}
		if (testingDepthMagnification){
			depthMagnification = mouseX.toFloat/width
			println("set depthMagnification valut to " + depthMagnification.toString)
		}
	}
	def translateProjection():PVector={
		val pi=3.1415926
		val shiftAngle=atan(dy/dx)
		val dz=cos(shiftAngle)*dx+sin(shiftAngle)*dy
//		println("")
//		println(dz.toString+dx.toString+dy.toString)
		val alpha=atan(dz/(1*centerPoint.z*perPixelPerDistance))
//		println(alpha)
		val beta=pi/2-alpha
//		println(beta)
//		println(centerPoint.z)
		var shift=centerPoint.z*cos(beta)
//		println(shift)
//		println(centerPoint.z)
//		println(perPixelPerDistance)
		shift=shift/centerPoint.z/perPixelPerDistance
//		println(shift)
		
		val shiftX=(shift*cos(shiftAngle)*tuneShift).toFloat
		val shiftY=(shift*sin(shiftAngle)*tuneShift).toFloat
		return new PVector(shiftX,shiftY)
	}
	def math(a: PVector, b: PVector, c: PVector){
	
		/** Equations Operation
			(a.x-b.x)*dx+(a.y-b.y)*dy=(a.z-b.z)
			(a.x-c.x)*dx+(a.y-c.y)*dy=(a.z-c.z)
			((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x)=((a.z-c.z)-(a.y-c.y)*dy)/(a.x-c.x)
			(a.z-b.z)/(a.x-b.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.y-c.y)*dy/(a.x-c.x)
			(a.y-c.y)*dy/(a.x-c.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x)
		*/
	
		dy=((a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x))/((a.y-c.y)/(a.x-c.x)-(a.y-b.y)/(a.x-b.x));
		dx=((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x);
	
		q1.z=a.z-((a.x-q1.x)*dx+(a.y-q1.y)*dy);
		q2.z=a.z-((a.x-q2.x)*dx+(a.y-q2.y)*dy);
		q3.z=a.z-((a.x-q3.x)*dx+(a.y-q3.y)*dy);
		q4.z=a.z-((a.x-q4.x)*dx+(a.y-q4.y)*dy);
		centerPoint.z=a.z-((a.x-centerPoint.x)*dx+(a.y-centerPoint.y)*dy);
//		convertToActoalDistance()  // for later corner use
		subtractCornerDepths()
		//magnifyConorDepthTest()
	}
	def math2(a: PVector, b: PVector, c: PVector){
		resetProjection()
		/** Equations Operation
			(a.x-b.x)*dx+(a.y-b.y)*dy=(a.z-b.z)
			(a.x-c.x)*dx+(a.y-c.y)*dy=(a.z-c.z)
			((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x)=((a.z-c.z)-(a.y-c.y)*dy)/(a.x-c.x)
			(a.z-b.z)/(a.x-b.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.y-c.y)*dy/(a.x-c.x)
			(a.y-c.y)*dy/(a.x-c.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x)
		*/
	
		dy=((a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x))/((a.y-c.y)/(a.x-c.x)-(a.y-b.y)/(a.x-b.x));
		dx=((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x);
	
		q1.z=a.z-((a.x-q1.x)*dx+(a.y-q1.y)*dy);
		q2.z=a.z-((a.x-q2.x)*dx+(a.y-q2.y)*dy);
		q3.z=a.z-((a.x-q3.x)*dx+(a.y-q3.y)*dy);
		q4.z=a.z-((a.x-q4.x)*dx+(a.y-q4.y)*dy);
		centerPoint.z=a.z-((a.x-centerPoint.x)*dx+(a.y-centerPoint.y)*dy);
//		subtractCornerDepths()
		//magnifyConorDepthTest()
		
//		counterProjection()
		translateProjection0()
		
//	    tuneDepth3()
	    coordinateProjection2()
	    tuneProjection2()
	 
	    biggestAreaProjection()
//	    println(q1.x)
//	    println(q3.x)
//	    flipProjection()
		
	}
	
	def tuneProjection3(){
//		val tuneCoordinateX2=tuneCoordinateX*abs(dy/0.5).toFloat*5
//		val tuneCoordinateY2=tuneCoordinateY*abs(dx/0.5).toFloat*5
		val tuneCoordinateX2=tuneCoordinateX
		val tuneCoordinateY2=tuneCoordinateY
		var avgX=0.toFloat
		var avgY=0.toFloat
		avgX=(q4.x-q1.x)/2*tuneCoordinateX2
//		println((q4.x-q1.x).toString+" "+tuneCoordinateX2)
//		println(q1.x.toString+" "+q4.x.toString+" avg: "+avgX)
		q1.x+=avgX
		q4.x-=avgX
//		println(q1.x.toString+" "+q4.x.toString)
		avgX=(q3.x-q2.x)/2*tuneCoordinateX2
		q2.x+=avgX
		q3.x-=avgX
		avgY=(q2.y-q1.y)/2*tuneCoordinateY2
		q1.y+=avgY
		q2.y-=avgY
		avgY=(q4.y-q3.y)/2*tuneCoordinateY2
		q3.y+=avgY
		q4.y-=avgY

	}
	def tuneProjection2(){

		val tuneCoordinateX2=tuneCoordinateX*abs(dy/10).toFloat*3000
		val tuneCoordinateY2=tuneCoordinateY*abs(dx/10).toFloat*5000
		q1.x-=tuneCoordinateX2
		q2.x+=tuneCoordinateX2
		q3.x-=tuneCoordinateX2
		q4.x+=tuneCoordinateX2
		q1.y-=tuneCoordinateY2
		q2.y-=tuneCoordinateY2
		q3.y+=tuneCoordinateY2
		q4.y+=tuneCoordinateY2

	}
	def tuneProjection(){
//	    tuneCoordinate*=cos(atan(dz)).toFloat
	    val tuneCoordinate2=tuneCoordinateX*abs(dz/10).toFloat
//	    println(q1.x)
//	    println(tuneCoordinate)
	    q1.x-=tuneCoordinate2
	    q2.x+=tuneCoordinate2
	    q3.x-=tuneCoordinate2
	    q4.x+=tuneCoordinate2
	    q1.y-=tuneCoordinate2
	    q2.y-=tuneCoordinate2
	    q3.y+=tuneCoordinate2
	    q4.y+=tuneCoordinate2
	    
//	    println(q1.x)
	}
	/* it is a wrong way to do, as it is the same with tuningDepth
	def magnifyConorDepthTest(){
		q1.z*=depthMagnification
		q2.z*=depthMagnification
		q3.z*=depthMagnification
		q4.z*=depthMagnification
	}*/
	def counterProjection(){
		q1.z*=(-1).toFloat
		q2.z*=(-1).toFloat
		q3.z*=(-1).toFloat
		q4.z*=(-1).toFloat
		centerPoint.z*=(-1).toFloat
		
	}
	def flipProjection(){
		val q1x=q1.x
		val q2x=q2.x
		val q1y=q1.y
		val q2y=q2.y
		q1.x=width/2-(q3.x-width/2)
		q2.x=width/2-(q4.x-width/2)
		q3.x=width/2-(q1x-width/2)
		q4.x=width/2-(q2x-width/2)
		q1.y=height/2-(q3.y-height/2)
		q2.y=height/2-(q4.y-height/2)
		q3.y=height/2-(q1y-height/2)
		q4.y=height/2-(q2y-height/2)
	}

	def translateProjection0(){
		val pi=3.1415926
		val shiftAngle=atan(dy/dx)
		dz=cos(shiftAngle)*dx+sin(shiftAngle)*dy
//		println("")
//		println(dz.toString+dx.toString+dy.toString)
		val alpha=atan(dz/(1*centerPoint.z*perPixelPerDistance))
//		println(alpha)
		val beta=pi/2-alpha
//		println(beta)
//		println(centerPoint.z)
		var shift=centerPoint.z*cos(beta)
//		println(shift)
//		println(centerPoint.z)
//		println(perPixelPerDistance)
		shift=shift/centerPoint.z/perPixelPerDistance
//		println(shift)
		
		val shiftX=(shift*cos(shiftAngle)).toFloat*tuneTranslateX
		val shiftY=(shift*sin(shiftAngle)).toFloat*tuneTranslateY
		q1.x+=shiftX
		q1.y+=shiftY
		q2.x+=shiftX
		q2.y+=shiftY
		q3.x+=shiftX
		q3.y+=shiftY
		q4.x+=shiftX
		q4.y+=shiftY
	}
	def biggestAreaProjection(){
		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
		val centerX=(rightX-leftX)/2+leftX
		val centerY=(lowerY-upperY)/2+upperY
		val ratioX=width/(rightX-leftX)
		val ratioY=height/(lowerY-upperY)
		val ratio=min2(ratioX,ratioY)
		var shiftX=0.toFloat
		var shiftY=0.toFloat
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-centerX)*ratio+width/2
		q1.y=(q1.y-centerY)*ratio+height/2
		q2.x=(q2.x-centerX)*ratio+width/2
		q2.y=(q2.y-centerY)*ratio+height/2
		q3.x=(q3.x-centerX)*ratio+width/2
		q3.y=(q3.y-centerY)*ratio+height/2
		q4.x=(q4.x-centerX)*ratio+width/2
		q4.y=(q4.y-centerY)*ratio+height/2
		if (ratioX<ratioY){
			if (dy>0){
				shiftX=0
				shiftY=(-1).toFloat*min2(q1.y,q2.y)
			}else{
				shiftX=0
				shiftY=min2(height-q3.y,height-q4.y)
			}
		}else{
			if (dx<0){
				shiftY=0
				shiftX=min2(width-q2.x,width-q3.x)
			}else{
				shiftY=0
				shiftX=(-1).toFloat*min2(q1.x,q4.x)
			}
		}
		q1.x+=shiftX
		q2.x+=shiftX
		q3.x+=shiftX
		q4.x+=shiftX
		q1.y+=shiftY
		q2.y+=shiftY
		q3.y+=shiftY
		q4.y+=shiftY
	}
	
	def resetProjection(){
		q1 = new PVector(0,0,0);
		q2 = new PVector(640,0,0);
		q3 = new PVector(640,480,0);
		q4 = new PVector(0,480,0);
	}
	
	def coordinateProjection1(){
		q1.x=1/(centerPoint.z/q1.z)*(q1.x-width/2)+width/2;
		q2.x=1/(centerPoint.z/q2.z)*(q2.x-width/2)+width/2;
		q3.x=1/(centerPoint.z/q3.z)*(q3.x-width/2)+width/2;
		q4.x=1/(centerPoint.z/q4.z)*(q4.x-width/2)+width/2;
		q1.y=1/(centerPoint.z/q1.z)*(q1.y-height/2)+height/2;
		q2.y=1/(centerPoint.z/q2.z)*(q2.y-height/2)+height/2;
		q3.y=1/(centerPoint.z/q3.z)*(q3.y-height/2)+height/2;
		q4.y=1/(centerPoint.z/q4.z)*(q4.y-height/2)+height/2;
		q1.z=0;
		q2.z=0;
		q3.z=0;
		q4.z=0;
	}
	def tuneDepth2(){
		q1.z+=tuneDepth*300
		q2.z+=tuneDepth*300
		q3.z+=tuneDepth*300
		q4.z+=tuneDepth*300
	}	
	def tuneDepth3(){
		dx+=(0.5-tuneDepth).toFloat
		dy+=(0.5-tuneDepth).toFloat
	}
	def coordinateProjection2(){
//		tuneDepth2()
		q1.x=(centerPoint.z/q1.z)*(q1.x-width/2)+width/2;
		q2.x=(centerPoint.z/q2.z)*(q2.x-width/2)+width/2;
		q3.x=(centerPoint.z/q3.z)*(q3.x-width/2)+width/2;
		q4.x=(centerPoint.z/q4.z)*(q4.x-width/2)+width/2;
		q1.y=(centerPoint.z/q1.z)*(q1.y-height/2)+height/2;
		q2.y=(centerPoint.z/q2.z)*(q2.y-height/2)+height/2;
		q3.y=(centerPoint.z/q3.z)*(q3.y-height/2)+height/2;
		q4.y=(centerPoint.z/q4.z)*(q4.y-height/2)+height/2;
		q1.z=0;
		q2.z=0;
		q3.z=0;
		q4.z=0;
	}

	
	
	
	def coordinateProjection0(){
		convertToActoalDistance()
//		val cornerToCenterDistance=sqrt((640/2)*640/2+(480/2)*480/2)
//		
//		
//		
//		
//		
//		val q1r=((q1.z*q1.z+centerPoint.z*centerPoint.z-2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))/(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)).toFloat
//		val q1r=(sqrt(q1.z*q1.z+centerPoint.z*centerPoint.z-2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))/(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)).toFloat
//		println(q1.z)
//		println(q1.z*q1.z)
//		println(centerPoint.z)
//		println(centerPoint.z*centerPoint.z)
//		println(cos(atan(cornerToCenterDistance*perPixelPerDistance)))
//		println(2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))
//		println(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)
//		println(q1r)
//		println("")
		
		var ratio=0.toFloat
		var distanceToCenter=0.toFloat
		
		distanceToCenter=sqrt((q1.x-width/2)*(q1.x-width/2)+(q1.y-height/2)*(q1.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q1.z*q1.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q1.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		
		q1.x=(q1.x-width/2)*ratio+width/2
		q1.y=(q1.y-height/2)*ratio+height/2
		q1.z=0
				
		distanceToCenter=sqrt((q2.x-width/2)*(q2.x-width/2)+(q2.y-height/2)*(q2.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q2.z*q2.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q2.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q2.x=(q2.x-width/2)*ratio+width/2
		q2.y=(q2.y-height/2)*ratio+height/2
		q2.z=0
		
		distanceToCenter=sqrt((q3.x-width/2)*(q3.x-width/2)+(q3.y-height/2)*(q3.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q3.z*q3.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q3.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q3.x=(q3.x-width/2)*ratio+width/2
		q3.y=(q3.y-height/2)*ratio+height/2
		q3.z=0
				
		distanceToCenter=sqrt((q4.x-width/2)*(q4.x-width/2)+(q4.y-height/2)*(q4.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q4.z*q4.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q4.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q4.x=(q4.x-width/2)*ratio+width/2
		q4.y=(q4.y-height/2)*ratio+height/2
		q4.z=0
	}
	
	def convertToActoalDistance(){
		q1.z=q1.z/cos(atan(sqrt(q1.x*q1.x+q1.y*q1.y)*perPixelPerDistance)).toFloat
		q2.z=q2.z/cos(atan(sqrt(q2.x*q2.x+q2.y*q2.y)*perPixelPerDistance)).toFloat
		q3.z=q3.z/cos(atan(sqrt(q3.x*q3.x+q3.y*q3.y)*perPixelPerDistance)).toFloat
		q4.z=q4.z/cos(atan(sqrt(q4.x*q4.x+q4.y*q4.y)*perPixelPerDistance)).toFloat
	}
	def subtractCornerDepths(){
		val maxx=max4(q1.z,q2.z,q3.z,q4.z)
				q1.z-=maxx;
		q2.z-=maxx;
		q3.z-=maxx;
		q4.z-=maxx;
	}
	def min2(a: Float, b: Float):Float={
		return minArray(Array(a,b))
	}
	def min3(a: Float, b: Float, c: Float):Float={
		return minArray(Array(a,b,c))
	}
	def min4(a: Float, b:Float, c:Float, d:Float):Float={
		return minArray(Array(a,b,c,d))
	}
	def minArray(input: Array[Float]):Float={
		var minn = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)<minn){minn=input(i)}
		}
		return minn
	}
	def max4(a: Float, b:Float, c:Float, d:Float):Float={
		return maxArray(Array(a,b,c,d))
	}
	def maxArray(input: Array[Float]):Float={
		var maxx = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)>maxx){maxx=input(i)}
		}
		return maxx
	}	
}