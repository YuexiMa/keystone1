import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet._
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library._;
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
import javax.media.opengl._;
import processing.opengl._;

object calibrate1 extends PApplet{
	var calibratingDepthWithProj=false
	var calibratingRGBwithDepthAndProj=true
	var testingDepthWithProj=false
	var testingRGBWithDepth=false
	var count=0
	var kinect: SimpleOpenNI  = null
	var homographyMatrix = Matrix.zeros[Double](3,3)
	//val imagee = loadImage("123.png")


	
		var proj1=new PVector(0,0)
		var proj2=new PVector(640,0)
		var proj3=new PVector(640,480)
		var proj4=new PVector(0,480)
		var cam1=new PVector(98,102)
		var cam2=new PVector(255,96)
		var cam3=new PVector(255,204)
		var cam4=new PVector(98,198)
		val proj=Array[PVector](proj1,proj2,proj3,proj4)
		val cam=Array[PVector](cam1,cam2,cam3,cam4)
	
		var p=new PVector(0,0)
	
	def main(args: Array[String]) {

		val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(calibrate1)
		calibrate1.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
	}


	override def setup() {
		size(640*2, 480,P3D)

		kinect = new SimpleOpenNI(this);

//		context.enableRGB();
//		context.enableDepth();
		
		// enable depthMap generation 
		if(kinect.enableDepth() == false)
		{
			println("Can't open the depthMap, maybe the camera is not connected!"); 
			exit();
			return;
		}

		// enable ir generation
		//context.enableRGB(640,480,30);
		//context.enableRGB(1280,1024,15);  
		if(kinect.enableRGB() == false)
		{
			println("Can't open the rgbMap, maybe the camera is not connected or there is no rgbSensor!"); 
			exit();
			return;
		}

		
		//Homography
		//val cam=Array[Double](1,1,2,1,2,2,1,2)//unit squire
		//val proj=Array[Double](0.8,1.2,2.5,0.6,2.5,2.4,0.8,1.8)//T-shape
		//      size(600,600,OPENGL)
		//size(800,800,P3D)
		//      pointTest1()
		//      homographyMatrix=inv(homographyMatrix)
	}

	override def draw() {
		//set(1,1,color(0,255,0))
		//image(applyToPImage(imagee),0,0,400,300)
		//line(120,120, 180,120)
		//val transformedCoordinates=applyToCoordinates(Array[Double](120,120,180,120))
		//line(transformedCoordinates(0).toInt,transformedCoordinates(1).toInt,transformedCoordinates(2).toInt,transformedCoordinates(3).toInt)
		//stop()
		//println(1)
		//delay(1000)
		//image(imagee,0,0,width,height)
		//println(2)
		//delay(1000)

		//image(applyToPImage2(imagee),0,0,imagee.width,imagee.height)

		//	  noStroke();
		//	  textureMode(NORMALIZED);
		//	  beginShape(QUADS);
		//	  texture(imagee);
		//	  val corner1=applyToCoordinate3(0,0)
		//	  val corner2=applyToCoordinate3(width,0)
		//	  val corner3=applyToCoordinate3(width,height)
		//	  val corner4=applyToCoordinate3(0,height)
		//	  vertex(corner1(0), corner1(1), 0.toFloat, 0.toFloat);
		//	  vertex(corner4(0), corner4(1), 0.toFloat, 1.toFloat);
		//	  vertex(corner3(0), corner3(1), 1.toFloat, 1.toFloat);	 
		//	  vertex(corner2(0), corner2(1), 1.toFloat, 0.toFloat); 
		//	  vertex(0.toFloat,0.toFloat,0.toFloat, 0.toFloat, 0.toFloat);
		//	  vertex(width.toFloat,0.toFloat,-300.toFloat, 1.toFloat, 0.toFloat);
		//	  vertex(width.toFloat,height.toFloat,(-600).toFloat, 1.toFloat, 1.toFloat);
		//	  vertex(0.toFloat,height.toFloat,(-300).toFloat, 0.toFloat, 1.toFloat);

		//	  val res=50
		//	  for (x <- 0 to (width/res).toInt) {
		//			for (y <- 0 to (height/res).toInt) { 
		//				  val corner1=applyToCoordinate3(0,0)
		//				  val corner2=applyToCoordinate3(width,0)
		//				  val corner3=applyToCoordinate3(width,height)
		//				  val corner4=applyToCoordinate3(0,height)
		//			}
		//	  }


		// update the cam
		kinect.update();

		background(255,255,255);

		// draw depthImageMap
		image(kinect.depthImage(),0,0);

		// draw irImageMap
		image(kinect.rgbImage(),kinect.depthWidth(),0);
		fill(0,255,0)
		ellipse(p.x,p.y,20,20)
	}
	override def mouseClicked(){
//		if (calibratingDepthWithProj && count<4){
//			cam(count)=new PVector(mouseX,mouseY)
//			println(count)
//			count+=1
//			if (count==4){
//				calibratingDepthWithProj=false
//				computeHomography(cam,proj)
//				writeHomography("depth-projector homography.txt")
//			}	
//		}
  		if (calibratingDepthWithProj && count<4){
		    println("selected corner in RGB")
			homographyMatrix=loadHomography2("rgb-depth homography.txt")
			cam(count)=applyToPoint(new PVector(mouseX-640,mouseY))
//			cam(count)=new PVector(mouseX-640,mouseY)
			println(count)
			count+=1
			if (count==4){
				proj(0)=new PVector(0,0)
				proj(1)=new PVector(848,0)
				proj(2)=new PVector(848,480)
				proj(3)=new PVector(0,480)
				
				calibratingDepthWithProj=false
				homographyMatrix=computeHomography2(cam,proj)
				writeHomography("depth-projector homography.txt")
				testingDepthWithProj=true
				println("start testing")
			}	
		}
		println(count)
		
		if(calibratingRGBwithDepthAndProj){
			//select points in RGB and then in Depth
			if (count%2==0){
				println("selected in rgb")
				cam(count/2)=new PVector(mouseX-640,mouseY)
			}else{
				println("selected in depth")
				proj((count-1)/2)=new PVector(mouseX,mouseY)
			}
			count+=1
			if (count==8){
				calibratingRGBwithDepthAndProj=false
				homographyMatrix=computeHomography2(cam,proj)
				writeHomography("rgb-depth homography.txt")
				calibratingDepthWithProj=true
//				testingRGBWithDepth=true
				count=0
			}
		}
		
		if (testingDepthWithProj){
			homographyMatrix=loadHomography2("depth-projector homography.txt")
			
			p=applyToPoint(new PVector(mouseX,mouseY))
			println(p)
		}
		if (testingRGBWithDepth){
			homographyMatrix=loadHomography2("rgb-depth homography.txt")
			
			p=applyToPoint(new PVector(mouseX-640,mouseY))
			println(p)
		}
		
	}
	def applyToPoint2(inputPoint: PVector):PVector={
			var X=inputPoint.x
					var Y=inputPoint.y
					var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
					for(i <- 0 to 2){
						for(j <- 0 to 2){
							p(i+3*j+1)=homographyMatrix(i,j)
						}
					}

			val x:Float=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
					val y:Float=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat
					val outputPoint: PVector=new PVector(x,y,inputPoint.z)
					return outputPoint
	}
	def applyToPoint(inputPoint: PVector):PVector={
			var X=inputPoint.x
					var Y=inputPoint.y
					var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
					for(i <- 0 to 2){
						for(j <- 0 to 2){
							p(i+3*j+1)=homographyMatrix(i,j)
						}
					}

			val x:Float=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
					val y:Float=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat
					val outputPoint: PVector=new PVector(x,y)
					return outputPoint
	}
	def pointExample2(){//this version uses PVector to compute homography
		val proj1=new PVector(100,100)
		val proj2=new PVector(200,100)
		val proj3=new PVector(200,200)
		val proj4=new PVector(100,200)
		val cam1=new PVector(98,102)
		val cam2=new PVector(255,96)
		val cam3=new PVector(255,204)
		val cam4=new PVector(98,198)
		val proj=Array[PVector](proj1,proj2,proj3,proj4)//unit squire
		val cam=Array[PVector](cam1,cam2,cam3,cam4)

		homographyMatrix=computeHomography2(cam,proj)

		val testPoint=new PVector(100,100)
		println(applyToPoint(testPoint))
	}
	def writeHomography(fileName: String){
		val file = new PrintWriter(new File(fileName));
		var content: java.lang.String=""
				for(i <- 0 to 2){
					for(j <- 0 to 2){
						content = homographyMatrix(i,j).toString
								file.write(content)
								file.write("\n")
					}
				}
		file.close();
	}    
	def loadHomography2(fileName: String): scalala.tensor.dense.DenseMatrix[Double] = {
		var item:String=""
		var column=0
		var row=0
		var homographyMatrix = Matrix.zeros[Double](3,3)
		val file = Source.fromFile(fileName)
		file.getLines.foreach((line) =>{
			item=line
					homographyMatrix(row,column)=item.toDouble
					column+=1
					if (column==3){
						column=0
						row+=1
					}
		})
		return (homographyMatrix)
	}
	def loadHomography(fileName: String) {
		var item:String=""
				var column=0
				var row=0
				var homographyMatrix = Matrix.zeros[Double](3,3)
				val file = Source.fromFile(fileName)
				file.getLines.foreach((line) =>{
					item=line
							homographyMatrix(row,column)=item.toDouble
							column+=1
							if (column==3){
								column=0
								row+=1
							}
				})
	}


	def computeHomography2(cameraPoints: Array[PVector], projectorPoints: Array[PVector]): scalala.tensor.dense.DenseMatrix[Double] ={  
		if (cameraPoints.length!=projectorPoints.length){println("Different points amount bewteen camera and projector image! Please have a double check.")}
		var numberOfPoints: Int = (cameraPoints.length)

				//initializing scalala matrixs cam and proj
				var cam = Matrix.zeros[Double](numberOfPoints,2)
				var proj = Matrix.zeros[Double](numberOfPoints,2)
				//assign values from input array to cam and proj
				for (i <- 0 to (numberOfPoints-1)){
					cam(i,0)=cameraPoints(i).x.toDouble
							cam(i,1)=cameraPoints(i).y.toDouble
							proj(i,0)=projectorPoints(i).x.toDouble
							proj(i,1)=projectorPoints(i).y.toDouble
				}

		// Creates the estimation matrix
		var matrix1 = Matrix.zeros[Double](numberOfPoints*2,9)
				//initializing row1 and row2
				var row1 = DenseVector.zeros[Double](9)
				var row2 = DenseVector.zeros[Double](9)
				//assign estimation matrix entries values from cam and proj 
				for (i <- 0 to (numberOfPoints-1)){
					row1 = DenseVector(cam(i,0), cam(i,1), 1, 0, 0, 0, -cam(i,0)*proj(i,0), -cam(i,1)*proj(i,0), -proj(i,0))
							row2 = DenseVector(0, 0, 0, cam(i,0), cam(i,1), 1, -cam(i,0)*proj(i,1), -cam(i,1)*proj(i,1), -proj(i,1))
							matrix1(i*2,::) := row1
							matrix1(i*2+1,::) := row2
				}

		//calculation eigenvalues and eigenvectors
		var matrix2 = matrix1.t * matrix1		
				val (eigenvalues,valuee0,eigenvectors)=eig(matrix2)


				//find the minium eigenvalue position
				var minPosition=0
				var minValue=eigenvalues(0)
				for (i <- 0 to 8){
					if (eigenvalues(i)<minValue){
						minValue=eigenvalues(i)
								minPosition=i
					}
				}

		//get the eigenvector correspond to the minumn eigenvalue
		var eigenvector = eigenvectors(::,minPosition)

				//write the eigenvector to homography matrix
				for(i <- 0 to 2){
					for(j <- 0 to 2){
						homographyMatrix(i,j)=eigenvector(i+3*j)
					}
				}

		//invert matrix for applying to coordinates
		//homographyMatrix=inv(homographyMatrix)
		return homographyMatrix
	}	



	def applyToCoordinates(inputCoordinates: Array[Double]):Array[Double]={
			var outputCoordinates=inputCoordinates
					val numberOfPoints=(inputCoordinates.length)/2

					//initializing
					var X=0.0
					var Y=0.0
					var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
					for(i <- 0 to 2){
						for(j <- 0 to 2){
							p(i+3*j+1)=homographyMatrix(i,j)
						}
					}

			for (i <- 0 to (numberOfPoints-1)){
				X=inputCoordinates(i*2)
						Y=inputCoordinates(i*2+1)

						outputCoordinates(i*2)=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
						outputCoordinates(i*2+1)=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))
			}



			//val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
			//val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))


			return outputCoordinates
	}


	def applyToCoordinate(inputCoordinate: Array[Double]):Array[Double]={
			val X=inputCoordinate(0)
					val Y=inputCoordinate(1)
					var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
					for(i <- 0 to 2){
						for(j <- 0 to 2){
							p(i+3*j+1)=homographyMatrix(i,j)
						}
					}

			val x:Double=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
					val y:Double=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))

					//val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
					//val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))


					return Array(x,y)
	}

	def applyToCoordinate3(XX: Int, YY: Int):Array[Float]={
			val X=XX.toDouble
					val Y=YY.toDouble
					var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
					for(i <- 0 to 2){
						for(j <- 0 to 2){
							p(i+3*j+1)=homographyMatrix(i,j)
						}
					}

			val x=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
					val y=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat

					//val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
					//val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))


					return Array(x,y)
	}

	def applyToCoordinate2(inputCoordinate: Array[Double]):Array[Double]={
			val cameraFrame=DenseVector[Double](inputCoordinate(0),inputCoordinate(1),1.toDouble)
					val projectorFrame=homographyMatrix * cameraFrame

					return Array(projectorFrame(0)/projectorFrame(2),projectorFrame(1)/projectorFrame(2))
	}  



}












