//version 9.0
/**notes
 * 1. for the right side rotation problem, it may be happening from the ratio control part? no, it happens even when i am not using biggest projection.
 * 2. infinity bug solution not confirmed. ??
 * 3. constrain function solves the enlarging ratio problem.
 * 4. constrain can toggle from homography application part.
 * 
 * 
 * 120723`1`1602 antikeystone demo
 * 				 applymatrix
 * 				 different return methods
 */

import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _, abs=>_, _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
/* For OpenGl
 * 1. put jogl.jar and gluegen-rt.jar from processing libiray: H:\Program Files\processing-1.5.1\modes\java\libraries\opengl\library\windows32
 * 2. set jogl.jar's native library to where it comes from.
 * 3. OR, find jogl.dll and jogl_awt.dll and put them in the lib and set native library to it.
 */
//import processing.opengl._;

class antiKeystoneClass extends PApplet{
	val tuningDepth=false
	val testingDepthMagnification=false
	val tuningCoordinate=false
	val tuningTranslate=false
//    var context: SimpleOpenNI  = null;
	var kinect: SimpleOpenNI  = null;
//	var tuneDepth = -1.toFloat//(0.6375).toFloat;
	var tuneDepth = (0.6375).toFloat
	var tuneShift: Float = 1.5.toFloat
//	var tuneCoordinateX: Float = 843.toFloat
//	var tuneCoordinateY: Float = 2831.toFloat
	var tuneCoordinateX: Float = 0.3328.toFloat
	var tuneCoordinateY: Float = 0.1375.toFloat
	var tuneTranslateX: Float = 1.toFloat
	var tuneTranslateY: Float = 1.toFloat
	var depthMagnification = 1.toFloat
	
//	var q1:PVector = new PVector(0,0,0);
//	var q2:PVector = new PVector(600,0,0);
//	var q3:PVector = new PVector(600,450,0);
//	var q4:PVector = new PVector(0,450,0);
	val qqx=640/4
	val qqy=480/4
	var q1:PVector = new PVector(0+qqx,0+qqy,0)
	var q2:PVector = new PVector(640-qqx,0+qqy,0)
	var q3:PVector = new PVector(640-qqx,480-qqy,0)
	var q4:PVector = new PVector(0+qqx,480-qqy,0)
	var ctp:PVector = new PVector(640/2,480/2,0)//getCenterPoint(q1,q2,q3,q4)
	println(ctp)
	var a1:PVector = new PVector(0+qqx,0+qqy,0)
	var a2:PVector = new PVector(640-qqx,0+qqy,0)
	var a3:PVector = new PVector(640-qqx,480-qqy,0)
	var a4:PVector = new PVector(0+qqx,480-qqy,0)
	var ctpp:PVector = new PVector(640/2,480/2,0)//getCenterPoint(a1,a2,a3,a4)
	
	var qqqq1=q1//storage veriables
	var qqqq2=q2
	var qqqq3=q3
	var qqqq4=q4

	var qqqqq1=q1//storage veriables
	var qqqqq2=q2
	var qqqqq3=q3
	var qqqqq4=q4
	
	var qqqqqq1=q1//storage veriables
	var qqqqqq2=q2
	var qqqqqq3=q3
	var qqqqqq4=q4
	
	var camm:Array[PVector]=null
	
	var diff1=0.toFloat
	var diff2=0.toFloat
	var diff3=0.toFloat
	var diff4=0.toFloat
	
	var centerPoint = new PVector(600/2, 450/2, 0)
	var pointt = 0;
	var lasttime=second()+minute()*60;
	var lasttime2=second()+minute()*60;
  
	var  dx:Float = 0.toFloat;
	var  dy:Float = 0.toFloat;
	var  dz:Double = 0.0;
	
	val perPixelPerDistance: Float=1045.toFloat/966/600

	val imagee=loadImage("bardcmsc7white.png")	
//	val aspectRatio=imagee.height.toFloat/imagee.width
//	val whitePage = createGraphics(imagee.width,imagee.height,P2D)
	val whitePage = createGraphics(848,480,P2D)
	val aspectRatio=480.toFloat/848
	whitePage.beginDraw()
//	whitePage.image(imagee,848/2-480/2,0)
	whitePage.image(imagee,848/2,0)
//	whitePage.image(imagee,0,0)
	whitePage.endDraw()
//	whitePage.loadPixels()
//	for (i <- 0 to 848*480-1){whitePage.pixels(i)=color(255)}
//	for (i <- 0 to 848-1) {whitePage.pixels(i+480/2*848)=color(255,0,0)}
//	for (i <- 0 to 480-1) {whitePage.pixels(848/2+i*848)=color(255,0,0)}
	
	var calibration=new cameraTransformationClass()
	var homography=new homographyClass()
//	var maximizer = new maximizeProjectionClass()
	
	var offscreen=createGraphics(848,480,P3D)
	var depthValues:Array[Int]=null
	var demo=true
	
	def setRange(p1:PVector, p2:PVector, p3:PVector, p4:PVector) {
		a1=p1
		a2=p2
		a3=p3
		a4=p4
	    ctpp = new PVector(640/2,480/2,0)//getCenterPoint(a1,a2,a3,a4)
	}
	def antiKeystone(p1:PVector, p2:PVector, p3:PVector, p4:PVector, depthmap:Array[Int]):PImage= {
		    depthValues=depthmap
			setRange(p1,p2,p3,p4)
			predictKeystone()
										println("after predict"+q1+q2+q3+q4)
			killKeystone2()
											println("after kill"+q1+q2+q3+q4)
			applyToProjector()
													println("after apply"+q2)
			
			

//		  val proj1=new PVector(0,0)
//		  val proj2=new PVector(848,0)
//	      val proj3=new PVector(848,480)
//		  val proj4=new PVector(0,480)
//	      val proj=Array[PVector](proj1,proj2,proj3,proj4)
//		  val cam=Array[PVector](q1,q1,q3,q4)
//	      homography.computeHomography(proj,cam)
//	      val homographyMatrix=homography.homographyMatrix
//	      offscreen.applyMatrix(  homographyMatrix(0,0).toFloat, homographyMatrix(0,1).toFloat, homographyMatrix(0,2).toFloat, 0,
//					    		  homographyMatrix(1,0).toFloat, homographyMatrix(1,1).toFloat, homographyMatrix(1,2).toFloat, 0,
//					    		  homographyMatrix(2,0).toFloat, homographyMatrix(2,1).toFloat, homographyMatrix(2,2).toFloat, 0,
//					    		  0.toFloat, 0.toFloat, 1.toFloat, 0.toFloat)
//		  offscreen.image(whitePage,0,0)
					    		  
//		offscreen.beginDraw()
//		offscreen.background(0)
//		offscreen.beginShape();
//		offscreen.textureMode(NORMALIZED);
//////		texture(imagee);
//		offscreen.texture(whitePage)
//		offscreen.vertex(q1.x, q1.y, 0, 0);
//		offscreen.vertex(q2.x, q2.y, 1, 0);
//		offscreen.vertex(q3.x, q3.y, 1, 1);
//		offscreen.vertex(q4.x, q4.y, 0, 1);
//		offscreen.endShape();
//		  
//		offscreen.endDraw()
//																println("before pass"+q2)
		drawOnShape(whitePage,q1,q2,q3,q4,30)
		println("returning offscreen")
		return offscreen
	}
	
	def drawOnShape(imageee:PImage, a:PVector, b:PVector, c:PVector, d:PVector, resolution:Int){
//																println("after pass"+b)
		println("draw on screen")
		  val proj1=new PVector(0,0)
		  val proj2=new PVector(848,0)
	      val proj3=new PVector(848,480)
		  val proj4=new PVector(0,480)
	      val proj=Array[PVector](proj1,proj2,proj3,proj4)
		  val cam=Array[PVector](a,b,c,d)
		  println("computing homography")
		  println(cam)
		  if (a.x<10000 &&a.y<10000 && b.x<10000 &&b.y<10000 && c.x<10000 &&c.y<10000 && d.x<10000 &&d.y<10000) {
		      homography.computeHomography(proj,cam)
		      println("homography computed")
		      val stepX=imageee.width/resolution
		      val stepY=imageee.height/resolution
//	      println("step"+stepX+"     "+stepY)
		      val sizeX=imageee.width.toFloat/stepX
		      val sizeY=imageee.height.toFloat/stepY
		      var pointt:PVector=null
//	      println(stepX+" " + stepY)
//		  println(sizeX+"size"+sizeY)
	      
		      if (sizeX>1&&sizeY>1&&stepX>1&&stepY>1) {
			      offscreen.beginDraw()
			      offscreen.background(0)
			      for (m<- 0 to stepX-1) {
			      	for (n<-0 to stepY-1) {
			      		var i=m.toFloat
			      		var j=n.toFloat
			      		
			      		offscreen.beginShape()
			      		offscreen.noStroke()
			      		offscreen.texture(imageee)
			      		pointt=homography.applyToPoint(new PVector(sizeX*i,sizeY*j))
			      		offscreen.vertex(pointt.x,pointt.y,sizeX*i,sizeY*j)
			      		pointt=homography.applyToPoint(new PVector(sizeX*(i+1),sizeY*j))
			      		offscreen.vertex(pointt.x,pointt.y,sizeX*(i+1),sizeY*j)
			      		pointt=homography.applyToPoint(new PVector(sizeX*(i+1),sizeY*(j+1)))
			      		offscreen.vertex(pointt.x,pointt.y,sizeX*(i+1),sizeY*(j+1))
			      		pointt=homography.applyToPoint(new PVector(sizeX*i,sizeY*(j+1)))
			      		offscreen.vertex(pointt.x,pointt.y,sizeX*i,sizeY*(j+1))
			      		offscreen.endShape()
	      		
//	      		i=m+0.5.toFloat
//	      		j=n
	      		
//	      		offscreen.beginShape()
//	      		offscreen.texture(imageee)
//	      		pointt=homography.applyToPoint(new PVector(sizeX*i-3,sizeY*j-3))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*i-3,sizeY*j-3)
//	      		pointt=homography.applyToPoint(new PVector(sizeX*(i+1)+3,sizeY*j-3))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*(i+1)+3,sizeY*j-3)
//	      		pointt=homography.applyToPoint(new PVector(sizeX*(i+1)+3,sizeY*(j+1)+3))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*(i+1)+3,sizeY*(j+1)+3)
//	      		pointt=homography.applyToPoint(new PVector(sizeX*i-3,sizeY*(j+1)+3))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*i-3,sizeY*(j+1)+3)
//	      		offscreen.endShape()
	      		
	      		
//	      		i=m
//	      		j=n+0.5.toFloat
//	      		
//	      		offscreen.beginShape()
//	      		offscreen.texture(imageee)
//	      		pointt=homography.applyToPoint(new PVector(sizeX*i,sizeY*j))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*i,sizeY*j)
//	      		pointt=homography.applyToPoint(new PVector(sizeX*(i+1),sizeY*j))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*(i+1),sizeY*j)
//	      		pointt=homography.applyToPoint(new PVector(sizeX*(i+1),sizeY*(j+1)))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*(i+1),sizeY*(j+1))
//	      		pointt=homography.applyToPoint(new PVector(sizeX*i,sizeY*(j+1)))
//	      		offscreen.vertex(pointt.x,pointt.y,sizeX*i,sizeY*(j+1))
//	      		offscreen.endShape()
			      	}
			      }
			      
			      offscreen.fill(255)
			      offscreen.rect(0,0,848,50)
			      offscreen.rect(0,50,50,480)
			      offscreen.rect(50,430,848,480)
			      offscreen.rect(798,20,848,430)
			      offscreen.endDraw()
		      }else {
		      	println("problem!!!!!!!!!!!!!")
		      }
		  }
	}
	
	def main(args: Array[String]) {

		
							
//						var b1:PVector = new PVector(240,30,0)
//						var b2:PVector = new PVector(360,30,0)
//						var b3:PVector = new PVector(420,300,0)
//						var b4:PVector = new PVector(200,300,0)
//						setRange(b1,b2,b3,b4)
		
		
//						println(a1)
						
//		
//		
//		
//		val frame = new javax.swing.JFrame("TITLE")
//		frame.getContentPane().add(antiKeystoneClass)
//		antiKeystoneClass.init
//		frame.setResizable(true)
//		frame.pack
//		frame.setVisible(true) 
	}
//	override def setup() {
//		/*
////		context = new SimpleOpenNI(this);
////
////		// mirror is by default enabled
////		context.setMirror(true);
////		// enable depthMap generation 
////		if(context.enableDepth() == false)
////		{
////			println("Can't open the depthMap, maybe the camera is not connected!"); 
////			exit();
////			return;
////		}
////		if(context.enableRGB() == false)
////		{
////			println("Can't open the rgbMap, maybe the camera is not connected or there is no rgbSensor!"); 
////			exit();
////			return;
////		}
////
////		size(context.depthWidth() + context.rgbWidth() + 10, context.rgbHeight());
//		*/
//	  
//		size(848, 480,OPENGL);
//		kinect = new SimpleOpenNI(this);
//		kinect.enableRGB();
//		kinect.enableDepth();
//	}
//
//	override def draw() {
//		/* initial test
////		// update the cam
////		context.update();
////
////		background(255,255,255);
////
////		// draw depthImageMap
////		image(context.depthImage(),0,0);
////
////		// draw rgbImageMap
////		image(context.rgbImage(),context.depthWidth() + 10,0);
//		*/
//		
//		kinect.update();
//		background(0);
//		val imagee = kinect.rgbImage();
//		
//		//draw axis
//		imagee.loadPixels()
//		for (i <- 0 to width-1){imagee.pixels(i+imagee.height/2*imagee.width)=color(0,255,0)}
//		for (i <- 0 to height-1){imagee.pixels(imagee.width/2+i*imagee.width)=color(0,255,0)}
//		imagee.updatePixels()
//		//
//		
////		image(imagee, 0, 0);
//		
//		//calculate corner depth
//		if (second()+minute()*60-lasttime>0.5){
////			val depthValues = kinect.depthMap();
////			var a = new PVector(200,150,0);
////			var b = new PVector(400,260,0);
////			var c= new PVector(250, 300,0);
////			a.z=(-1)*depthValues((a.x+a.y*600).toInt)//*tuneDepth;
////			b.z=(-1)*depthValues((b.x+b.y*600).toInt)//*tuneDepth;
////			c.z=(-1)*depthValues((c.x+c.y*600).toInt)//*tuneDepth;
//
//			
//			
////			println(depthValues((centerPoint.x+centerPoint.y*600).toInt))
//
////			q1.z=(-1)*depthValues((q1.x+100+(q1.y+100)*600).toInt)*tuneDepth;
////			q2.z=(-1)*depthValues((q2.x-100+(q2.y+100)*600).toInt)*tuneDepth;
////			q3.z=(-1)*depthValues((q3.x-100+(q3.y-100)*600).toInt)*tuneDepth;
////			q4.z=(-1)*depthValues((q4.x+100+(q4.y-100)*600).toInt)*tuneDepth;
////			subtractCornerDepths()
//					
//			/* [replaced by subtractConorDepth().]
//				val adjust=min3(a.z,b.z,c.z)+150;
//				a.z=a.z-adjust;
//				b.z=b.z-adjust;
//				c.z=c.z-adjust;
//			*/
////			math3(a,b,c);
//			predictKeystone()
//			killKeystone2()
//			applyToProjector()
//			lasttime=second()+minute()*60;
//		}
//		
//		//drawing
//		stroke(0,255,0);
////		background(0);
//		//    fill(0,255,0);
//		//    rect(0+100,100,width-100,height-100);
////		translate(-tuneDepth*centerPoint.z*dx,-tuneDepth*centerPoint.z*dy)
////		translate(centerPoint.z*dx,centerPoint.z*dy)
////		translate(0,3*(mouseY-height/2))
////		translate(translateProjection().x,translateProjection().y)
//		beginShape();
//		textureMode(NORMALIZED);
////		texture(imagee);
//		texture(whitePage)
////		vertex(q1.x, q1.y, q1.z, 0, 0);
////		vertex(q2.x, q2.y, q2.z, 1, 0);
////		vertex(q3.x, q3.y, q3.z, 1, 1);
////		vertex(q4.x, q4.y, q4.z, 0, 1);
//		vertex(q1.x, q1.y, 0, 0);
//		vertex(q2.x, q2.y, 1, 0);
//		vertex(q3.x, q3.y, 1, 1);
//		vertex(q4.x, q4.y, 0, 1);
//		endShape();
//		
//		//draw axis
//		stroke(0,0,255)
//		line(0,height/2,width,height/2)
//		line(width/2,0,width/2,height)
//		stroke(0,255,0)
//		line(a1.x,a1.y,a2.x,a2.y)
//		line(a1.x,a1.y,a4.x,a4.y)
//		line(a3.x,a3.y,a2.x,a2.y)
//		line(a3.x,a3.y,a4.x,a4.y)
////		if (second()+minute()*60-lasttime2>2){
////			println("");
////			println(q1.z);
////			println(q2.z);
////			println(q3.z);
////			println(q4.z);
////			lasttime2=second()+minute()*60;
////		}
//		
//
//	}
//	override def mousePressed(){
//		/* initial test
////		val depthValues = kinect.depthMap();
////		val clickPosition = mouseX + (mouseY * 600);
////		val millimeters = depthValues(clickPosition);
////		val inches = millimeters / 25.4;
////		println("mm: " + millimeters + " in: " + inches);
//        */
//		if (tuningCoordinate && mouseButton==LEFT){
//			tuneCoordinateX = mouseX.toFloat/width.toFloat
////					println(mouseX)
//			println("set tuneCoordinateX valut to " + tuneCoordinateX.toString)
//			tuneCoordinateY = mouseY.toFloat/height.toFloat
////					println(mouseY)
//			println("set tuneCoordinateX valut to " + tuneCoordinateY.toString)
//		}
//		if (tuningTranslate && mouseButton==RIGHT ){
////			tuneTranslateX = (width/2-mouseX.toFloat)/(width/2).toFloat
////			tuneTranslateY = (height/2-mouseY.toFloat)/(height/2).toFloat
//			tuneTranslateX = mouseX.toFloat/width.toFloat
//			tuneTranslateY = mouseY.toFloat/height.toFloat
//			
//			println("set tuneTranslateX valut to " + tuneCoordinateX.toString)
//			println("set tuneTranslateY valut to " + tuneCoordinateY.toString)
//		}
//		if (tuningDepth){
//		    //for tuneDepth. we use this now because the homography between homography and camera are not set yet.
//			tuneDepth = mouseX.toFloat/width
//			println(mouseX)
//			println("set tuneDepth valut to " + tuneDepth.toString)
//		}
//		if (testingDepthMagnification){
//			depthMagnification = mouseX.toFloat/width
//			println("set depthMagnification valut to " + depthMagnification.toString)
//		}
//	}	
	
	def killKeystone2() {
		println("kill keystone")
		pseudoTranslation2()
		
		
//	 q1 = new PVector(458.9302.toFloat, 170.98877.toFloat, 754.22565.toFloat);
//	 q2 = new PVector(1041.435.toFloat, 1.0.toFloat, 717.5185.toFloat);
//	 q3 = new PVector( 758.6742.toFloat, 915.5791.toFloat, 770.22345.toFloat);
//	 q4 = new PVector(320.73956.toFloat, 981.70844.toFloat, 774.22656.toFloat);
		
		
		val wall=Array[PVector](q1,q2,q3,q4)
		var maximizer = new maximizeProjectionClass()
		println("before maximize"+q1+q2+q3+q4)
		val maxArea=maximizer.maximize(q1,q2,q3,q4,aspectRatio)
		println("after maximize"+maxArea(0)+maxArea(1)+maxArea(2)+maxArea(3))
//		val maxArea=wall
		resetProjection3()
		var cam=Array[PVector](q1,q2,q3,q4)
				

		
		homography.computeHomography(wall,cam)
		
											val testpoint=q2
		
		q1=homography.applyToPoint(maxArea(0))
		q2=homography.applyToPoint(maxArea(1))
		q3=homography.applyToPoint(maxArea(2))
		q4=homography.applyToPoint(maxArea(3))
		
											if (q2.x<10000) {}else {println(testpoint);println(maxArea(1))}
											println(q2)
		
		val projectionSize=maxArea(0).z
		
		//		if (q1==a1 &&q2==a2 &&q3==a3&&q4==a4) {
		val difff1=(q1.x-qqqq1.x)*(q1.x-qqqq1.x)+(q1.y-qqqq1.y)*(q1.x-qqqq1.x)
		val difff2=(q2.x-qqqq2.x)*(q2.x-qqqq2.x)+(q2.y-qqqq2.y)*(q1.x-qqqq2.x)
		val difff3=(q3.x-qqqq3.x)*(q3.x-qqqq3.x)+(q3.y-qqqq3.y)*(q1.x-qqqq3.x)
		val difff4=(q4.x-qqqq4.x)*(q4.x-qqqq4.x)+(q4.y-qqqq4.y)*(q1.x-qqqq4.x)
		var threashold=0.toFloat
		if (demo) {
			threashold =(abs(q2.x-q1.x)).toFloat
		}else {
			threashold=1200.toFloat
		}
											println(q2)
		if (projectionSize==0) {
				q1=qqqq1
				q2=qqqq2
				q3=qqqq3
				q4=qqqq4
				
														println("1111111 zero area")
				
		}else {
			 if (abs(difff1-diff1)<threashold && abs(difff2-diff2)<threashold && abs(difff3-diff3)<threashold && abs(difff4-diff4)<threashold) {
				q1=qqqq1
				q2=qqqq2
				q3=qqqq3
				q4=qqqq4
				
																				println("22222222 shaking detected")
			 }else {
				 												println("33333333   valid new position")
				qqqq1=q1
				qqqq2=q2
				qqqq3=q3
				qqqq4=q4
				diff1=difff1
				diff2=difff2
				diff3=difff3
				diff4=difff4
			 }
		}
																		println(q2)
//		println("projection size = "+ (projectionSize/100).toString+" cm^2.")
//		println("123")
//		println(wall)
//		println(maxArea)
//		q1=homography.applyToPoint(wall(0))
//		q2=homography.applyToPoint(wall(1))
//		q3=homography.applyToPoint(wall(2))
//		q4=homography.applyToPoint(wall(3))
	}
	def killKeystone0() {
		val wall=Array[PVector](q1,q2,q3,q4)
		resetProjection2()
		var cam=Array[PVector](q1,q2,q3,q4)

		
		homography.computeHomography(wall,cam)
		
		q1=homography.applyToPoint(q1)
		q2=homography.applyToPoint(q2)
		q3=homography.applyToPoint(q3)
		q4=homography.applyToPoint(q4)
	}
	def killKeystone() {
		val wall=Array[PVector](q1,q2,q3,q4)
		resetProjection3()
		val cam=Array[PVector](q1,q2,q3,q4)
		homography.computeHomography(wall,cam)
		q1=homography.applyToPoint(q1)
		q2=homography.applyToPoint(q2)
		q3=homography.applyToPoint(q3)
		q4=homography.applyToPoint(q4)
		
		constrainProjection2()
//		constrainProjection()
	}
	def inRange(a:PVector):Boolean={
		if (a.x>=0 && a.x<=848 && a.y>=0 &&a.y<=480) {return true}else {return false}
	}
	def getDepths() {
//		val depthValues = kinect.depthMap();
		if (withinBoundry()) {
			q1.z=depthValues((q1.x+q1.y*640-1).toInt)
			q2.z=depthValues((q2.x+q2.y*640-1).toInt)
			q3.z=depthValues((q3.x+q3.y*640-1).toInt)
			q4.z=depthValues((q4.x+q4.y*640-1).toInt)
		}
	}
	def withinBoundry():Boolean= {
		if (q1.x>0 &&q2.x>0&&q3.x>0&&q4.x>0&&q1.x<848&&q2.x<848&&q3.x<848&&q4.x<848&&q1.y>0&&q2.y>0&&q3.y>0&&q4.y>0&&q1.y<480&&q2.y<480&q3.y<480&&q4.y<480) {return true}
		else {return false}
	}
	def predictKeystone() {
		println("predict keystone")
//		val depthValues = kinect.depthMap();
		resetProjection3()

		q1.z=depthValues((q1.x+q1.y*640-1).toInt)
		q2.z=depthValues((q2.x+q2.y*640-1).toInt)
		q3.z=depthValues((q3.x+q3.y*640-1).toInt)
		q4.z=depthValues((q4.x+q4.y*640-1).toInt)
		ctp.z=depthValues((ctp.x+ctp.y*640-1).toInt)
		
		if (q1.z > 0 && q2.z>0 && q3.z>0 && q4.z>0 && ctp.z>0) {
		
			q1=calibration.depthTo3D(q1)
			q2=calibration.depthTo3D(q2)
			q3=calibration.depthTo3D(q3)
			q4=calibration.depthTo3D(q4)
			ctp=calibration.depthTo3D(ctp)
		
//		pseudoTranslation()
//			var rotationMatrix:scalala.tensor.dense.DenseMatrix[Double]=null
		    val rotationMatrix=getRotation2()
			q1=calibration.applyMatrix(q1,rotationMatrix)
			q2=calibration.applyMatrix(q2,rotationMatrix)
			q3=calibration.applyMatrix(q3,rotationMatrix)
			q4=calibration.applyMatrix(q4,rotationMatrix)
			ctp=calibration.applyMatrix(ctp,rotationMatrix)
//		println(q1)
//		constrainProjection2(a1,a2,a3,a4)
//		constrainProjection()
//			val translationMatrix=getTranslation()
//			q1=calibration.applyMatrix(q1,translationMatrix)
//			q2=calibration.applyMatrix(q2,translationMatrix)
//			q3=calibration.applyMatrix(q3,translationMatrix)
//			q4=calibration.applyMatrix(q4,translationMatrix)
//			
//			println(q1)
//			println(q2)
//			println(q3)
//			println(q4)
			qqqqqq1=q1
			qqqqqq2=q2
			qqqqqq3=q3
			qqqqqq4=q4
			
		}else {
			println("zero depth!!!!!!!!!!!!!!!!")
			q1=qqqqqq1
			q2=qqqqqq2
			q3=qqqqqq3
			q4=qqqqqq4
		}

	}
	def applyToProjector() {
		getDepths()

		
		if (q1.z==0|q2.z==0|q3.z==0|q4.z==0) {
				q1=qqqqq1
				q2=qqqqq2
				q3=qqqqq3
				q4=qqqqq4
				
		}else {
			if (inRange(q1)&&inRange(q2)&&inRange(q3)&&inRange(q4)) {
				qqqqq1=q1
				qqqqq2=q2
				qqqqq3=q3
				qqqqq4=q4
			q1=calibration.depthToProjector(q1)
			q2=calibration.depthToProjector(q2)
			q3=calibration.depthToProjector(q3)
			q4=calibration.depthToProjector(q4)
			}else
				q1=qqqqq1
				q2=qqqqq2
				q3=qqqqq3
				q4=qqqqq4
		}
			
		
		
		
		
		

	}
	def getCenterPoint(a:PVector, b:PVector, c:PVector, d:PVector):PVector= {
		val m1=(q3.y-q1.y)/(q3.x-q1.x)//slope
		val m2=(q4.y-q2.y)/(q4.x-q2.x)//slope
		val xx=(m1*q1.x-m2*q2.x+q2.y-q1.y)/(m1-m2)
		val yy=m1*(xx-q1.x)+q1.y
		return new PVector(xx,yy,0)
	}
	def pseudoTranslation() {
		q1.z-=ctp.z
		q2.z-=ctp.z
	}
	def pseudoTranslation2(){
		var tx=min4(q1.x,q2.x,q3.x,q4.x)
		var ty=min4(q1.y,q2.y,q3.y,q4.y)
		if (tx>0) {tx=1}else {tx=(-1)*tx+1}		
		if (ty>0) {ty=1}else {ty=(-1)*ty+1}
		var translationMatrix=Matrix.eye[Double](4)
		translationMatrix(0,3)=tx
		translationMatrix(1,3)=ty
		q1=calibration.applyMatrix(q1,translationMatrix)
		q2=calibration.applyMatrix(q2,translationMatrix)
		q3=calibration.applyMatrix(q3,translationMatrix)
		q4=calibration.applyMatrix(q4,translationMatrix)
	}	
	def getTranslation():scalala.tensor.dense.DenseMatrix[Double]= {
		val tx=ctpp.x-ctp.x
		val ty=ctpp.y-ctp.y
		var translationMatrix=Matrix.eye[Double](4)
		translationMatrix(0,3)=tx
		translationMatrix(1,3)=ty
		return translationMatrix
	}
	def averageRotation():PVector= {
//		println(a1)
//		println(a2)
//		println(a3)
//		println(a4)
		var p1=new PVector(0,0,0)
		var p2=new PVector(0,0,0)
		var p3=new PVector(0,0,0)
		var p4=new PVector(0,0,0)
		p1.x=max2(a1.x,a4.x)
//		println("max"+max2(a1.x,a4.x))
//		
//		println("1"+p1)
		p4.x=p1.x
		p1.y=max2(a1.y,a2.y)
//		println("2"+p1)
		p2.y=p1.y
//		println("3"+p1)
		p2.x=min2(a2.x,a3.x)
//		println("4"+p1)
		p3.x=p2.x
//		println("5"+p1)
		p3.y=min2(a3.y,a4.y)
//		println("6"+p1)
		p4.y=p3.y
//		println("7"+p1)
//		println(p2)
//		println(p3)
//		println(p4)
		var searchs=16
		var stepX=(p2.x-p1.x)/searchs
		var stepY=(p4.y-p1.y)/searchs
		
		var x1=p4
		var x2=p3
		var y1=p4
		var y2=p1
		var thetaY=0.toFloat
		var thetaX=0.toFloat
		
		var xx1=0
		var xx2=0
		var xx3=0
		var total=0
		var valid=true
		for (i<- 0 to searchs-1) {
//			println("i"+i)
//			println("p"+p1+p2+p3+p4)
//			println(x1,x2,y1,y2)
			x1.x=p4.x+stepX*i
			x1.y=p4.y-stepY*i
			x2.x=p3.x
			x2.y=x1.y
			y1=x1
			y2.x=y1.x
			y2.y=p1.y
//			println(x1,x2,y1,y2)
			
			x1.z=depthValues((x1.x+x1.y*640-1).toInt)
			x2.z=depthValues((x2.x+x2.y*640-1).toInt)
			y1.z=depthValues((y1.x+y1.y*640-1).toInt)
//			println(i)
//			println(x1)
//			println(x2)
//			println(y2)
			y2.z=depthValues((y2.x+y2.y*640-1).toInt)

			if (y1.z==0) {valid=false}
			if (y2.z==0) {valid=false}
			if (x1.z==0) {valid=false}
			if (x2.z==0) {valid=false}
			
			
			x1=calibration.depthTo3D(x1)
			x2=calibration.depthTo3D(x2)
			y1=calibration.depthTo3D(y1)
			y2=calibration.depthTo3D(y2)
			if (valid) {
				thetaY+=(-atan((y2.z-y1.z)/(y2.y-y1.y))).toFloat
				thetaX+=(atan((cos(thetaY)*(x2.z-x1.z)/(x2.x-x1.x)))).toFloat
				total+=1
			}
		}
		thetaX=thetaX/total
		thetaY=thetaY/total
		return new PVector(thetaX,thetaY)
	}
	def getRotation2():scalala.tensor.dense.DenseMatrix[Double]= {
		val thetas=averageRotation()
		var thetaX=thetas.x
		var thetaY=thetas.y
		val thetaZ=0.toFloat
		
		val pi=3.1415926
		println("thetaX="+(thetaX/pi*180).toString)
		println("thetaY="+(thetaY/pi*180).toString)
		
		if (thetaY<2*pi && thetaY>(-2*pi)){}else {thetaY=0.toFloat}
		if (thetaX<2*pi && thetaX>(-2*pi)){}else {thetaX=0.toFloat}
		
		return calibration.getR(thetaY,thetaX,thetaZ)
	}
	def getRotation():scalala.tensor.dense.DenseMatrix[Double]= {
//		val depthValues = kinect.depthMap()
//		var x1=new PVector(640/2,480/2,0)
//		var x2=new PVector(640/2+20,480/2,0)
//		var y1=x1
//		var y2=new PVector(640/2,480/2+20,0)
		
		var x1=a4
		var x2=a3
		var y1=x1
		var y2=a1
		
		
		x1.z=depthValues((x1.x+x1.y*640-1).toInt)
		x2.z=depthValues((x2.x+x2.y*640-1).toInt)
		y1.z=depthValues((y1.x+y1.y*640-1).toInt)
		y2.z=depthValues((y2.x+y2.y*640-1).toInt)
		
		x1=calibration.depthTo3D(x1)
		x2=calibration.depthTo3D(x2)
		y1=calibration.depthTo3D(y1)
		y2=calibration.depthTo3D(y2)
		
		
		
		var thetaY=(-atan((y2.z-y1.z)/(y2.y-y1.y))).toFloat
		var thetaX=(atan((cos(thetaY)*(x2.z-x1.z)/(x2.x-x1.x)))).toFloat
		val thetaZ=0.toFloat
		val pi=3.1415926
		
		
//		println("thetaX="+(thetaX/pi*180).toString)
//		println("thetaY="+(thetaY/pi*180).toString)
		
		if (thetaY<2*pi && thetaY>(-2*pi)){}else {thetaY=0.toFloat}
		if (thetaX<2*pi && thetaX>(-2*pi)){}else {thetaX=0.toFloat}
		
		return calibration.getR(thetaY,thetaX,thetaZ)
	}
	def constrainProjection2() {
//		val upperWidth=a2.x-a1.x
//		val lowerWidth=a3.x-a4.x
//		val leftHeight=a4.y-a1.y
//		val rightHeight=a3.y-a2.y
//		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
//		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
//		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
//		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
//		val centerX=(rightX-leftX)/2+leftX
//		val centerY=(lowerY-upperY)/2+upperY
//		val centerX=ctp.x
//		val centerY=ctp.y
//		val ratioX=width/(rightX-leftX)
//		val ratioY=height/(lowerY-upperY)
//		val upperCtp=new PVector((a2.x-a1.x)/2+a1.x,(a2.y-a1.y)/2+a1.y)
//		val lowerCtp=new PVector((a3.x-a4.x)/2+a4.x,(a3.y-a4.y)/2+a4.y)
//		val leftCtp=new PVector((a4.x-a1.x)/2+a1.x,(a4.y-a1.y)/2+a1.y)
//		val rightCtp=new PVector((a3.x-a2.x)/2+a2.x,(a3.y-a2.y)/2+a2.y)
		
		val ratio1=abs((a1.y-ctpp.y)/(q1.y-ctp.y))
		val ratio2=abs((a1.x-ctpp.x)/(q1.x-ctp.x))
		val ratio3=abs((a2.y-ctpp.y)/(q2.y-ctp.y))
		val ratio4=abs((a2.x-ctpp.x)/(q2.x-ctp.x))
		val ratio5=abs((a3.y-ctpp.y)/(q3.y-ctp.y))
		val ratio6=abs((a3.x-ctpp.x)/(q3.x-ctp.x))
		val ratio7=abs((a4.y-ctpp.y)/(q4.y-ctp.y))
		val ratio8=abs((a4.x-ctpp.x)/(q4.x-ctp.x))
		val ratio=min8(ratio1,ratio2,ratio3,ratio4,ratio5,ratio6,ratio7,ratio8)
//		var shiftX=0.toFloat
//		var shiftY=0.toFloat
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-ctp.x)*ratio+ctpp.x
		q1.y=(q1.y-ctp.y)*ratio+ctpp.y
		q2.x=(q2.x-ctp.x)*ratio+ctpp.x
		q2.y=(q2.y-ctp.y)*ratio+ctpp.y
		q3.x=(q3.x-ctp.x)*ratio+ctpp.x
		q3.y=(q3.y-ctp.y)*ratio+ctpp.y
		q4.x=(q4.x-ctp.x)*ratio+ctpp.x
		q4.y=(q4.y-ctp.y)*ratio+ctpp.y
		println(ctp)
		println(ctpp)
		println("1")
	}
	def constrainProjection3() {
//		val upperWidth=a2.x-a1.x
//		val lowerWidth=a3.x-a4.x
//		val leftHeight=a4.y-a1.y
//		val rightHeight=a3.y-a2.y
//		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
//		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
//		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
//		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
//		val centerX=(rightX-leftX)/2+leftX
//		val centerY=(lowerY-upperY)/2+upperY
//		val centerX=ctp.x
//		val centerY=ctp.y
//		val ratioX=width/(rightX-leftX)
//		val ratioY=height/(lowerY-upperY)
//		val upperCtp=new PVector((a2.x-a1.x)/2+a1.x,(a2.y-a1.y)/2+a1.y)
//		val lowerCtp=new PVector((a3.x-a4.x)/2+a4.x,(a3.y-a4.y)/2+a4.y)
//		val leftCtp=new PVector((a4.x-a1.x)/2+a1.x,(a4.y-a1.y)/2+a1.y)
//		val rightCtp=new PVector((a3.x-a2.x)/2+a2.x,(a3.y-a2.y)/2+a2.y)
		
		val ratio1=((a1.y-ctpp.y)/(q1.y-ctp.y))
		val ratio2=((a1.x-ctpp.x)/(q1.x-ctp.x))
		val ratio3=((a2.y-ctpp.y)/(q2.y-ctp.y))
		val ratio4=((a2.x-ctpp.x)/(q2.x-ctp.x))
		val ratio5=((a3.y-ctpp.y)/(q3.y-ctp.y))
		val ratio6=((a3.x-ctpp.x)/(q3.x-ctp.x))
		val ratio7=((a4.y-ctpp.y)/(q4.y-ctp.y))
		val ratio8=((a4.x-ctpp.x)/(q4.x-ctp.x))
		val ratio=minAbs8(ratio1,ratio2,ratio3,ratio4,ratio5,ratio6,ratio7,ratio8)
//		var shiftX=0.toFloat
//		var shiftY=0.toFloat
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-ctp.x)*ratio+ctpp.x
		q1.y=(q1.y-ctp.y)*ratio+ctpp.y
		q2.x=(q2.x-ctp.x)*ratio+ctpp.x
		q2.y=(q2.y-ctp.y)*ratio+ctpp.y
		q3.x=(q3.x-ctp.x)*ratio+ctpp.x
		q3.y=(q3.y-ctp.y)*ratio+ctpp.y
		q4.x=(q4.x-ctp.x)*ratio+ctpp.x
		q4.y=(q4.y-ctp.y)*ratio+ctpp.y
	}
	def constrainProjection(){//old name: "biggestAreeProjection3"
		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
		val centerX=(rightX-leftX)/2+leftX
		val centerY=(lowerY-upperY)/2+upperY
		val ratioX=width/(rightX-leftX)
		val ratioY=height/(lowerY-upperY)
		val ratio=min2(ratioX,ratioY)
		var shiftX=0.toFloat
		var shiftY=0.toFloat
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-centerX)*ratio+width/2
		q1.y=(q1.y-centerY)*ratio+height/2
		q2.x=(q2.x-centerX)*ratio+width/2
		q2.y=(q2.y-centerY)*ratio+height/2
		q3.x=(q3.x-centerX)*ratio+width/2
		q3.y=(q3.y-centerY)*ratio+height/2
		q4.x=(q4.x-centerX)*ratio+width/2
		q4.y=(q4.y-centerY)*ratio+height/2
//		if (ratioX<ratioY){
//			if (dy>0){
//				shiftX=0
//				shiftY=(-1).toFloat*min2(q1.y,q2.y)
//			}else{
//				shiftX=0
//				shiftY=min2(height-q3.y,height-q4.y)
//			}
//		}else{
//			if (dx<0){
//				shiftY=0
//				shiftX=min2(width-q2.x,width-q3.x)
//			}else{
//				shiftY=0
//				shiftX=(-1).toFloat*min2(q1.x,q4.x)
//			}
//		}
//		q1.x+=shiftX
//		q2.x+=shiftX
//		q3.x+=shiftX
//		q4.x+=shiftX
//		q1.y+=shiftY
//		q2.y+=shiftY
//		q3.y+=shiftY
//		q4.y+=shiftY
	}
	
	
	
	
	
	
	def translateProjection():PVector={
		val pi=3.1415926
		val shiftAngle=atan(dy/dx)
		val dz=cos(shiftAngle)*dx+sin(shiftAngle)*dy
//		println("")
//		println(dz.toString+dx.toString+dy.toString)
		val alpha=atan(dz/(1*centerPoint.z*perPixelPerDistance))
//		println(alpha)
		val beta=pi/2-alpha
//		println(beta)
//		println(centerPoint.z)
		var shift=centerPoint.z*cos(beta)
//		println(shift)
//		println(centerPoint.z)
//		println(perPixelPerDistance)
		shift=shift/centerPoint.z/perPixelPerDistance
//		println(shift)
		
		val shiftX=(shift*cos(shiftAngle)*tuneShift).toFloat
		val shiftY=(shift*sin(shiftAngle)*tuneShift).toFloat
		return new PVector(shiftX,shiftY)
	}
	def math(a: PVector, b: PVector, c: PVector){
	
		/** Equations Operation
			(a.x-b.x)*dx+(a.y-b.y)*dy=(a.z-b.z)
			(a.x-c.x)*dx+(a.y-c.y)*dy=(a.z-c.z)
			((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x)=((a.z-c.z)-(a.y-c.y)*dy)/(a.x-c.x)
			(a.z-b.z)/(a.x-b.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.y-c.y)*dy/(a.x-c.x)
			(a.y-c.y)*dy/(a.x-c.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x)
		*/
	
		dy=((a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x))/((a.y-c.y)/(a.x-c.x)-(a.y-b.y)/(a.x-b.x));
		dx=((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x);
	
		q1.z=a.z-((a.x-q1.x)*dx+(a.y-q1.y)*dy);
		q2.z=a.z-((a.x-q2.x)*dx+(a.y-q2.y)*dy);
		q3.z=a.z-((a.x-q3.x)*dx+(a.y-q3.y)*dy);
		q4.z=a.z-((a.x-q4.x)*dx+(a.y-q4.y)*dy);
		centerPoint.z=a.z-((a.x-centerPoint.x)*dx+(a.y-centerPoint.y)*dy);
//		convertToActoalDistance()  // for later corner use
		subtractCornerDepths()
		//magnifyConorDepthTest()
	}
	def math3(a: PVector, b: PVector, c: PVector){
		resetProjection()
		/** Equations Operation
			(a.x-b.x)*dx+(a.y-b.y)*dy=(a.z-b.z)
			(a.x-c.x)*dx+(a.y-c.y)*dy=(a.z-c.z)
			((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x)=((a.z-c.z)-(a.y-c.y)*dy)/(a.x-c.x)
			(a.z-b.z)/(a.x-b.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.y-c.y)*dy/(a.x-c.x)
			(a.y-c.y)*dy/(a.x-c.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x)
		*/
	
		dy=((a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x))/((a.y-c.y)/(a.x-c.x)-(a.y-b.y)/(a.x-b.x));
		dx=((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x);
	
		q1.z=a.z-((a.x-q1.x)*dx+(a.y-q1.y)*dy);
		q2.z=a.z-((a.x-q2.x)*dx+(a.y-q2.y)*dy);
		q3.z=a.z-((a.x-q3.x)*dx+(a.y-q3.y)*dy);
		q4.z=a.z-((a.x-q4.x)*dx+(a.y-q4.y)*dy);
		centerPoint.z=a.z-((a.x-centerPoint.x)*dx+(a.y-centerPoint.y)*dy);
		
		println(centerPoint.z.toString+" ctp.z")
		println()
//		subtractCornerDepths()
		//magnifyConorDepthTest()
		
//		counterProjection()
//		translateProjection0()
		
//	    tuneDepth3()
	    coordinateProjection2()
	    tuneProjection3()
	 
	    biggestAreaProjection()
//	    println(q1.x)
//	    println(q3.x)
//	    flipProjection()
		
	}

	def tuneProjection3(){
//		val tuneCoordinateX2=tuneCoordinateX*abs(dy/0.5).toFloat*5
//		val tuneCoordinateY2=tuneCoordinateY*abs(dx/0.5).toFloat*5
		val tuneCoordinateX2=tuneCoordinateX
		val tuneCoordinateY2=tuneCoordinateY
		var avgX=0.toFloat
		var avgY=0.toFloat
		avgX=(q4.x-q1.x)/2*tuneCoordinateX2
//		println((q4.x-q1.x).toString+" "+tuneCoordinateX2)
//		println(q1.x.toString+" "+q4.x.toString+" avg: "+avgX)
		q1.x+=avgX
		q4.x-=avgX
//		println(q1.x.toString+" "+q4.x.toString)
		avgX=(q3.x-q2.x)/2*tuneCoordinateX2
		q2.x+=avgX
		q3.x-=avgX
		avgY=(q2.y-q1.y)/2*tuneCoordinateY2
		q1.y+=avgY
		q2.y-=avgY
		avgY=(q4.y-q3.y)/2*tuneCoordinateY2
		q3.y+=avgY
		q4.y-=avgY

	}
	def tuneProjection2(){

		val tuneCoordinateX2=tuneCoordinateX*abs(dy/10).toFloat*3000
		val tuneCoordinateY2=tuneCoordinateY*abs(dx/10).toFloat*5000
		q1.x-=tuneCoordinateX2
		q2.x+=tuneCoordinateX2
		q3.x-=tuneCoordinateX2
		q4.x+=tuneCoordinateX2
		q1.y-=tuneCoordinateY2
		q2.y-=tuneCoordinateY2
		q3.y+=tuneCoordinateY2
		q4.y+=tuneCoordinateY2

	}
	def tuneProjection(){
//	    tuneCoordinate*=cos(atan(dz)).toFloat
	    val tuneCoordinate2=tuneCoordinateX*abs(dz/10).toFloat
//	    println(q1.x)
//	    println(tuneCoordinate)
	    q1.x-=tuneCoordinate2
	    q2.x+=tuneCoordinate2
	    q3.x-=tuneCoordinate2
	    q4.x+=tuneCoordinate2
	    q1.y-=tuneCoordinate2
	    q2.y-=tuneCoordinate2
	    q3.y+=tuneCoordinate2
	    q4.y+=tuneCoordinate2
	    
//	    println(q1.x)
	}
	/* it is a wrong way to do, as it is the same with tuningDepth
	def magnifyConorDepthTest(){
		q1.z*=depthMagnification
		q2.z*=depthMagnification
		q3.z*=depthMagnification
		q4.z*=depthMagnification
	}*/
	def counterProjection(){
		q1.z*=(-1).toFloat
		q2.z*=(-1).toFloat
		q3.z*=(-1).toFloat
		q4.z*=(-1).toFloat
		centerPoint.z*=(-1).toFloat
		
	}
	def flipProjection(){
		val q1x=q1.x
		val q2x=q2.x
		val q1y=q1.y
		val q2y=q2.y
		q1.x=width/2-(q3.x-width/2)
		q2.x=width/2-(q4.x-width/2)
		q3.x=width/2-(q1x-width/2)
		q4.x=width/2-(q2x-width/2)
		q1.y=height/2-(q3.y-height/2)
		q2.y=height/2-(q4.y-height/2)
		q3.y=height/2-(q1y-height/2)
		q4.y=height/2-(q2y-height/2)
	}

	def translateProjection0(){
		val pi=3.1415926
		val shiftAngle=atan(dy/dx)
		dz=cos(shiftAngle)*dx+sin(shiftAngle)*dy
//		println("")
//		println(dz.toString+dx.toString+dy.toString)
		val alpha=atan(dz/(1*centerPoint.z*perPixelPerDistance))
//		println(alpha)
		val beta=pi/2-alpha
//		println(beta)
//		println(centerPoint.z)
		var shift=centerPoint.z*cos(beta)
//		println(shift)
//		println(centerPoint.z)
//		println(perPixelPerDistance)
		shift=shift/centerPoint.z/perPixelPerDistance
//		println(shift)
		
		val shiftX=(shift*cos(shiftAngle)).toFloat*tuneTranslateX
		val shiftY=(shift*sin(shiftAngle)).toFloat*tuneTranslateY
		q1.x+=shiftX
		q1.y+=shiftY
		q2.x+=shiftX
		q2.y+=shiftY
		q3.x+=shiftX
		q3.y+=shiftY
		q4.x+=shiftX
		q4.y+=shiftY
	}
	def biggestAreaProjection(){
		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
		val centerX=(rightX-leftX)/2+leftX
		val centerY=(lowerY-upperY)/2+upperY
		val ratioX=width/(rightX-leftX)
		val ratioY=height/(lowerY-upperY)
		val ratio=min2(ratioX,ratioY)
		var shiftX=0.toFloat
		var shiftY=0.toFloat
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-centerX)*ratio+width/2
		q1.y=(q1.y-centerY)*ratio+height/2
		q2.x=(q2.x-centerX)*ratio+width/2
		q2.y=(q2.y-centerY)*ratio+height/2
		q3.x=(q3.x-centerX)*ratio+width/2
		q3.y=(q3.y-centerY)*ratio+height/2
		q4.x=(q4.x-centerX)*ratio+width/2
		q4.y=(q4.y-centerY)*ratio+height/2
		if (ratioX<ratioY){
			if (dy>0){
				shiftX=0
				shiftY=(-1).toFloat*min2(q1.y,q2.y)
			}else{
				shiftX=0
				shiftY=min2(height-q3.y,height-q4.y)
			}
		}else{
			if (dx<0){
				shiftY=0
				shiftX=min2(width-q2.x,width-q3.x)
			}else{
				shiftY=0
				shiftX=(-1).toFloat*min2(q1.x,q4.x)
			}
		}
		q1.x+=shiftX
		q2.x+=shiftX
		q3.x+=shiftX
		q4.x+=shiftX
		q1.y+=shiftY
		q2.y+=shiftY
		q3.y+=shiftY
		q4.y+=shiftY
	}
	
	def resetProjection(){
		q1 = new PVector(0,0,0);
		q2 = new PVector(600,0,0);
		q3 = new PVector(600,450,0);
		q4 = new PVector(0,450,0);
	}
	def resetProjection2() {
		q1 = new PVector(0+qqx,0+qqy,0);
		q2 = new PVector(640-qqx,0+qqy,0);
		q3 = new PVector(640-qqx,480-qqy,0);
		q4 = new PVector(0+qqx,480-qqy,0);
		ctp = new PVector(640/2,480/2,0)
	}	
	def resetProjection3() {
		q1 = a1
		q2 = a2
		q3 = a3
		q4 = a4
		ctp = ctpp
	}
	def coordinateProjection1(){
		q1.x=1/(centerPoint.z/q1.z)*(q1.x-width/2)+width/2;
		q2.x=1/(centerPoint.z/q2.z)*(q2.x-width/2)+width/2;
		q3.x=1/(centerPoint.z/q3.z)*(q3.x-width/2)+width/2;
		q4.x=1/(centerPoint.z/q4.z)*(q4.x-width/2)+width/2;
		q1.y=1/(centerPoint.z/q1.z)*(q1.y-height/2)+height/2;
		q2.y=1/(centerPoint.z/q2.z)*(q2.y-height/2)+height/2;
		q3.y=1/(centerPoint.z/q3.z)*(q3.y-height/2)+height/2;
		q4.y=1/(centerPoint.z/q4.z)*(q4.y-height/2)+height/2;
		q1.z=0;
		q2.z=0;
		q3.z=0;
		q4.z=0;
	}
	def tuneDepth2(){
		q1.z+=tuneDepth*300
		q2.z+=tuneDepth*300
		q3.z+=tuneDepth*300
		q4.z+=tuneDepth*300
	}	
	def tuneDepth3(){
		dx+=(0.5-tuneDepth).toFloat
		dy+=(0.5-tuneDepth).toFloat
	}
	def coordinateProjection2(){
//		tuneDepth2()
		q1.x=(centerPoint.z/q1.z)*(q1.x-width/2)+width/2;
		q2.x=(centerPoint.z/q2.z)*(q2.x-width/2)+width/2;
		q3.x=(centerPoint.z/q3.z)*(q3.x-width/2)+width/2;
		q4.x=(centerPoint.z/q4.z)*(q4.x-width/2)+width/2;
		q1.y=(centerPoint.z/q1.z)*(q1.y-height/2)+height/2;
		q2.y=(centerPoint.z/q2.z)*(q2.y-height/2)+height/2;
		q3.y=(centerPoint.z/q3.z)*(q3.y-height/2)+height/2;
		q4.y=(centerPoint.z/q4.z)*(q4.y-height/2)+height/2;
		q1.z=0;
		q2.z=0;
		q3.z=0;
		q4.z=0;
	}

	
	
	def coordinateProjection0(){
		convertToActoalDistance()
//		val cornerToCenterDistance=sqrt((600/2)*600/2+(450/2)*450/2)
//		
//		
//		
//		
//		
//		val q1r=((q1.z*q1.z+centerPoint.z*centerPoint.z-2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))/(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)).toFloat
//		val q1r=(sqrt(q1.z*q1.z+centerPoint.z*centerPoint.z-2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))/(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)).toFloat
//		println(q1.z)
//		println(q1.z*q1.z)
//		println(centerPoint.z)
//		println(centerPoint.z*centerPoint.z)
//		println(cos(atan(cornerToCenterDistance*perPixelPerDistance)))
//		println(2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))
//		println(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)
//		println(q1r)
//		println("")
		
		var ratio=0.toFloat
		var distanceToCenter=0.toFloat
		
		distanceToCenter=sqrt((q1.x-width/2)*(q1.x-width/2)+(q1.y-height/2)*(q1.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q1.z*q1.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q1.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		
		q1.x=(q1.x-width/2)*ratio+width/2
		q1.y=(q1.y-height/2)*ratio+height/2
		q1.z=0
				
		distanceToCenter=sqrt((q2.x-width/2)*(q2.x-width/2)+(q2.y-height/2)*(q2.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q2.z*q2.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q2.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q2.x=(q2.x-width/2)*ratio+width/2
		q2.y=(q2.y-height/2)*ratio+height/2
		q2.z=0
		
		distanceToCenter=sqrt((q3.x-width/2)*(q3.x-width/2)+(q3.y-height/2)*(q3.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q3.z*q3.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q3.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q3.x=(q3.x-width/2)*ratio+width/2
		q3.y=(q3.y-height/2)*ratio+height/2
		q3.z=0
				
		distanceToCenter=sqrt((q4.x-width/2)*(q4.x-width/2)+(q4.y-height/2)*(q4.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q4.z*q4.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q4.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q4.x=(q4.x-width/2)*ratio+width/2
		q4.y=(q4.y-height/2)*ratio+height/2
		q4.z=0
	}
	
	def convertToActoalDistance(){
		q1.z=q1.z/cos(atan(sqrt(q1.x*q1.x+q1.y*q1.y)*perPixelPerDistance)).toFloat
		q2.z=q2.z/cos(atan(sqrt(q2.x*q2.x+q2.y*q2.y)*perPixelPerDistance)).toFloat
		q3.z=q3.z/cos(atan(sqrt(q3.x*q3.x+q3.y*q3.y)*perPixelPerDistance)).toFloat
		q4.z=q4.z/cos(atan(sqrt(q4.x*q4.x+q4.y*q4.y)*perPixelPerDistance)).toFloat
	}
	def subtractCornerDepths(){
		val maxx=max4(q1.z,q2.z,q3.z,q4.z)
				q1.z-=maxx;
		q2.z-=maxx;
		q3.z-=maxx;
		q4.z-=maxx;
	}
	def min2(a: Float, b: Float):Float={
		return minArray(Array(a,b))
	}
	def min3(a: Float, b: Float, c: Float):Float={
		return minArray(Array(a,b,c))
	}
	def min4(a: Float, b:Float, c:Float, d:Float):Float={
		return minArray(Array(a,b,c,d))
	}
	def min8(a: Float, b:Float, c:Float, d:Float, e:Float, f:Float, g:Float, h:Float):Float={
		return minArray(Array(a,b,c,d,e,f,g,h))
	}
	def minAbs8(a: Float, b:Float, c:Float, d:Float, e:Float, f:Float, g:Float, h:Float):Float={
		return minAbsArray(Array(a,b,c,d,e,f,g,h))
	}
	def minArray(input: Array[Float]):Float={
		var minn = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)<minn){minn=input(i)}
		}
		return minn
	}	
	def minAbsArray(input: Array[Float]):Float={
		var position = 0
		var minn = abs(input(0))
		for (i <- 1 to (input.length-1)){
			if (abs(input(i))<minn){
				minn=abs(input(i))
				position = i
			}
		}
		return input(position)
	}
	def max4(a: Float, b:Float, c:Float, d:Float):Float={
		return maxArray(Array(a,b,c,d))
	}
	def max2(a: Float, b:Float):Float={
		return maxArray(Array(a,b))
	}
	def maxArray(input: Array[Float]):Float={
		var maxx = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)>maxx){maxx=input(i)}
		}
		return maxx
	}	
}