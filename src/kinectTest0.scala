import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _,  _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
/* For OpenGl
 * 1. put jogl.jar and gluegen-rt.jar from processing libiray: H:\Program Files\processing-1.5.1\modes\java\libraries\opengl\library\windows32
 * 2. set jogl.jar's native library to where it comes from.
 * 3. OR, find jogl.dll and jogl_awt.dll and put them in the lib and set native library to it.
 */

import processing.opengl._;
object kinectTest0 extends PApplet{
	val tuningDepth=false
	val testingDepthMagnification=true
//    var context: SimpleOpenNI  = null;
	var kinect: SimpleOpenNI  = null;
	var tuneDepth = -1.toFloat//(0.6375).toFloat;
	var depthMagnification = 1.toFloat
	
	var q1:PVector = new PVector(0,0,0);
	var q2:PVector = new PVector(640,0,0);
	var q3:PVector = new PVector(640,480,0);
	var q4:PVector = new PVector(0,480,0);
	var centerPoint = new PVector(640/2, 480/2, 0)
	var pointt = 0;
	var lasttime=second()+minute()*60;
	var lasttime2=second()+minute()*60;
  
	var  dx:Float = 0.toFloat;
	var  dy:Float = 0.toFloat;
	
	val perPixelPerDistance: Float=1045.toFloat/966/640
	
	def main(args: Array[String]) {

		val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(kinectTest0)
		kinectTest0.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
	}
	override def setup() {
		/*
//		context = new SimpleOpenNI(this);
//
//		// mirror is by default enabled
//		context.setMirror(true);
//		// enable depthMap generation 
//		if(context.enableDepth() == false)
//		{
//			println("Can't open the depthMap, maybe the camera is not connected!"); 
//			exit();
//			return;
//		}
//		if(context.enableRGB() == false)
//		{
//			println("Can't open the rgbMap, maybe the camera is not connected or there is no rgbSensor!"); 
//			exit();
//			return;
//		}
//
//		size(context.depthWidth() + context.rgbWidth() + 10, context.rgbHeight());
		*/
	  
		size(640, 480,OPENGL);
		kinect = new SimpleOpenNI(this);
		kinect.enableRGB();
		kinect.enableDepth();
	}

	override def draw() {
		/* initial test
//		// update the cam
//		context.update();
//
//		background(255,255,255);
//
//		// draw depthImageMap
//		image(context.depthImage(),0,0);
//
//		// draw rgbImageMap
//		image(context.rgbImage(),context.depthWidth() + 10,0);
		*/
		
		kinect.update();
		background(255);
		val imagee = kinect.rgbImage();
		
		//draw axis
		imagee.loadPixels()
		for (i <- 0 to width-1){imagee.pixels(i+imagee.height/2*imagee.width)=color(0,255,0)}
		for (i <- 0 to height-1){imagee.pixels(imagee.width/2+i*imagee.width)=color(0,255,0)}
		imagee.updatePixels()
		//
		
//		image(imagee, 0, 0);
		
		//calculate corner depth
		if (second()+minute()*60-lasttime>0.5){
			val depthValues = kinect.depthMap();
			var a = new PVector(200,250,0);
			var b = new PVector(300,260,0);
			var c= new PVector(250, 300,0);
			a.z=(-1)*depthValues((a.x+a.y*640).toInt)*tuneDepth;
			b.z=(-1)*depthValues((b.x+b.y*640).toInt)*tuneDepth;
			c.z=(-1)*depthValues((c.x+c.y*640).toInt)*tuneDepth;
			

//			q1.z=(-1)*depthValues((q1.x+100+(q1.y+100)*640).toInt)*tuneDepth;
//			q2.z=(-1)*depthValues((q2.x-100+(q2.y+100)*640).toInt)*tuneDepth;
//			q3.z=(-1)*depthValues((q3.x-100+(q3.y-100)*640).toInt)*tuneDepth;
//			q4.z=(-1)*depthValues((q4.x+100+(q4.y-100)*640).toInt)*tuneDepth;
//			subtractCornerDepths()
					
			/* [replaced by subtractConorDepth().]
				val adjust=min3(a.z,b.z,c.z)+150;
				a.z=a.z-adjust;
				b.z=b.z-adjust;
				c.z=c.z-adjust;
			*/
			math(a,b,c);
			lasttime=second()+minute()*60;
		}
		
		//drawing
		stroke(0,255,0);
		background(200);
		//    fill(0,255,0);
		//    rect(0+100,100,width-100,height-100);
//		translate(-tuneDepth*centerPoint.z*dx,-tuneDepth*centerPoint.z*dy)
//		translate(centerPoint.z*dx,centerPoint.z*dy)
		
		beginShape();
		textureMode(NORMALIZED);
		texture(imagee);
		vertex(q1.x, q1.y, q1.z, 0, 0);
		vertex(q2.x, q2.y, q2.z, 1, 0);
		vertex(q3.x, q3.y, q3.z, 1, 1);
		vertex(q4.x, q4.y, q4.z, 0, 1);
		
//		vertex(q1.x, q1.y, 0, 0);
//		vertex(q2.x, q2.y, 1, 0);
//		vertex(q3.x, q3.y, 1, 1);
//		vertex(q4.x, q4.y, 0, 1);
		endShape();
		
		//draw axis
		stroke(0,0,255)
		line(0,height/2,width,height/2)
		line(width/2,0,width/2,height)
		
//		if (second()+minute()*60-lasttime2>2){
//			println("");
//			println(q1.z);
//			println(q2.z);
//			println(q3.z);
//			println(q4.z);
//			lasttime2=second()+minute()*60;
//		}

	}
	override def mousePressed(){
		/* initial test
//		val depthValues = kinect.depthMap();
//		val clickPosition = mouseX + (mouseY * 640);
//		val millimeters = depthValues(clickPosition);
//		val inches = millimeters / 25.4;
//		println("mm: " + millimeters + " in: " + inches);
        */
		if (tuningDepth){
		    //for tuneDepth. we use this now because the homography between homography and camera are not set yet.
			tuneDepth = mouseX.toFloat/640
			println(mouseX)
			println("set tuneDepth valut to " + tuneDepth.toString)
		}
		if (testingDepthMagnification){
			depthMagnification = mouseX.toFloat/640
			println("set depthMagnification valut to " + depthMagnification.toString)
		}
	}
	def math(a: PVector, b: PVector, c: PVector){
	
		/** Equations Operation
			(a.x-b.x)*dx+(a.y-b.y)*dy=(a.z-b.z)
			(a.x-c.x)*dx+(a.y-c.y)*dy=(a.z-c.z)
			((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x)=((a.z-c.z)-(a.y-c.y)*dy)/(a.x-c.x)
			(a.z-b.z)/(a.x-b.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.y-c.y)*dy/(a.x-c.x)
			(a.y-c.y)*dy/(a.x-c.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x)
		*/
	
		dy=((a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x))/((a.y-c.y)/(a.x-c.x)-(a.y-b.y)/(a.x-b.x));
		dx=((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x);
	
		q1.z=a.z-((a.x-q1.x)*dx+(a.y-q1.y)*dy);
		q2.z=a.z-((a.x-q2.x)*dx+(a.y-q2.y)*dy);
		q3.z=a.z-((a.x-q3.x)*dx+(a.y-q3.y)*dy);
		q4.z=a.z-((a.x-q4.x)*dx+(a.y-q4.y)*dy);
		centerPoint.z=a.z-((a.x-centerPoint.x)*dx+(a.y-centerPoint.y)*dy);
//		convertToActoalDistance()  // for later corner use
		subtractCornerDepths()
		//magnifyConorDepthTest()
	}
	def math2(a: PVector, b: PVector, c: PVector){
		resetProjection()
		/** Equations Operation
			(a.x-b.x)*dx+(a.y-b.y)*dy=(a.z-b.z)
			(a.x-c.x)*dx+(a.y-c.y)*dy=(a.z-c.z)
			((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x)=((a.z-c.z)-(a.y-c.y)*dy)/(a.x-c.x)
			(a.z-b.z)/(a.x-b.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.y-c.y)*dy/(a.x-c.x)
			(a.y-c.y)*dy/(a.x-c.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x)
		*/
	
		dy=((a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x))/((a.y-c.y)/(a.x-c.x)-(a.y-b.y)/(a.x-b.x));
		dx=((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x);
	
		q1.z=a.z-((a.x-q1.x)*dx+(a.y-q1.y)*dy);
		q2.z=a.z-((a.x-q2.x)*dx+(a.y-q2.y)*dy);
		q3.z=a.z-((a.x-q3.x)*dx+(a.y-q3.y)*dy);
		q4.z=a.z-((a.x-q4.x)*dx+(a.y-q4.y)*dy);
		centerPoint.z=a.z-((a.x-centerPoint.x)*dx+(a.y-centerPoint.y)*dy);
//		subtractCornerDepths()
		//magnifyConorDepthTest()
		
		
		//translateProjection()
	    coordinateProjection()
	    biggestAreaProjection()
	    println(q1.x)
	    println(q3.x)
	    flipProjection()
		
	}
	/* it is a wrong way to do, as it is the same with tuningDepth
	def magnifyConorDepthTest(){
		q1.z*=depthMagnification
		q2.z*=depthMagnification
		q3.z*=depthMagnification
		q4.z*=depthMagnification
	}*/
	def flipProjection(){
		q1.x=width/2-(q3.x-width/2)
		q2.x=width/2-(q4.x-width/2)
		q3.x=width/2-(q1.x-width/2)
		q4.x=width/2-(q2.x-width/2)
		q1.y=height/2-(q3.y-height/2)
		q2.y=height/2-(q4.y-height/2)
		q3.y=height/2-(q1.y-height/2)
		q4.y=height/2-(q2.y-height/2)
	}
	def translateProjection(){
		val pi=3.1415926
		val shiftAngle=atan(dy/dx)
		val dz=cos(shiftAngle)*dx+sin(shiftAngle)*dy
//		println("")
//		println(dz.toString+dx.toString+dy.toString)
		val alpha=atan(dz/(1*centerPoint.z*perPixelPerDistance))
//		println(alpha)
		val beta=pi/2-alpha
//		println(beta)
//		println(centerPoint.z)
		var shift=centerPoint.z*cos(beta)
//		println(shift)
//		println(centerPoint.z)
//		println(perPixelPerDistance)
		shift=shift/centerPoint.z/perPixelPerDistance
//		println(shift)
		
		val shiftX=(shift*cos(shiftAngle)).toFloat
		val shiftY=(shift*sin(shiftAngle)).toFloat
		q1.x+=shiftX
		q1.y+=shiftY
		q2.x+=shiftX
		q2.y+=shiftY
		q3.x+=shiftX
		q3.y+=shiftY
		q4.x+=shiftX
		q4.y+=shiftY
	}
	def biggestAreaProjection(){
		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
		val centerX=(rightX-leftX)/2+leftX
		val centerY=(lowerY-upperY)/2+upperY
		val ratioX=width/(rightX-leftX)
		val ratioY=height/(lowerY-upperY)
		val ratio=min3(ratioX,ratioY,1)
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-centerX)*ratio+width/2
		q1.y=(q1.y-centerY)*ratio+height/2
		q2.x=(q2.x-centerX)*ratio+width/2
		q2.y=(q2.y-centerY)*ratio+height/2
		q3.x=(q3.x-centerX)*ratio+width/2
		q3.y=(q3.y-centerY)*ratio+height/2
		q4.x=(q4.x-centerX)*ratio+width/2
		q4.y=(q4.y-centerY)*ratio+height/2
		
	}
	def resetProjection(){
		q1 = new PVector(0,0,0);
		q2 = new PVector(640,0,0);
		q3 = new PVector(640,480,0);
		q4 = new PVector(0,480,0);
	}
	def coordinateProjection(){
		convertToActoalDistance()
//		val cornerToCenterDistance=sqrt((640/2)*640/2+(480/2)*480/2)
//		
//		
//		
//		
//		
//		val q1r=((q1.z*q1.z+centerPoint.z*centerPoint.z-2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))/(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)).toFloat
//		val q1r=(sqrt(q1.z*q1.z+centerPoint.z*centerPoint.z-2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))/(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)).toFloat
//		println(q1.z)
//		println(q1.z*q1.z)
//		println(centerPoint.z)
//		println(centerPoint.z*centerPoint.z)
//		println(cos(atan(cornerToCenterDistance*perPixelPerDistance)))
//		println(2*q1.z*centerPoint.z*cos(atan(cornerToCenterDistance*perPixelPerDistance)))
//		println(cornerToCenterDistance*perPixelPerDistance*centerPoint.z)
//		println(q1r)
//		println("")
		
		var ratio=0.toFloat
		var distanceToCenter=0.toFloat
		
		distanceToCenter=sqrt((q1.x-width/2)*(q1.x-width/2)+(q1.y-height/2)*(q1.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q1.z*q1.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q1.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		
		q1.x=(q1.x-width/2)*ratio+width/2
		q1.y=(q1.y-height/2)*ratio+height/2
		q1.z=0
				
		distanceToCenter=sqrt((q2.x-width/2)*(q2.x-width/2)+(q2.y-height/2)*(q2.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q2.z*q2.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q2.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q2.x=(q2.x-width/2)*ratio+width/2
		q2.y=(q2.y-height/2)*ratio+height/2
		q2.z=0
		
		distanceToCenter=sqrt((q3.x-width/2)*(q3.x-width/2)+(q3.y-height/2)*(q3.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q3.z*q3.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q3.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q3.x=(q3.x-width/2)*ratio+width/2
		q3.y=(q3.y-height/2)*ratio+height/2
		q3.z=0
				
		distanceToCenter=sqrt((q4.x-width/2)*(q4.x-width/2)+(q4.y-height/2)*(q4.y-height/2)).toFloat
		ratio=(//ratio for distance toward center
			  sqrt(//line BC
			      q4.z*q4.z+//line AB
			      centerPoint.z*centerPoint.z-//line AC
			      2*q4.z*centerPoint.z*cos(//line 2*AB*AC*cos(A)
			      						  atan(distanceToCenter*perPixelPerDistance)//angle A
			    		  				  )
				  )/(distanceToCenter*perPixelPerDistance*centerPoint.z)//projector expected distance
			  ).toFloat
		q4.x=(q4.x-width/2)*ratio+width/2
		q4.y=(q4.y-height/2)*ratio+height/2
		q4.z=0

		

		
		
		
		
		
	}
	
	def convertToActoalDistance(){
		q1.z=q1.z/cos(atan(sqrt(q1.x*q1.x+q1.y*q1.y)*perPixelPerDistance)).toFloat
		q2.z=q2.z/cos(atan(sqrt(q2.x*q2.x+q2.y*q2.y)*perPixelPerDistance)).toFloat
		q3.z=q3.z/cos(atan(sqrt(q3.x*q3.x+q3.y*q3.y)*perPixelPerDistance)).toFloat
		q4.z=q4.z/cos(atan(sqrt(q4.x*q4.x+q4.y*q4.y)*perPixelPerDistance)).toFloat
	}
	def subtractCornerDepths(){
		val maxx=max4(q1.z,q2.z,q3.z,q4.z)
				q1.z-=maxx;
		q2.z-=maxx;
		q3.z-=maxx;
		q4.z-=maxx;
	}
	def min3(a: Float, b: Float, c: Float):Float={
		return minArray(Array(a,b,c))
	}
	def min4(a: Float, b:Float, c:Float, d:Float):Float={
		return minArray(Array(a,b,c,d))
	}
	def minArray(input: Array[Float]):Float={
		var minn = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)<minn){minn=input(i)}
		}
		return minn
	}
	def max4(a: Float, b:Float, c:Float, d:Float):Float={
		return maxArray(Array(a,b,c,d))
	}
	def maxArray(input: Array[Float]):Float={
		var maxx = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)>maxx){maxx=input(i)}
		}
		return maxx
	}	
}