import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,tan=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _, abs=>_, _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
/* For OpenGl
 * 1. put jogl.jar and gluegen-rt.jar from processing libiray: H:\Program Files\processing-1.5.1\modes\java\libraries\opengl\library\windows32
 * 2. set jogl.jar's native library to where it comes from.
 * 3. OR, find jogl.dll and jogl_awt.dll and put them in the lib and set native library to it.
 */
import processing.opengl._;

object keystoneTest8 extends PApplet{

	var kinect: SimpleOpenNI  = null;
	var calibration=new cameraTransformationClass3()
	var homographyFromWallToDepth=new homographyClass3()
	var homographyFromDepthToWall=new homographyClass3()
	var rotationMatrix=Matrix.zeros[Double](4,4)
	
	def main(args: Array[String]) {

		val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(keystoneTest8)
		keystoneTest8.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
	}
	override def setup() {
		size(640, 480,OPENGL);
		kinect = new SimpleOpenNI(this);
		kinect.enableDepth();
	}

	override def draw() {
		kinect.update();
		background(0);
		val imagee = kinect.depthImage();
		
		//draw axis
		imagee.loadPixels()
		for (i <- 0 to 640-1){imagee.pixels(i+imagee.height/2*imagee.width)=color(0,255,0)}
		for (i <- 0 to 480-1){imagee.pixels(imagee.width/2+i*imagee.width)=color(0,255,0)}
		imagee.updatePixels()

		image(imagee,0,0)
		
		antiKeystone()
	}
	
	def antiKeystone() {
		val q1=new PVector(100,100,0)
		val q2=new PVector(540,100,0)
		val q3=new PVector(540,380,0)
		val q4=new PVector(100,380,0)
		
		val depthValues = kinect.depthMap()
		q1.z=depthValues((q1.x+q1.y*640).toInt)
		q2.z=depthValues((q2.x+q2.y*640).toInt)
		q3.z=depthValues((q3.x+q3.y*640).toInt)
		q4.z=depthValues((q4.x+q4.y*640).toInt)
		
		rotationMatrix=getRotation()
		
		val wall1=calibration.depthToWall(q1,rotationMatrix)
		val wall2=calibration.depthToWall(q2,rotationMatrix)
		val wall3=calibration.depthToWall(q3,rotationMatrix)
		val wall4=calibration.depthToWall(q4,rotationMatrix)
		
		val wall=Array[PVector](wall1,wall2,wall3,wall4)
		val cam=Array[PVector](q1,q2,q3,q4)
		homographyFromWallToDepth.computeHomography(wall,cam)
		homographyFromDepthToWall.computeHomography(cam,wall)
		
		
		val matrix=inv(homographyFromDepthToWall.homographyMatrix)
		
		if (matrix==homographyFromWallToDepth.homographyMatrix) {println("same!!!!")}
		println(calibration.applyMatrix(q1,matrix))
		println(calibration.applyMatrix(q2,matrix))
		println(calibration.applyMatrix(q3,matrix))
		println(calibration.applyMatrix(q4,matrix))
		println("done")
	}
	
	def getRotation():scalala.tensor.dense.DenseMatrix[Double]= {
		val depthValues = kinect.depthMap()
		val x1=new PVector(640/2,480/2,0)
		val x2=new PVector(640/2+10,480/2,0)
		val y1=x1
		val y2=new PVector(640/2,480/2+10,0)
		x1.z=depthValues((x1.x+x1.y*640).toInt)
		x2.z=depthValues((x2.x+x2.y*640).toInt)
		y1.z=depthValues((y1.x+y1.y*640).toInt)
		y2.z=depthValues((y2.x+y2.y*640).toInt)
		
		val thetaY=(-atan((y2.z-y1.z)/(y2.y-y1.y))).toFloat
		val thetaX=(-atan((cos(thetaY)*(x2.z-x1.z)/(x2.x-x1.x)))).toFloat
		val thetaZ=0.toFloat
		val pi=3.1415926
//		println("thetaX="+(thetaX/pi*180).toString)
//		println("thetaY="+(thetaY/pi*180).toString)
		
		return calibration.getR(thetaX,thetaY,thetaZ)
	}
	
}