import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,tan=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _, abs=>_, _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;

object demo11 extends PApplet{
	var calibratingDepthWithProj=false
	var calibratingRGBwithDepthAndProj=true
	var testingDepthWithProj=false
	var testingRGBWithDepth=false
	var count=0
	val imagee = loadImage("123.png")


	
		var proj1=new PVector(0,0)
		var proj2=new PVector(640,0)
		var proj3=new PVector(640,480)
		var proj4=new PVector(0,480)
		var cam1=new PVector(98,102)
		var cam2=new PVector(255,96)
		var cam3=new PVector(255,204)
		var cam4=new PVector(98,198)
		val proj=Array[PVector](proj1,proj2,proj3,proj4)
		val cam=Array[PVector](cam1,cam2,cam3,cam4)
	
		var p=new PVector(0,0)
	
	
	
	
	
	
	var homographyMatrix=loadHomography3(("depth-projector homography.txt"))
	
	var q1:PVector = new PVector(0,0,0);
	var q2:PVector = new PVector(640,0,0);
	var q3:PVector = new PVector(640,480,0);
	var q4:PVector = new PVector(0,480,0);
	
	var offscreen=createGraphics(848,480,OPENGL)
//	val imagee=loadImage("logo.jpg")
	
	
	
	
	
	
	def main(args: Array[String]) {

		val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(demo11)
		demo11.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
	}


	override def setup() {
		size(848, 480,P3D)
		run(new PVector(0,0), new PVector(500,500))
	}

	override def draw() {

		image(offscreen,0,0)
	}
	
		def run(a:PVector,b:PVector):PGraphics={
	    q1=applyToPoint(a)
	    q2=applyToPoint(new PVector(b.x,a.y))
	    q3=applyToPoint(new PVector(a.x,b.y))
	    q4=applyToPoint(b)
        logo()
        return (offscreen)
	}
	
    def logo(){
        offscreen.background(0)
        
        offscreen.beginShape();
		offscreen.textureMode(NORMALIZED);

		offscreen.texture(imagee)

		offscreen.vertex(q1.x, q1.y, 0, 0);
		offscreen.vertex(q2.x, q2.y, 1, 0);
		offscreen.vertex(q3.x, q3.y, 1, 1);
		offscreen.vertex(q4.x, q4.y, 0, 1);
		offscreen.endShape();
    }
    def applyToPoint(inputPoint: PVector):PVector={
	  var X=inputPoint.x
	  var Y=inputPoint.y
	  var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
	  
	  val x:Float=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val y:Float=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val outputPoint: PVector=new PVector(x,y)
	  return outputPoint
    }
    def loadHomography3(fileName: String): scalala.tensor.dense.DenseMatrix[Double] = {
		var matrix = Matrix.zeros[Double](3,3)
		var item:String=""
		var column=0
		var row=0
		val file = Source.fromFile(fileName)
		file.getLines.foreach((line) =>{
			item=line
					matrix(row,column)=item.toDouble
					column+=1
					if (column==3){
						column=0
						row+=1
					}
		})
		return (matrix)
	}
}