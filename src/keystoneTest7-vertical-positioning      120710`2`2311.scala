import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,tan=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _, abs=>_, _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
/* For OpenGl
 * 1. put jogl.jar and gluegen-rt.jar from processing libiray: H:\Program Files\processing-1.5.1\modes\java\libraries\opengl\library\windows32
 * 2. set jogl.jar's native library to where it comes from.
 * 3. OR, find jogl.dll and jogl_awt.dll and put them in the lib and set native library to it.
 */
import processing.opengl._;

object keystoneTest7 extends PApplet{
	val tuningDepth=true
	val testingDepthMagnification=false
	val tuningCoordinate=false
	val tuningTranslate=false
	

	var kinect: SimpleOpenNI  = null;
//	var tuneDepth = -1.toFloat//(0.6375).toFloat;
	var tuneDepth = (0.5578125).toFloat
	var tuneShift: Float = 1.5.toFloat
//	var tuneCoordinateX: Float = 843.toFloat
//	var tuneCoordinateY: Float = 2831.toFloat
	var tuneCoordinateX: Float = 0.3328.toFloat
	var tuneCoordinateY: Float = 0.1375.toFloat
	var tuneTranslateX: Float = 1.toFloat
	var tuneTranslateY: Float = 1.toFloat
	var depthMagnification = 1.toFloat
	
	var homographyMatrix = Matrix.zeros[Double](3,3)
	var depthProjectorH=loadHomography3(("depth-projector homography.txt"))
	var rgbDepthH=loadHomography3("rgb-depth homography.txt")
	
	var q1:PVector = new PVector(0,0,0);
	var q2:PVector = new PVector(640,0,0);
	var q3:PVector = new PVector(640,480,0);
	var q4:PVector = new PVector(0,480,0);
	var centerPoint = new PVector(640/2, 480/2, 0)
	var pointt = 0;
	var lasttime=second()+minute()*60;
	var lasttime2=second()+minute()*60;
  
	var  dx:Float = 0.toFloat;
	var  dy:Float = 0.toFloat;
	var  dz:Double = 0.0;
	var thetaY=0.toFloat
	
	val perPixelPerDistance: Float=1045.toFloat/966/600

	val whitePage = createImage(600,450,RGB)
	whitePage.loadPixels()
	for (i <- 0 to 600*450-1){whitePage.pixels(i)=color(255)}

	var proj1=new PVector(0,0)
	var proj2=new PVector(640,0)
	var proj3=new PVector(640,480)
	var proj4=new PVector(0,480)
	var cam1=new PVector(98,102)
	var cam2=new PVector(255,96)
	var cam3=new PVector(255,204)
	var cam4=new PVector(98,198)
	val proj=Array[PVector](proj1,proj2,proj3,proj4)
	val cam=Array[PVector](cam1,cam2,cam3,cam4)

	var p=new PVector(0,0)

	
	
	
	def main(args: Array[String]) {

		val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(keystoneTest7)
		keystoneTest7.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
	}
	override def setup() {
		size(640*2, 480,OPENGL);
		kinect = new SimpleOpenNI(this);
		kinect.enableRGB();
		kinect.enableDepth();
	}

	override def draw() {
		kinect.update();
		background(0);
		val imagee = kinect.rgbImage();
		
		//draw axis
		imagee.loadPixels()
		for (i <- 0 to 640-1){imagee.pixels(i+imagee.height/2*imagee.width)=color(0,255,0)}
		for (i <- 0 to 480-1){imagee.pixels(imagee.width/2+i*imagee.width)=color(0,255,0)}
		imagee.updatePixels()
		//
		
//		image(imagee, 0, 0);
		
		//calculate corner depth
		if (second()+minute()*60-lasttime>0.5){
			val depthValues = kinect.depthMap();
			var a = new PVector(200,150,0);
			var b = new PVector(400,260,0);
			var c= new PVector(250, 300,0);
			a.z=(1)*depthValues((a.x+a.y*480).toInt)//-(tuneDepth-1/2)*600
			b.z=(1)*depthValues((b.x+b.y*480).toInt)//-(tuneDepth-1/2)*600
			c.z=(1)*depthValues((c.x+c.y*480).toInt)//-(tuneDepth-1/2)*600
			
//			println(depthValues((centerPoint.x+centerPoint.y*600).toInt))

//			q1.z=(-1)*depthValues((q1.x+100+(q1.y+100)*600).toInt)*tuneDepth;
//			q2.z=(-1)*depthValues((q2.x-100+(q2.y+100)*600).toInt)*tuneDepth;
//			q3.z=(-1)*depthValues((q3.x-100+(q3.y-100)*600).toInt)*tuneDepth;
//			q4.z=(-1)*depthValues((q4.x+100+(q4.y-100)*600).toInt)*tuneDepth;
//			subtractCornerDepths()
					
			/* [replaced by subtractConorDepth().]
				val adjust=min3(a.z,b.z,c.z)+150;
				a.z=a.z-adjust;
				b.z=b.z-adjust;
				c.z=c.z-adjust;
			*/
			math6(a,b,c);
			lasttime=second()+minute()*60;
		}
		
		//drawing
		stroke(0,255,0);
//		background(0);
		//    fill(0,255,0);
		//    rect(0+100,100,640-100,480-100);
//		translate(-tuneDepth*centerPoint.z*dx,-tuneDepth*centerPoint.z*dy)
//		translate(centerPoint.z*dx,centerPoint.z*dy)
//		translate(0,3*(mouseY-480/2))
//		translate(translateProjection().x,translateProjection().y)
		beginShape();
		textureMode(NORMALIZED);
//		texture(imagee);
		texture(whitePage)
//		vertex(q1.x, q1.y, q1.z, 0, 0);
//		vertex(q2.x, q2.y, q2.z, 1, 0);
//		vertex(q3.x, q3.y, q3.z, 1, 1);
//		vertex(q4.x, q4.y, q4.z, 0, 1);
		
		vertex(q1.x, q1.y, 0, 0);
		vertex(q2.x, q2.y, 1, 0);
		vertex(q3.x, q3.y, 1, 1);
		vertex(q4.x, q4.y, 0, 1);
		endShape();
		
		//draw axis
		stroke(0,0,255)
		line(0,480/2,640,480/2)
		line(640/2,0,640/2,480)
		
//		if (second()+minute()*60-lasttime2>2){
//			println("");
//			println(q1.z);
//			println(q2.z);
//			println(q3.z);
//			println(q4.z);
//			lasttime2=second()+minute()*60;
//		}

		image(kinect.rgbImage(),640,0)
	}
	override def mousePressed(){
	  
		if (tuningCoordinate && mouseButton==LEFT){
			tuneCoordinateX = mouseX.toFloat/640.toFloat
//					println(mouseX)
			println("set tuneCoordinateX valut to " + tuneCoordinateX.toString)
			tuneCoordinateY = mouseY.toFloat/480.toFloat
//					println(mouseY)
			println("set tuneCoordinateX valut to " + tuneCoordinateY.toString)
		}
		if (tuningTranslate && mouseButton==RIGHT ){
//			tuneTranslateX = (640/2-mouseX.toFloat)/(640/2).toFloat
//			tuneTranslateY = (480/2-mouseY.toFloat)/(480/2).toFloat
			tuneTranslateX = mouseX.toFloat/640.toFloat
			tuneTranslateY = mouseY.toFloat/480.toFloat
			
			println("set tuneTranslateX valut to " + tuneCoordinateX.toString)
			println("set tuneTranslateY valut to " + tuneCoordinateY.toString)
		}
		if (tuningDepth){
		    //for tuneDepth. we use this now because the homography between homography and camera are not set yet.
			tuneDepth = mouseX.toFloat/640
			println(mouseX)
			println("set tuneDepth valut to " + tuneDepth.toString)
		}
		if (testingDepthMagnification){
			depthMagnification = mouseX.toFloat/640
			println("set depthMagnification valut to " + depthMagnification.toString)
		}
	}
	
	def math6(a: PVector, b: PVector, c: PVector){
		if (a.z>0 && b.z>0){
			resetProjection()
			
			calculateThetaY(a,b)

//		println(thetaY/3.1415926*180)
			
			q1.z=centerPoint.z/(q1.y*perPixelPerDistance*tan(thetaY)+1).toFloat
			q2.z=centerPoint.z/(q2.y*perPixelPerDistance*tan(thetaY)+1).toFloat
			q3.z=centerPoint.z/(q3.y*perPixelPerDistance*tan(thetaY)+1).toFloat
			q4.z=centerPoint.z/(q4.y*perPixelPerDistance*tan(thetaY)+1).toFloat
			
			
//			println("======================================")
		    coordinateProjection4()
//			println(q1.x)
//		    println(q2.x)
//			println(q4.x)
//			println(q3.x)
//			println(q1.y)
//		    println(q2.y)
//			println(q4.y)
//			println(q3.y)
		    biggestAreaProjection()
//		    println("------")
//			println(q1.x)
//		    println(q2.x)
//			println(q4.x)
//			println(q3.x)
//			println(q1.y)
//		    println(q2.y)
//			println(q4.y)
//			println(q3.y)
		    counterProjection2()
//		    println("------")
//			println(q1.x)
//		    println(q2.x)
//			println(q4.x)
//			println(q3.x)
//			println(q1.y)
//		    println(q2.y)
//			println(q4.y)
//			println(q3.y)
		    biggestAreaProjection2()
//		    println("------")
//			println(q1.x)
//		    println(q2.x)
//			println(q4.x)
//			println(q3.x)
//			println(q1.y)
//		    println(q2.y)
//			println(q4.y)
//			println(q3.y)
		    
		}else{
			println("zero depth")
		}
		
	}
	def coordinateProjection5(){
		q1.x=q1.x/(q1.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q2.x=q2.x/(q2.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q3.x=q3.x/(q3.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q4.x=q4.x/(q4.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q1.y=q1.y/((q1.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
		q2.y=q2.y/((q2.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
		q3.y=q3.y/((q3.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
		q4.y=q4.y/((q4.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
	}
	def coordinateProjection4(){
		q1.x=q1.x/(q1.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q2.x=q2.x/(q2.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q3.x=q3.x/(q3.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q4.x=q4.x/(q4.y*perPixelPerDistance*tan(thetaY)+1).toFloat
		q1.y=q1.y/((q1.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
		q2.y=q2.y/((q2.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
		q3.y=q3.y/((q3.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
		q4.y=q4.y/((q4.y*perPixelPerDistance*tan(thetaY)+1)*cos(thetaY)).toFloat
	}
	def counterProjection(){
		q1.z*=(-1).toFloat
		q2.z*=(-1).toFloat
		q3.z*=(-1).toFloat
		q4.z*=(-1).toFloat
		centerPoint.z*=(-1).toFloat
		
	}
	def counterProjection2(){
		cam(0)=new PVector(q1.x,q1.y)
		cam(1)=new PVector(q2.x,q2.y)
		cam(2)=new PVector(q3.x,q3.y)
		cam(3)=new PVector(q4.x,q4.y)
		computeHomography(cam,proj)
//		homographyMatrix=inv(homographyMatrix)
//		writeHomography("screenHomography.data")
		resetProjection0()
		q1=applyToPoint2(q1)
		q2=applyToPoint2(q2)
		q3=applyToPoint2(q3)
		q4=applyToPoint2(q4)
		
	}
	def counterProjection3(){
		cam(0)=new PVector(q1.x,q1.y)
		cam(1)=new PVector(q2.x,q2.y)
		cam(2)=new PVector(q3.x,q3.y)
		cam(3)=new PVector(q4.x,q4.y)
		computeHomography(cam,proj)
//		homographyMatrix=inv(homographyMatrix)
//		writeHomography("screenHomography.data")
		resetProjection0()
		q1=applyToPoint2(q1)
		q2=applyToPoint2(q2)
		q3=applyToPoint2(q3)
		q4=applyToPoint2(q4)
		
	}
	


	
	def biggestAreaProjection(){
		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
		val centerX=(rightX-leftX)/2+leftX
		val centerY=(lowerY-upperY)/2+upperY
		val ratioX=640/(rightX-leftX)
		val ratioY=480/(lowerY-upperY)
		val ratio=min2(ratioX,ratioY)
		var shiftX=0.toFloat
		var shiftY=0.toFloat
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-centerX)*ratio+640/2
		q1.y=(q1.y-centerY)*ratio+480/2
		q2.x=(q2.x-centerX)*ratio+640/2
		q2.y=(q2.y-centerY)*ratio+480/2
		q3.x=(q3.x-centerX)*ratio+640/2
		q3.y=(q3.y-centerY)*ratio+480/2
		q4.x=(q4.x-centerX)*ratio+640/2
		q4.y=(q4.y-centerY)*ratio+480/2
		if (ratioX<ratioY){
			if (dy>0){
				shiftX=0
				shiftY=(-1).toFloat*min2(q1.y,q2.y)
			}else{
				shiftX=0
				shiftY=min2(480-q3.y,480-q4.y)
			}
		}else{
			if (dx<0){
				shiftY=0
				shiftX=min2(640-q2.x,640-q3.x)
			}else{
				shiftY=0
				shiftX=(-1).toFloat*min2(q1.x,q4.x)
			}
		}
		q1.x+=shiftX
		q2.x+=shiftX
		q3.x+=shiftX
		q4.x+=shiftX
		q1.y+=shiftY
		q2.y+=shiftY
		q3.y+=shiftY
		q4.y+=shiftY
	}
	def biggestAreaProjection2(){
		val upperY=min4(q1.y,q2.y,q3.y,q4.y)
		val lowerY=max4(q1.y,q2.y,q3.y,q4.y)
		val leftX=min4(q1.x,q2.x,q3.x,q4.x)
		val rightX=max4(q1.x,q2.x,q3.x,q4.x)
		val centerX=(rightX-leftX)/2+leftX
		val centerY=(lowerY-upperY)/2+upperY
		val ratioX=640/(rightX-leftX)
		val ratioY=480/(lowerY-upperY)
		val ratio=min2(ratioX,ratioY)
		var shiftX=0.toFloat
		var shiftY=0.toFloat
//		println(ratioY.toString + "      123")
		q1.x=(q1.x-centerX)*ratio+640/2
		q1.y=(q1.y-centerY)*ratio+480/2
		q2.x=(q2.x-centerX)*ratio+640/2
		q2.y=(q2.y-centerY)*ratio+480/2
		q3.x=(q3.x-centerX)*ratio+640/2
		q3.y=(q3.y-centerY)*ratio+480/2
		q4.x=(q4.x-centerX)*ratio+640/2
		q4.y=(q4.y-centerY)*ratio+480/2
		if (ratioX<ratioY){
			if (dy>0){
				shiftX=0
				shiftY=(-1).toFloat*min2(q1.y,q2.y)
			}else{
				shiftX=0
				shiftY=min2(480-q3.y,480-q4.y)
			}
		}else{
			if (dx<0){
				shiftY=0
				shiftX=min2(640-q2.x,640-q3.x)
			}else{
				shiftY=0
				shiftX=(-1).toFloat*min2(q1.x,q4.x)
			}
		}
		q1.x-=shiftX
		q2.x-=shiftX
		q3.x-=shiftX
		q4.x-=shiftX
		q1.y-=shiftY
		q2.y-=shiftY
		q3.y-=shiftY
		q4.y-=shiftY
	}
	def resetProjection(){
		q1 = new PVector(0-640/2,0-480/2,0);
		q2 = new PVector(640-640/2,0-480/2,0);
		q3 = new PVector(640-640/2,480-480/2,0);
		q4 = new PVector(0-640/2,480-480/2,0);
	}
	def resetProjection0(){
		q1 = new PVector(0,0,0);
		q2 = new PVector(640,0,0);
		q3 = new PVector(640,480,0);
		q4 = new PVector(0,480,0);
	}
	def calculateThetaY(a: PVector, b: PVector){
			val y1=a.y-480/2
			val y2=b.y-480/2
			val Y1=a.z*y1*perPixelPerDistance
			val Y2=b.z*y2*perPixelPerDistance
			centerPoint.z=(a.z*Y2-b.z*Y1)/(Y2-Y1)
			thetaY=tuneDepth*(atan((centerPoint.z-a.z)/(a.z*y1*perPixelPerDistance))).toFloat
	}

	def min2(a: Float, b: Float):Float={
		return minArray(Array(a,b))
	}
	def min3(a: Float, b: Float, c: Float):Float={
		return minArray(Array(a,b,c))
	}
	def min4(a: Float, b:Float, c:Float, d:Float):Float={
		return minArray(Array(a,b,c,d))
	}
	def minArray(input: Array[Float]):Float={
		var minn = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)<minn){minn=input(i)}
		}
		return minn
	}
	def max4(a: Float, b:Float, c:Float, d:Float):Float={
		return maxArray(Array(a,b,c,d))
	}
	def maxArray(input: Array[Float]):Float={
		var maxx = input(0)
		for (i <- 1 to (input.length-1)){
			if (input(i)>maxx){maxx=input(i)}
		}
		return maxx
	}	
	
	
	def loadHomography3(fileName: String): scalala.tensor.dense.DenseMatrix[Double] = {
		var matrix = Matrix.zeros[Double](3,3)
		var item:String=""
		var column=0
		var row=0
		val file = Source.fromFile(fileName)
		file.getLines.foreach((line) =>{
			item=line
					matrix(row,column)=item.toDouble
					column+=1
					if (column==3){
						column=0
						row+=1
					}
		})
		return (matrix)
	}
	
	
	

	def computeHomography(cameraPoints: Array[PVector], projectorPoints: Array[PVector]){  
		if (cameraPoints.length!=projectorPoints.length){println("Different points amount bewteen camera and projector image! Please have a double check.")}
		var numberOfPoints: Int = (cameraPoints.length)

				//initializing scalala matrixs cam and proj
				var cam = Matrix.zeros[Double](numberOfPoints,2)
				var proj = Matrix.zeros[Double](numberOfPoints,2)
				//assign values from input array to cam and proj
				for (i <- 0 to (numberOfPoints-1)){
					cam(i,0)=cameraPoints(i).x.toDouble
							cam(i,1)=cameraPoints(i).y.toDouble
							proj(i,0)=projectorPoints(i).x.toDouble
							proj(i,1)=projectorPoints(i).y.toDouble
				}

		// Creates the estimation matrix
		var matrix1 = Matrix.zeros[Double](numberOfPoints*2,9)
				//initializing row1 and row2
				var row1 = DenseVector.zeros[Double](9)
				var row2 = DenseVector.zeros[Double](9)
				//assign estimation matrix entries values from cam and proj 
				for (i <- 0 to (numberOfPoints-1)){
					row1 = DenseVector(cam(i,0), cam(i,1), 1, 0, 0, 0, -cam(i,0)*proj(i,0), -cam(i,1)*proj(i,0), -proj(i,0))
							row2 = DenseVector(0, 0, 0, cam(i,0), cam(i,1), 1, -cam(i,0)*proj(i,1), -cam(i,1)*proj(i,1), -proj(i,1))
							matrix1(i*2,::) := row1
							matrix1(i*2+1,::) := row2
				}

		//calculation eigenvalues and eigenvectors
		var matrix2 = matrix1.t * matrix1		
				val (eigenvalues,valuee0,eigenvectors)=eig(matrix2)


				//find the minium eigenvalue position
				var minPosition=0
				var minValue=eigenvalues(0)
				for (i <- 0 to 8){
					if (eigenvalues(i)<minValue){
						minValue=eigenvalues(i)
								minPosition=i
					}
				}

		//get the eigenvector correspond to the minumn eigenvalue
		var eigenvector = eigenvectors(::,minPosition)

				//write the eigenvector to homography matrix
				for(i <- 0 to 2){
					for(j <- 0 to 2){
						homographyMatrix(i,j)=eigenvector(i+3*j)
					}
				}

		//invert matrix for applying to coordinates
		//homographyMatrix=inv(homographyMatrix)
		

	}	

	def applyToPoint2(inputPoint: PVector):PVector={
			var X=inputPoint.x
					var Y=inputPoint.y
					var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
					for(i <- 0 to 2){
						for(j <- 0 to 2){
							p(i+3*j+1)=homographyMatrix(i,j)
						}
					}

			val x:Float=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
					val y:Float=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat
					val outputPoint: PVector=new PVector(x,y,inputPoint.z)
					return outputPoint
	}
	
	
}