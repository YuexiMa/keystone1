//version 3.1

import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet.{min => _, max => _, sqrt => _, atan=>_, cos=>_,sin=>_, abs=>_,tan=>_,  _}//exclude min and max to avoid repeats 
import processing.core.PGraphics._
import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library.{min => _, max => _, sqrt => _, abs=>_, _}//exclude min and max to avoid repeats with scala function
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
//import processing.opengl._;


class cameraTransformationClass extends PApplet{
	
	var PPdepth=setPPdepth()
	var PPdepth2=setPPdepth2()
	var PPdepthInv=inv(PPdepth2)
	var PPrgbInDepth=setPPrgbInDepth()
	var	PPrgbInRGB=setPPrgbInRGB()
	var	PPprojector=setPPprojector()
	var M10depth2rgb=PPrgbInDepth*inv(PPdepth2)
	var M10rgb2projector=PPprojector*inv(PPrgbInRGB)
	var M10depth2projector=M10rgb2projector*M10depth2rgb

	var position: PVector = new PVector(0,0,0);
	var homographyFromProjectorToDepth = new homographyClass()
	var homographyFromRGBToDepth = new homographyClass()
	var homographySet=false
	
	def main(args: Array[String]) {
	}
	
	def returnPPdepth(): scalala.tensor.dense.DenseMatrix[Double]={
		return PPdepth2
	}
	def setHomography(cam1:PVector, cam2:PVector, cam3:PVector,cam4:PVector) {
		  val proj1=depthToProjector2(cam1)
		  val proj2=depthToProjector2(cam2)
	      val proj3=depthToProjector2(cam3)
		  val proj4=depthToProjector2(cam4)
	      val proj=Array[PVector](proj1,proj2,proj3,proj4)
		  val cam=Array[PVector](cam1,cam2,cam3,cam4)
	      homographyFromProjectorToDepth.computeHomography(proj,cam)
//	      homographyFromProjectorToDepth.homographyMatrix=inv(homographyFromProjectorToDepth.homographyMarix)
	      homographySet=true
	}
	def setHomographyFromRGBToDepth(cam1:PVector, cam2:PVector, cam3:PVector,cam4:PVector) {
		  val proj1=depthToRGB2(cam1)
		  val proj2=depthToRGB2(cam2)
	      val proj3=depthToRGB2(cam3)
		  val proj4=depthToRGB2(cam4)
	      val proj=Array[PVector](proj1,proj2,proj3,proj4)
		  val cam=Array[PVector](cam1,cam2,cam3,cam4)
	      homographyFromRGBToDepth.computeHomography(proj,cam)
	}
	def getDisparity(depth:Float):Float={
		val doff=1090
		val b=75
		val f=525
		//equation: dpeth = b*f / (1/8 * (doff - disparity))
		return -8*b*f/depth+doff
	}
	
	def depthTo3D(input:PVector):PVector= {
		val xx=DenseVector(input.x,input.y,1,getDisparity(input.z))
		val xx1=PPdepthInv*xx
		return (new PVector((xx1(0)/xx1(3)).toFloat,(xx1(1)/xx1(3)).toFloat,(xx1(2)/xx1(3)).toFloat))
	}
	def depthToWall(input:PVector,rotationMatrix:scalala.tensor.dense.DenseMatrix[Double]):PVector= {
		val xx=DenseVector(input.x,input.y,1,getDisparity(input.z))
		val xx1=rotationMatrix*PPdepthInv*xx
		return (new PVector((xx1(0)/xx1(3)).toFloat,(xx1(1)/xx1(3)).toFloat,(xx1(2)/xx1(3)).toFloat))
	}
	def applyMatrix(input:PVector,matrix:scalala.tensor.dense.DenseMatrix[Double]):PVector= {
		val xx=DenseVector(input.x,input.y,input.z,1)
		val xx1=matrix*xx
		return (new PVector((xx1(0)/xx1(3)).toFloat,(xx1(1)/xx1(3)).toFloat,(xx1(2)/xx1(3)).toFloat))
	}
	def depthToProjectorTest(input: PVector):PVector={
		val xx=DenseVector(input.x,input.y,1,input.z)
		val xx1=M10depth2projector*xx
		return (new PVector((xx1(0)/xx1(2)).toFloat,(xx1(1)/xx1(2)).toFloat,(xx1(3)/xx1(2)).toFloat))
	}
		
	def depthToProjector4(input: PVector):PVector={
		val xx=DenseVector(input.x,input.y,1,computeDisperity(input))
		val xx1=M10depth2projector*xx
		
		return (new PVector((xx1(0)/xx1(2)).toFloat,(xx1(1)/xx1(2)).toFloat,(xx1(3)/xx1(2)).toFloat))
	}
	def depthToRGB4(input: PVector):PVector={
		val xx=DenseVector(input.x,input.y,1,computeDisperity(input))
		val xx1=M10depth2rgb*xx
		return (new PVector((xx1(0)/xx1(2)).toFloat,(xx1(1)/xx1(2)).toFloat,(xx1(3)/xx1(2)).toFloat))
	}
	def depthToProjector2(input: PVector):PVector={
		val xx=DenseVector(input.x,input.y,1,getDisparity(input.z))
		val xx1=M10depth2projector*xx
		
		return (new PVector((xx1(0)/xx1(2)).toFloat,(xx1(1)/xx1(2)).toFloat,(xx1(3)/xx1(2)).toFloat))
	}
	def depthToRGB2(input: PVector):PVector={
		val xx=DenseVector(input.x,input.y,1,getDisparity(input.z))
		val xx1=M10depth2rgb*xx
		return (new PVector((xx1(0)/xx1(2)).toFloat,(xx1(1)/xx1(2)).toFloat,(xx1(3)/xx1(2)).toFloat))
	}	

	def depthToProjector(input: PVector):PVector={
		return depthToProjector2(input)
	}
	def projectorToDepth(input: PVector):PVector= {
		if (homographySet==false) {println("homography not set yet")}
		return homographyFromProjectorToDepth.applyToPoint(input)
	}
	def RGBToDepth(input: PVector):PVector= {
		return homographyFromRGBToDepth.applyToPoint(input)
	}
	
def computeDisperity(input: PVector):Float={
	val p0=PPdepth(0,0)
	val p1=PPdepth(0,1)
	val p2=PPdepth(0,2)
	val p3=PPdepth(0,3)
	val p4=PPdepth(1,0)
	val p5=PPdepth(1,1)
	val p6=PPdepth(1,2)
	val p7=PPdepth(1,3)
	val p8=PPdepth(2,0)
	val p9=PPdepth(2,1)
	val p10=PPdepth(2,2)
	val p11=PPdepth(2,3)
	val p12=PPdepth(3,0)
	val p13=PPdepth(3,1)
	val p14=PPdepth(3,2)
	val p15=PPdepth(3,3)
	val i=input.x
	val j=input.y
	val z=input.z
	val a=1-i*p8/p0
	val b=1-j*p8/p4
	val yy=((i*p10*z+i*p11-p2*z-p3)/p0/a-(i*p10*z+j*p11-p6*z-p7)/p4/b)/((j*p9-p5)/p4/b-(i*p9-p1)/p0/a)
	val xx=(i*p9-p1)/(p0*a)*yy+(i*p10*z+i*p11-p2*z-p3)/p0/(1-i*p8/p0)
	val w=p8*xx+p9*yy+p10*z+p11
	return ((p12*xx+p13*yy+p14*z+p15)/w).toFloat
	
}	
	

	def setPPdepth():scalala.tensor.dense.DenseMatrix[Double]={
			val fx=590.46551.toFloat
			val fy=594.22814.toFloat
			val cx=327.25031.toFloat
			val cy=242.93381.toFloat
			val s=0.toFloat
			val r1=0.toFloat
			val r2=0.toFloat
			val r3=0.toFloat
			val tx=0.toFloat
			val ty=0.toFloat
			val tz=0.toFloat
			return getPP(fx,fy,cx,cy,s,r1,r2,r3,tx,ty,tz)
	}	
	def setPPdepth2():scalala.tensor.dense.DenseMatrix[Double]={//in depth camera coordinates
			val fx=590.46551.toFloat
			val fy=594.22814.toFloat
			val cx=327.25031.toFloat
			val cy=242.93381.toFloat
			val s=0.toFloat
			val r1=0.toFloat
			val r2=0.toFloat
			val r3=0.toFloat
			val tx=0.toFloat
			val ty=0.toFloat
			val tz=0.toFloat
			return getPP2(fx,fy,cx,cy,s,r1,r2,r3,tx,ty,tz)
	}
	def setPPrgbInDepth():scalala.tensor.dense.DenseMatrix[Double]={//in depth camera coordinates
			val fx=528.36876.toFloat
			val fy=531.91103.toFloat
			val cx=320.91889.toFloat
			val cy=259.33948.toFloat
			val s=0.toFloat
			val r1=(-0.00267).toFloat
			val r2=(0.00263).toFloat
			val r3=(0.00593).toFloat
			val tx=(-24.94831).toFloat
			val ty=(-0.0620).toFloat
			val tz=(0.43037).toFloat
			return getPP(fx,fy,cx,cy,s,r1,r2,r3,tx,ty,tz)	
	}
	
	def setPPrgbInRGB():scalala.tensor.dense.DenseMatrix[Double]={//in rgb camera coordinates
			val fx=528.36876.toFloat
			val fy=531.91103.toFloat
			val cx=320.91889.toFloat
			val cy=259.33948.toFloat
			val s=0.toFloat
			val r1=0.toFloat
			val r2=0.toFloat
			val r3=0.toFloat
			val tx=0.toFloat
			val ty=0.toFloat
			val tz=0.toFloat
			return getPP(fx,fy,cx,cy,s,r1,r2,r3,tx,ty,tz)	
	}
	def setPPprojector():scalala.tensor.dense.DenseMatrix[Double]={//in rgb camera coordinates
			val fx=1074.52250.toFloat
			val fy=1052.52017.toFloat
			val cx=409.21261.toFloat
			val cy=482.00961.toFloat
			val s=0.toFloat
			val r1=(0.1677).toFloat
			val r2=(-0.0053).toFloat
			val r3=(-0.0136).toFloat
			val tx=(14.2731).toFloat
			val ty=(29.6701).toFloat
			val tz=(-21.4575).toFloat
			return getPP(fx,fy,cx,cy,s,r1,r2,r3,tx,ty,tz)
	}
	//---------------------------------------------------------------------------
	
	//===========================================================================
	def getPP(fx:Float,fy:Float,cx:Float,cy:Float,s:Float,r1:Float,r2:Float,r3:Float,tx:Float,ty:Float,tz:Float):scalala.tensor.dense.DenseMatrix[Double]={
		return getKK(fx,fy,cx,cy,s)*getE(r1,r2,r3,tx,ty,tz)
	}
	def getPP2(fx:Float,fy:Float,cx:Float,cy:Float,s:Float,r1:Float,r2:Float,r3:Float,tx:Float,ty:Float,tz:Float):scalala.tensor.dense.DenseMatrix[Double]={
		var matrix = getKK(fx,fy,cx,cy,s)*getE(r1,r2,r3,tx,ty,tz)
		val diff=1090
		val b=75
		val f=525
		matrix(3,2)= diff.toDouble
		matrix(3,3)=(-8*b*f).toDouble
		return matrix
	}
	
	def getE(r1:Float,r2:Float,r3:Float,tx:Float,ty:Float,tz:Float):scalala.tensor.dense.DenseMatrix[Double]={
  		var matrix = getR(r1,r2,r3)
		matrix(0,3) = tx
		matrix(1,3) = ty
		matrix(2,3) = tz
		
		return matrix
	}
	def getR(r1:Float,r2:Float,r3:Float):scalala.tensor.dense.DenseMatrix[Double]={
  		var Rz= Matrix.zeros[Double](4,4)
		Rz(0,::) := DenseVector(cos(r3),-sin(r3),0,0)
		Rz(1,::) := DenseVector(sin(r3),cos(r3),0,0)
		Rz(2,::) := DenseVector(0,0,1,0)
		Rz(3,::) := DenseVector(0,0,0,1)
  		var Ry= Matrix.zeros[Double](4,4)
		Ry(0,::) := DenseVector(cos(r2),0,sin(r2),0)
		Ry(1,::) := DenseVector(0,1,0,0)
		Ry(2,::) := DenseVector(-sin(r2),0,cos(r2),0)
		Ry(3,::) := DenseVector(0,0,0,1)
  		var Rx= Matrix.zeros[Double](4,4)
		Rx(0,::) := DenseVector(1,0,0,0)
		Rx(1,::) := DenseVector(0,cos(r1),-sin(r1),0)
		Rx(2,::) := DenseVector(0,sin(r1),cos(r1),0)
		Rx(3,::) := DenseVector(0,0,0,1)
		
		return Rz*Ry*Rx
	}
	def getRKinect(r1:Float,r2:Float):scalala.tensor.dense.DenseMatrix[Double]={
  		var Ry= Matrix.zeros[Double](4,4)
		Ry(0,::) := DenseVector(cos(r2),0,sin(r2),0)
		Ry(1,::) := DenseVector(0,1,0,0)
		Ry(2,::) := DenseVector(-sin(r2),0,cos(r2),0)
		Ry(3,::) := DenseVector(0,0,0,1)
  		var Rx= Matrix.zeros[Double](4,4)
		Rx(0,::) := DenseVector(1,0,0,0)
		Rx(1,::) := DenseVector(0,cos(r1),-sin(r1),0)
		Rx(2,::) := DenseVector(0,sin(r1),cos(r1),0)
		Rx(3,::) := DenseVector(0,0,0,1)
		
		return Rx*Ry
	}
	def getKK(fx:Float,fy:Float,cx:Float,cy:Float,s:Float):scalala.tensor.dense.DenseMatrix[Double]={//verified 120718`3`1539
		var matrix= Matrix.zeros[Double](4,4)
		matrix(0,::) := DenseVector(fx.toDouble,s.toDouble, cx.toDouble,0.toDouble)
		matrix(1,::) := DenseVector(0.toDouble, fy.toDouble,cy.toDouble,0.toDouble)
		matrix(2,::) := DenseVector(0.toDouble, 0.toDouble, 1.toDouble, 0.toDouble)
		matrix(3,::) := DenseVector(0.toDouble, 0.toDouble, 0.toDouble, 1.toDouble)
		return matrix
	}

}