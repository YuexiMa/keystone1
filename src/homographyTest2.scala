import math._
import scala.util.Random
import processing.core._
import processing.core.PConstants._
import processing.core.PApplet._
import processing.core.PGraphics._
//import SimpleOpenNI._
import scalala.scalar._
import scalala.tensor.::;
import scalala.tensor.mutable._;
import scalala.tensor.dense._;
import scalala.tensor.sparse._;
import scalala.library.Library._;
import scalala.library.LinearAlgebra._;
import scalala.library.Statistics._;
import scalala.library.Plotting._;
import scalala.operators.Implicits._;
import scala.io.Source;
import java.io._;
//for OpenGl
//build path add native lib from opengl in processing/model...
//import processing.jogl;
//import processing.gluegen.
import javax.media.opengl._;
import processing.opengl._;
/**two way to configure opengl:
 * 1: add opengl and gluegen-rt.jar from processing library, and import the following:
 *   processing.opengl._
 *   javax.media.opengl._
 * 2. add the folder called ProcessingEclipseTemplate's libraries, and link jogl and gluegen-rt's natives libiries from the folder in the folder.
 */
object homographyTest2 extends PApplet{
  
//  var context: SimpleOpenNI  = null
  var homographyMatrix = Matrix.zeros[Double](3,3)
  val imagee = loadImage("123.png")
  println(1)
  def main(args: Array[String]) {
    	
        val frame = new javax.swing.JFrame("TITLE")
		frame.getContentPane().add(homographyTest2)
		homographyTest2.init
		frame.setResizable(true)
		frame.pack
		frame.setVisible(true) 
  }
  def pointExample2(){//this version uses PVector to compute homography
	  val proj1=new PVector(100,100)
	  val proj2=new PVector(200,100)
      val proj3=new PVector(200,200)
	  val proj4=new PVector(100,200)
	  val cam1=new PVector(98,102)
	  val cam2=new PVector(255,96)
	  val cam3=new PVector(255,204)
	  val cam4=new PVector(98,198)
      val proj=Array[PVector](proj1,proj2,proj3,proj4)//unit squire
	  val cam=Array[PVector](cam1,cam2,cam3,cam4)

      computeHomography(cam,proj)
	  
	  val testPoint=new PVector(100,100)
	  println(applyToPoint(testPoint))
  }
  
  def pointTest1(){//this version uses PVector to compute homography
	  val proj1=new PVector(0,0)
	  val proj2=new PVector(640,0)
      val proj3=new PVector(480,640)
	  val proj4=new PVector(0,480)
	  val cam1=new PVector(213,63)
	  val cam2=new PVector(545,72)
	  val cam3=new PVector(559,318)
	  val cam4=new PVector(217,324)
      val proj=Array[PVector](proj1,proj2,proj3,proj4)//unit squire
	  val cam=Array[PVector](cam1,cam2,cam3,cam4)

      computeHomography(cam,proj)
	  
	  val testPoint=new PVector(538,479)
	  println(123)
	  println(applyToPoint(testPoint))
  }
  override def setup() {
//      
//	  context = new SimpleOpenNI(this);
//	   
//	  // mirror is by default enabled
//	  context.setMirror(true);
//	  // enable depthMap generation 
//	  if(context.enableDepth() == false)
//	  {
//	     println("Can't open the depthMap, maybe the camera is not connected!"); 
//	     exit();
//	     return;
//	  }
//	  
//	  // enable ir generation
//	  //context.enableRGB(640,480,30);
//	  //context.enableRGB(1280,1024,15);  
//	  if(context.enableRGB() == false)
//	  {
//	     println("Can't open the rgbMap, maybe the camera is not connected or there is no rgbSensor!"); 
//	     exit();
//	     return;
//	  }
//	  
//	  size(context.depthWidth() + context.rgbWidth() + 10, context.rgbHeight(),P3D)
	  
	  //Homography
	  //val cam=Array[Double](1,1,2,1,2,2,1,2)//unit squire
	  //val proj=Array[Double](0.8,1.2,2.5,0.6,2.5,2.4,0.8,1.8)//T-shape
      size(600,600,OPENGL)
      //size(800,800,P3D)
      pointTest1()
      homographyMatrix=inv(homographyMatrix)
  }

  override def draw() {
	  //set(1,1,color(0,255,0))
      //image(applyToPImage(imagee),0,0,400,300)
      //line(120,120, 180,120)
      //val transformedCoordinates=applyToCoordinates(Array[Double](120,120,180,120))
      //line(transformedCoordinates(0).toInt,transformedCoordinates(1).toInt,transformedCoordinates(2).toInt,transformedCoordinates(3).toInt)
      //stop()
	  //println(1)
      //delay(1000)
      //image(imagee,0,0,width,height)
      //println(2)
      //delay(1000)
    
	  //image(applyToPImage2(imagee),0,0,imagee.width,imagee.height)
      
	  noStroke();
	  textureMode(NORMALIZED);
	  beginShape(QUADS);
	  texture(imagee);
	  val corner1=applyToCoordinate3(0,0)
	  val corner2=applyToCoordinate3(width,0)
	  val corner3=applyToCoordinate3(width,height)
	  val corner4=applyToCoordinate3(0,height)
//	  vertex(corner1(0), corner1(1), 0.toFloat, 0.toFloat);
//	  vertex(corner4(0), corner4(1), 0.toFloat, 1.toFloat);
//	  vertex(corner3(0), corner3(1), 1.toFloat, 1.toFloat);	 
//	  vertex(corner2(0), corner2(1), 1.toFloat, 0.toFloat); 
	  vertex(0.toFloat,0.toFloat,0.toFloat, 0.toFloat, 0.toFloat);
	  vertex(width.toFloat,0.toFloat,-300.toFloat, 1.toFloat, 0.toFloat);
	  vertex(width.toFloat,height.toFloat,(-600).toFloat, 1.toFloat, 1.toFloat);
	  vertex(0.toFloat,height.toFloat,(-300).toFloat, 0.toFloat, 1.toFloat);

//	  val res=50
//	  for (x <- 0 to (width/res).toInt) {
//			for (y <- 0 to (height/res).toInt) { 
//				  val corner1=applyToCoordinate3(0,0)
//				  val corner2=applyToCoordinate3(width,0)
//				  val corner3=applyToCoordinate3(width,height)
//				  val corner4=applyToCoordinate3(0,height)
//			}
//	  }
	  
	  
	  
	  println(1)
	  endShape();
	  
	  /*
      loadPixels()
      imagee.loadPixels()
      for (j <- 0 to height){
        for (i <- 0 to width){
          val coordinateAt = new PVector(i,j)
          val coordinateFrom = applyToPoint(coordinateAt)
          //if (i==100 & j==100){
            println(coordinateFrom)
            println(i+width*j)
            println(coordinateFrom.x.toInt+imagee.width*coordinateFrom.y.toInt)
            if (coordinateFrom.x.toInt+imagee.width*coordinateFrom.y.toInt==48000){exit()}
            pixels(i+width*j)=imagee.pixels(coordinateFrom.x.toInt+imagee.width*coordinateFrom.y.toInt)
          //}
          //if (coordinateFrom.x>0 & coordinateFrom.x<imagee.width & coordinateFrom.y>0 & coordinateFrom.y<imagee.height){
          /*val fromPosition=coordinateFrom.x.toInt+imagee.width*coordinateFrom.y.toInt
          if (fromPosition>=0 & fromPosition<imagee.pixels.length){
            pixels(i+width*j)=imagee.pixels(coordinateFrom.x.toInt+imagee.width*coordinateFrom.y.toInt)
          }*/
        }
      }
      updatePixels()*/
        
	  
	  
	  
	  
	  
	  // update the cam
	  //context.update();
	  
	  //background(255,255,255);
	  
	  // draw depthImageMap
	  //image(context.depthImage(),0,0);

	  // draw irImageMap
	  //image(applyToPImage(context.rgbImage()),context.depthWidth() + 10,0);
  }

  def applyToScreen1(source: PImage):PImage={
	var destination: PImage = createImage(source.width, source.height, RGB);
	var sourceLocation: Int = 0
	var destinationLocation: Int = 0
	var c =color(source.pixels(sourceLocation))
	var mappedCoordinate=Array[Double](0,0)
	var X:Double=0
	var Y:Double=0
	
    // We are going to look at both image's pixels
    source.loadPixels();
    destination.loadPixels();
    
    //println(1)
    val widthh = source.width
    val heightt = source.height
    //println(widthh)
    //println(heightt)
    //println(source.pixels.length) //1049088
    //c = color(source.pixels(1))
    //destination.pixels(0)=c
    
    for (y <- 0 to (heightt-1)) {
      for (x <- 0 to (widthh-1)) {
        //println(2)
        destinationLocation = x + y*widthh;
        /*println(x)
        println(y)
        println(X)
        println(Y)
        println(sourceLocation)*/
        
        //destination.pixels(sourceLocation)=c
        //println(color(0,255,0))
        
        
        mappedCoordinate=applyToCoordinate(Array(x,y))
        X=mappedCoordinate(0)
        Y=mappedCoordinate(1)
        //println(4)
        /*println(x)
        println(y)
        println(X)
        println(Y)*/
        if (X >= 0 & X<widthh & Y>=0 & Y<heightt){
        	//println("dest:")
        	//println(destinationLocation)
        	//println(5)
	        sourceLocation = X.toInt + widthh * Y.toInt
	        //println(6)
	        c = color(source.pixels(sourceLocation))
	        destination.pixels(destinationLocation)=c
        }
        
        }
      }
    //c = color(source.pixels(9999999))//to stop the script
    //println(9)
	
	// We changed the pixels in destination
	destination.updatePixels();
	return(destination)
  }
    
    
    
  def applyToPImage2(source: PImage):PImage={
	var destination: PImage = createImage(source.width, source.height, RGB);
	var sourceLocation: Int = 0
	var destinationLocation: Int = 0
	var c =color(source.pixels(sourceLocation))
	var mappedCoordinate=Array[Double](0,0)
	var X:Double=0
	var Y:Double=0
	
    // We are going to look at both image's pixels
    source.loadPixels();
    destination.loadPixels();
    
    //println(1)
    val widthh = source.width
    val heightt = source.height
    //println(widthh)
    //println(heightt)
    //println(source.pixels.length) //1049088
    //c = color(source.pixels(1))
    //destination.pixels(0)=c
    
    for (y <- 0 to (heightt-1)) {
      for (x <- 0 to (widthh-1)) {
        //println(2)
        destinationLocation = x + y*widthh;
        /*println(x)
        println(y)
        println(X)
        println(Y)
        println(sourceLocation)*/
        
        //destination.pixels(sourceLocation)=c
        //println(color(0,255,0))
        
        
        mappedCoordinate=applyToCoordinate(Array(x,y))
        X=mappedCoordinate(0)
        Y=mappedCoordinate(1)
        //println(4)
        /*println(x)
        println(y)
        println(X)
        println(Y)*/
        if (X >= 0 & X<widthh & Y>=0 & Y<heightt){
        	//println("dest:")
        	//println(destinationLocation)
        	//println(5)
	        sourceLocation = X.toInt + widthh * Y.toInt
	        //println(6)
	        c = color(source.pixels(sourceLocation))
	        destination.pixels(destinationLocation)=c
        }
        
        }
      }
    //c = color(source.pixels(9999999))//to stop the script
    //println(9)
	
	// We changed the pixels in destination
	destination.updatePixels();
	return(destination)
  }
    def applyToPoint(inputPoint: PVector):PVector={
	  var X=inputPoint.x
	  var Y=inputPoint.y
	  var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
	  
	  val x:Float=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val y:Float=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val outputPoint: PVector=new PVector(x,y)
	  return outputPoint
  }
  def applyToPImage(source: PImage):PImage={
	var destination: PImage = createImage(source.width, source.height, RGB);
	var sourceLocation: Int = 0
	var destinationLocation: Int = 0
	var c =color(source.pixels(sourceLocation))
	var mappedCoordinate=Array[Double](0,0)
	var X:Double=0
	var Y:Double=0
    // We are going to look at both image's pixels
    source.loadPixels();
    destination.loadPixels();
    //println(1)
    val widthh = source.width
    val heightt = source.height
    //println(widthh)
    //println(heightt)
    //println(source.pixels.length) //1049088
    //c = color(source.pixels(1))
    //destination.pixels(0)=c
    for (y <- 0 to (heightt-1)) {
      for (x <- 0 to (widthh-1)) {
        //println(2)
        sourceLocation = x + y*widthh;
        /*println(x)
        println(y)
        println(X)
        println(Y)
        println(sourceLocation)*/
        c = color(source.pixels(sourceLocation))
        //destination.pixels(sourceLocation)=c
        //println(color(0,255,0))
        
        
        mappedCoordinate=applyToCoordinate(Array(x,y))
        X=mappedCoordinate(0)
        Y=mappedCoordinate(1)
        //println(4)
        /*println(x)
        println(y)
        println(X)
        println(Y)*/
        if (X >= 0 & X<widthh & Y>=0 & Y<heightt){
        	//println("dest:")
        	//println(destinationLocation)
        	//println(5)
	        destinationLocation = X.toInt + widthh * Y.toInt
	        //println(6)
	        destination.pixels(destinationLocation)=c
        }
        
        }
      }
    //c = color(source.pixels(9999999))//to stop the script
    //println(9)
	
	// We changed the pixels in destination
	destination.updatePixels();
	return(destination)
  }
  def writeHomography(fileName: String){
	  val file = new PrintWriter(new File(fileName));
	  var content: java.lang.String=""
		for(i <- 0 to 2){
		for(j <- 0 to 2){
			content = homographyMatrix(i,j).toString
			file.write(content)
			file.write("\n")
		}
	}
	file.close();
  }    
    
  def loadHomography(fileName: String) {
		var item:String=""
		var column=0
		var row=0
		var homographyMatrix = Matrix.zeros[Double](3,3)
		val file = Source.fromFile(fileName)
		file.getLines.foreach((line) =>{
			item=line
			homographyMatrix(row,column)=item.toDouble
			column+=1
			if (column==3){
				column=0
				row+=1
			}
		})
	}
	

  def computeHomography(cameraPoints: Array[PVector], projectorPoints: Array[PVector]){  
	    if (cameraPoints.length!=projectorPoints.length){println("Different points amount bewteen camera and projector image! Please have a double check.")}
		var numberOfPoints: Int = (cameraPoints.length)
        
		//initializing scalala matrixs cam and proj
		var cam = Matrix.zeros[Double](numberOfPoints,2)
		var proj = Matrix.zeros[Double](numberOfPoints,2)
		//assign values from input array to cam and proj
		for (i <- 0 to (numberOfPoints-1)){
			cam(i,0)=cameraPoints(i).x.toDouble
			cam(i,1)=cameraPoints(i).y.toDouble
			proj(i,0)=projectorPoints(i).x.toDouble
			proj(i,1)=projectorPoints(i).y.toDouble
		}
		
		// Creates the estimation matrix
		var matrix1 = Matrix.zeros[Double](numberOfPoints*2,9)
		//initializing row1 and row2
		var row1 = DenseVector.zeros[Double](9)
		var row2 = DenseVector.zeros[Double](9)
		//assign estimation matrix entries values from cam and proj 
		for (i <- 0 to (numberOfPoints-1)){
			row1 = DenseVector(cam(i,0), cam(i,1), 1, 0, 0, 0, -cam(i,0)*proj(i,0), -cam(i,1)*proj(i,0), -proj(i,0))
			row2 = DenseVector(0, 0, 0, cam(i,0), cam(i,1), 1, -cam(i,0)*proj(i,1), -cam(i,1)*proj(i,1), -proj(i,1))
			matrix1(i*2,::) := row1
			matrix1(i*2+1,::) := row2
		}

		//calculation eigenvalues and eigenvectors
		var matrix2 = matrix1.t * matrix1		
		val (eigenvalues,valuee0,eigenvectors)=eig(matrix2)
		
		
		//find the minium eigenvalue position
		var minPosition=0
		var minValue=eigenvalues(0)
		for (i <- 0 to 8){
		  if (eigenvalues(i)<minValue){
		    minValue=eigenvalues(i)
		    minPosition=i
		  }
		}
		
		//get the eigenvector correspond to the minumn eigenvalue
		var eigenvector = eigenvectors(::,minPosition)
		
		//write the eigenvector to homography matrix
		for(i <- 0 to 2){
			for(j <- 0 to 2){
				homographyMatrix(i,j)=eigenvector(i+3*j)
			}
		}
		
		//invert matrix for applying to coordinates
		//homographyMatrix=inv(homographyMatrix)
  }	


  
  def applyToCoordinates(inputCoordinates: Array[Double]):Array[Double]={
      var outputCoordinates=inputCoordinates
      val numberOfPoints=(inputCoordinates.length)/2
      
      //initializing
      var X=0.0
      var Y=0.0
      var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
      
      for (i <- 0 to (numberOfPoints-1)){
		  X=inputCoordinates(i*2)
		  Y=inputCoordinates(i*2+1)

		  outputCoordinates(i*2)=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
		  outputCoordinates(i*2+1)=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))
      }
      

	  
	  //val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
	  //val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))
	  

	  return outputCoordinates
  }
  
  
  def applyToCoordinate(inputCoordinate: Array[Double]):Array[Double]={
	  val X=inputCoordinate(0)
	  val Y=inputCoordinate(1)
	  var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
	  
	  val x:Double=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
	  val y:Double=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))
	  
	  //val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
	  //val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))
	  

	  return Array(x,y)
  }
  
  def applyToCoordinate3(XX: Int, YY: Int):Array[Float]={
	  val X=XX.toDouble
	  val Y=YY.toDouble
	  var p=Array[Double](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j)
	    }
	  }
	  
	  val x=((p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  val y=((p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))).toFloat
	  
	  //val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
	  //val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))
	  

	  return Array(x,y)
  }
  
  def applyToCoordinate2(inputCoordinate: Array[Double]):Array[Double]={
	val cameraFrame=DenseVector[Double](inputCoordinate(0),inputCoordinate(1),1.toDouble)
    val projectorFrame=homographyMatrix * cameraFrame

	return Array(projectorFrame(0)/projectorFrame(2),projectorFrame(1)/projectorFrame(2))
  }  
  
  
  
}

  


























/* old version until 120624`6`1744
  def test5(){//line and shape conversion test
    //create homography
    val cam=Array[Double](1,1,2,1,2,2,1,2)//unit squire
    val proj=Array[Double](0.8,1.2,2.5,0.6,2.5,2.4,0.8,1.8)//T-shape
    computeHomography(cam,proj)
	homographyMatrix=inv(homographyMatrix)
	
	//input a line
	//println(applyHomographyToCoordinates(Array(1.0,1.0,2.0,1.0)))//success
	//input shape: a square
	val converted=applyToCoordinates(Array[Double](5,5,6,5,6,6,5,6))
	println(converted)
	homographyMatrix=inv(homographyMatrix)
	println(applyToCoordinates(converted))//success
  }
  def test6(){
      val proj=Array[Double](100,100,200,100,200,200,100,200)//unit squire
	  val cam=Array[Double](98,102,255,96,255,204,98,198)//T-shape
    
      
      //val proj=Array[Double](0,0,1,0,1,1,0,1)//this works
      //val cam=Array[Double](0,0,2,0,2,1,0,1)
	  
      computeHomography(cam,proj)
	  //
      val testPoint=new PVector(100,100)
	  println(applyToPoint(testPoint))
	  println(applyToCoordinate(Array[Double](100,100)))
	  println(applyToCoordinate2(Array[Double](100,100)))
  }

  def pointExample1(){
      val proj=Array[Double](100,100,200,100,200,200,100,200)//unit squire
	  val cam=Array[Double](98,102,255,96,255,204,98,198)

      computeHomography(cam,proj)
	  
	  val testPoint=new PVector(100,100)
	  println(applyToPoint(testPoint))
  }

  
  
  def test4(){//point conversion test
    val cam=Array[Double](0,0,1,0,1,1,0,1)
    val proj=Array[Double](0,0,2,0,2,1,0,1)
    computeHomography(cam,proj)
	    homographyMatrix=inv(homographyMatrix)
	    val input=applyHomographyToCoordinate(Array[Double](1,1))
	    homographyMatrix=inv(homographyMatrix)
    println(applyHomographyToCoordinate(input))
  }
*/


/*//old version until 120624`6`1527
/*//Jama Matrix support
import processing.core.PApplet;
import processing.core.PVector;
import Jama.{Matrix => MatrixJama, EigenvalueDecomposition};
*/

  def test3(){
      //val cam=DenseMatrix((1.0,1.0),(1.0,2.0),(2.0,1.0),(2.0,2.0))
      //val proj=DenseMatrix((1.0,1.0),(1.0,2.0),(2.0,1.0),(2.0,2.0))
      //var homographyMatrix=computeHomography(cam, proj)
      
      val matrix=DenseMatrix((0.0,0.0),(0.0,1.0),(1.0,0.0),(1.0,1.0))
      var homographyMatrix=computeHomography(matrix, matrix)
      
      writeHomography("homographyMatrix.data",homographyMatrix)
      //writeHomography("homographyMatrix2.data",loadHomography("homographyMatrix.data"))
      val inputCoordinates=DenseVector[Double](1,1)
      val convertedCoordinates=applyHomography(inputCoordinates)
      println(convertedCoordinates(0))
      println(convertedCoordinates(1))
  }
  
  def test1(){
    	println("-1")
        var a=DenseMatrix((1.0,2.0),(2.0,1.0))
        eig(a)
        val (q,w,e)=eig(a)
        val b=e(0,::)
        println(b(0).toString())
  } 

  def test2(){
      val cam=DenseMatrix((1.0,1.0),(1.0,2.0),(2.0,1.0),(2.0,2.0))
      val proj=DenseMatrix((1.0,1.0),(1.0,2.0),(2.0,1.0),(2.0,2.0))
      var homographyMatrix=computeHomography(cam, proj)
      writeHomography("homographyMatrix.data",homographyMatrix)
      //writeHomography("homographyMatrix2.data",loadHomography("homographyMatrix.data"))
      val inputCoordinates=DenseVector[Double](0,0)
      val convertedCoordinates=applyHomography(inputCoordinates)
      println(convertedCoordinates(0))
      println(convertedCoordinates(1))
  }

  
  def writeHomography(fileName: String, homographyMatrix: scalala.tensor.dense.DenseMatrix[Double]){
	  val file = new PrintWriter(new File(fileName));
	  var content: java.lang.String=""
		for(i <- 0 to 2){
		for(j <- 0 to 2){
			content = homographyMatrix(i,j).toString
			file.write(content)
			file.write("\n")
		}
	}
	file.close();
  }
   
  def loadHomography(fileName: String): scalala.tensor.dense.DenseMatrix[Double]= {
		var item:String=""
		var column=0
		var row=0
		var homographyMatrix = Matrix.zeros[Double](3,3)
		val file = Source.fromFile(fileName)
		file.getLines.foreach((line) =>{
			item=line
			homographyMatrix(row,column)=item.toDouble
			column+=1
			if (column==3){
				column=0
				row+=1
			}
		})
		return homographyMatrix
	}
	
	//def computeHomography(cam: DenseMatrix, proj: DenseMatrix){
  def computeHomography(cam: scalala.tensor.dense.DenseMatrix[Double], proj: scalala.tensor.dense.DenseMatrix[Double]): scalala.tensor.dense.DenseMatrix[Double]= {  
	    // Creates an array of two times the size of the cam[] array, which is 4*3*2=24 
		var matrix1 = Matrix.zeros[Double](8,9)

		
		// Creates the estimation matrix
		var row1 = DenseVector.zeros[Double](9)
		var row2 = row1
		for (i <- 0 to 3){
		    //println(cam(i,0), cam(i,1), proj(i,0), proj(i,1))
			row1 = DenseVector(cam(i,0), cam(i,1), 1, 0, 0, 0, -cam(i,0)*proj(i,0), -cam(i,1)*proj(i,0), -proj(i,0))
			row2 = DenseVector(0, 0, 0, cam(i,0), cam(i,1), 1, -cam(i,0)*proj(i,1), -cam(i,1)*proj(i,1), -proj(i,1))
			
			//println(row1)
			matrix1(i*2,::) := row1
			//println(matrix1(i*2,::))
		    
			//println(row2)
			matrix1(i*2+1,::) := row2
			//println(matrix1(i*2+1,::))
		}

		var matrix2 = matrix1.t * matrix1
		/*for (i <- 0 to 7){
		  println(matrix1(i,::))
		}
		for (i <- 0 to 8){
		  println(matrix1.t(i,::))
		}
		for (i <- 0 to 8){
		  println(matrix2(i,::))
		}*/
			
		val (eigenvalues,valuee0,eigenvectors)=eig(matrix2)
		
		var minPosition=0
		var minValue=eigenvalues(0)
		for (i <- 0 to 8){
		  if (eigenvalues(i)<minValue){
		    minValue=eigenvalues(i)
		    minPosition=i
		  }
		}
		//println(minPosition)
		val eigenvector = eigenvectors(::,minPosition)
		println(eigenvalues)
		/*for (i <- 0 to 8){
		  println(eigenvectors(i,::))
		}*/
		println(eigenvector)
		
		
		
		/*//Jama Matrix
		var X: MatrixJama = MatrixJama.random(9,9);
		for (i <- 0 to 8){
		  for (j<- 0 to 8){
		  X.set(i,j,matrix2(i,j));
		  }
		}
		println(" ")
		for (i <- 0 to 8){
		  println(X.get(i,0));
		}
		val E:EigenvalueDecomposition  = X.eig();
		val v:MatrixJama = E.getV();
		val eigenvaluess = E.getRealEigenvalues();
		for (i<- 0 to 8){println(eigenvaluess(i));}
		println("")
		for (i <- 0 to 8){
		  println(v.get(i,0));
		}
		*/

		
		var homographyMatrix=DenseMatrix.zeros[Double](3,3)
		for(i <- 0 to 2){
			for(j <- 0 to 2){
				homographyMatrix(i,j)=eigenvector(i+3*j)
			}
		}
		return homographyMatrix
  }	
	
  def myMain(){

  }
	
  def applyHomography(inputCoordinates: scalala.tensor.dense.DenseVector[Double]):scalala.tensor.dense.DenseVector[Double]={
	  val homographyMatrix=loadHomography("homographyMatrix.data")
	  val X=inputCoordinates(0).toFloat
	  val Y=inputCoordinates(1).toFloat
	  var p=Array[Float](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j).toFloat
	    }
	  }
	  
	  val x:Float=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
	  val y:Float=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))
	  
	  //val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
	  //val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))
	  
	  var sum:Float=0
	  for (i <- 1 to 9){
	    sum+=p(i)*p(i)
	  }
	  println("sum:")
	  println(sum)
	  
	  var convertedCoordinates=DenseVector(x.toDouble,y.toDouble)
	  return convertedCoordinates
  }
  
  def applyHomography(inputCoordinates: scalala.tensor.dense.DenseVector[Double]):scalala.tensor.dense.DenseVector[Double]={
	  val homographyMatrix=loadHomography("homographyMatrix.data")
	  val X=inputCoordinates(0).toFloat
	  val Y=inputCoordinates(1).toFloat
	  var p=Array[Float](0,0,0,0,0,0,0,0,0,0)
  	  for(i <- 0 to 2){
		for(j <- 0 to 2){
		  p(i+3*j+1)=homographyMatrix(i,j).toFloat
	    }
	  }
	  
	  val x:Float=(p(1)*X+p(2)*Y+p(3))/(p(7)*X+p(8)*Y+p(9))
	  val y:Float=(p(4)*X+p(5)*Y+p(6))/(p(7)*X+p(8)*Y+p(9))
	  
	  //val x:Float=(p(0)*X+p(1)*Y+p(2))/(p(6)*X+p(7)*Y+p(8))
	  //val y:Float=(p(3)*X+p(4)*Y+p(5))/(p(6)*X+p(7)*Y+p(8))
	  
	  var sum:Float=0
	  for (i <- 1 to 9){
	    sum+=p(i)*p(i)
	  }
	  println("sum:")
	  println(sum)
	  
	  var convertedCoordinates=DenseVector(x.toDouble,y.toDouble)
	  return convertedCoordinates
  }
}
*/

