import SimpleOpenNI.*;
import processing.opengl.*;
SimpleOpenNI kinect;

PVector q1=new PVector(0,0,0);
PVector q2=new PVector(640,0,0);
PVector q3=new PVector(640,480,0);
PVector q4=new PVector(0,480,0);
int pointt=0;
int lasttime=second()+minute()*60;
int lasttime2=second()+minute()*60;
PVector[] input = new PVector[3];
Boolean keystoneCorrection=false;

void setup()
{
  size(640, 480, OPENGL);
  kinect = new SimpleOpenNI(this);
  kinect.enableDepth();
}


void draw()
{
  kinect.update();
  PImage depthImage = kinect.depthImage();

  if (second()+minute()*60-lasttime>0.1){
    int[] depthValues = kinect.depthMap();
    PVector a = new PVector(200,250,0);
    PVector b = new PVector(300,260,0);
    PVector c= new PVector(250, 300,0);
    float magnification=0.4;
    a.z=-depthValues[int(a.x+a.y*640)]*magnification;
    b.z=-depthValues[int(b.x+b.y*640)]*magnification;
    c.z=-depthValues[int(c.x+c.y*640)]*magnification;
//    float adjust=min(a.z,b.z,c.z)+150;
//    a.z=a.z-adjust;
//    b.z=b.z-adjust;
//    c.z=c.z-adjust;
    math(a,b,c);
    lasttime=second()+minute()*60;
  }
  
    stroke(0,255,0);
    background(0);
//    fill(0,255,0);
//    rect(0+100,100,width-100,height-100);
    beginShape();
    textureMode(NORMALIZED);
    texture(depthImage);
    vertex(0, 0, q1.z, 0, 0);
    vertex(width, 0, q2.z, 1, 0);
    vertex(width, height, q3.z, 1, 1);
    vertex(0, height, q4.z, 0, 1);
    endShape();
    if (second()+minute()*60-lasttime2>1){
      println("");
      println(q1.z);
      println(q2.z);
      println(q3.z);
      println(q4.z);
      lasttime2=second()+minute()*60;
    }
}



/*
void draw()
{
  kinect.update();
  PImage depthImage = kinect.depthImage();
  if (keystoneCorrection==false){
    //image(depthImage, 0, 0);
  }else{
    noStroke();
    beginShape();
    textureMode(NORMALIZED);
    texture(depthImage);
    vertex(0, 0, q1.z, 0, 0);
    vertex(width, 0, q2.z, 1, 0);
    vertex(width, height, q3.z, 1, 1);
    vertex(0, height, q4.z, 0, 1);
    println(q4.z);
    endShape();
    q4.z+=1;
  }
}
void mousePressed(){
  int[] depthValues = kinect.depthMap();
  int clickPosition = mouseX + (mouseY * 640);
  int millimeters = depthValues[clickPosition];
  float inches = millimeters / 25.4;
  println("mm: "
+ millimeters + " in: " + inches);
  
  if (pointt<3){
    input[pointt]=new PVector(mouseX,mouseY,-inches*2);
    pointt+=1;
  }
  if (pointt==3){
    math(input[0],input[1],input[2]);
    keystoneCorrection=true;
  }
}
*/


void math(PVector a, PVector b, PVector c){
  float dx=0;
  float dy=0;
//  (a.x-b.x)*dx+(a.y-b.y)*dy=(a.z-b.z)
//  (a.x-c.x)*dx+(a.y-c.y)*dy=(a.z-c.z)
//  ((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x)=((a.z-c.z)-(a.y-c.y)*dy)/(a.x-c.x)
//  (a.z-b.z)/(a.x-b.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.y-c.y)*dy/(a.x-c.x)
//  (a.y-c.y)*dy/(a.x-c.x)-(a.y-b.y)*dy/(a.x-b.x)=(a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x)
  dy=((a.z-c.z)/(a.x-c.x)-(a.z-b.z)/(a.x-b.x))/((a.y-c.y)/(a.x-c.x)-(a.y-b.y)/(a.x-b.x));
  dx=((a.z-b.z)-(a.y-b.y)*dy)/(a.x-b.x);
  
  q1.z=a.z-((a.x-q1.x)*dx+(a.y-q1.y)*dy);
  q2.z=a.z-((a.x-q2.x)*dx+(a.y-q2.y)*dy);
  q3.z=a.z-((a.x-q3.x)*dx+(a.y-q3.y)*dy);
  q4.z=a.z-((a.x-q4.x)*dx+(a.y-q4.y)*dy);
//  float adjust0=max(q1.z,q2.z,q3.z);
//  float adjust=max(adjust0,q4.z);
//  q1.z-=adjust;
//  q2.z-=adjust;
//  q3.z-=adjust;
//  q4.z-=adjust;
}
